#ifndef WARTHOG_CONSTANTS_H
#define WARTHOG_CONSTANTS_H

// constants.h
//
// @author: dharabor
// @created: 01/08/2012
//

#include <cfloat>
#include <cmath>
#include <climits>
#include <stdint.h>
#include <boost/multiprecision/cpp_int.hpp>

namespace warthog
{
    typedef uint64_t sn_id_t; // address space for state identifiers
    static const sn_id_t SN_ID_MAX = UINT64_MAX;
    static const uint32_t GRID_ID_MAX = (uint32_t)warthog::SN_ID_MAX;
    static const sn_id_t NO_PARENT = SN_ID_MAX;

	// each node in a weighted grid map uses sizeof(dbword) memory.
	// in a uniform-cost grid map each dbword is a contiguous set
	// of nodes s.t. every bit represents a node.
	typedef uint8_t dbword;

	// gridmap constants
	static const uint32_t DBWORD_BITS = sizeof(warthog::dbword)*8;
	static const uint32_t DBWORD_BITS_MASK = (warthog::DBWORD_BITS-1);
	static const uint32_t LOG2_DBWORD_BITS = static_cast<uint32_t>(ceil(log10(warthog::DBWORD_BITS) / log10(2)));

	// search and sort constants
	static const double DBL_ONE = 1.0f;
	static const double DBL_TWO = 2.0f;
	static const double DBL_ROOT_TWO = 1.414213562f;
    static const double DBL_ROOT_THREE = 1.732050808f;
	static const double DBL_ONE_OVER_TWO = 0.5;
	static const double DBL_ONE_OVER_ROOT_TWO = 1.0/DBL_ROOT_TWO;//0.707106781f;
	static const double DBL_ROOT_TWO_OVER_FOUR = DBL_ROOT_TWO*0.25;
    static const int32_t ONE = 100000;

    static const uint32_t INF32 = UINT32_MAX; // indicates uninitialised or undefined values 
    static const uint64_t INFTY = UINT64_MAX; // indicates uninitialised or undefined values 

    typedef double cost_t;
    static const cost_t COST_MAX = DBL_MAX; 
    static const cost_t COST_MIN = DBL_MIN;

	// hashing constants
	static const uint32_t FNV32_offset_basis = 2166136261;
	static const uint32_t FNV32_prime = 16777619;

    // JPS large bitmasks (over 64bit)
    inline void
    inner_store_large_num(uint8_t tiles[9], uint8_t given[9])
    {
        tiles[0] = given[0];
        tiles[1] = given[1];
        tiles[2] = given[2];
        tiles[3] = given[3];
        tiles[4] = given[4];
        tiles[5] = given[5];
        tiles[6] = given[6];
        tiles[7] = given[7];
        tiles[8] = given[8];
    }

    inline boost::multiprecision::uint128_t 
    store_large_num(uint8_t large_num[9])
    {
        boost::multiprecision::uint128_t tiles;
        inner_store_large_num((uint8_t*)&tiles, large_num);    
        return tiles;
    }

    // 55556405003242636032
        // tiles[8] tiles[7] tiles[6] tiles[5] tiles[4] tiles[3] tiles[2] tiles[1] tiles[0]
        // 00000011 00000011 00000000 00000000 00000000 00000000 00000011 00000011 00000000 
    static uint8_t num_components1_wc[9] = {0, 3, 3, 0, 0, 0, 0, 3, 3};
    static const boost::multiprecision::uint128_t lnum_USW_wc = store_large_num(num_components1_wc);

    // 55556405003242635520
        // tiles[8] tiles[7] tiles[6] tiles[5] tiles[4] tiles[3] tiles[2] tiles[1] tiles[0]
        // 00000011 00000011 00000000 00000000 00000000 00000000 00000011 00000001 00000000 
    static uint8_t num_components1[9] = {0, 1, 3, 0, 0, 0, 0, 3, 3};
    static const boost::multiprecision::uint128_t lnum_USW = store_large_num(num_components1);
    
    // 37037603335495090688
        // tiles[8] tiles[7] tiles[6] tiles[5] tiles[4] tiles[3] tiles[2] tiles[1] tiles[0]
        // 00000010 00000010 00000000 00000000 00000000 00000000 00000010 00000010 00000000
    static uint8_t num_components2_wc[9] = {0, 2, 2, 0, 0, 0, 0, 2, 2};
    static const boost::multiprecision::uint128_t lnum_US_wc = store_large_num(num_components2_wc);
    
    // 37037603335495090176
        // tiles[8] tiles[7] tiles[6] tiles[5] tiles[4] tiles[3] tiles[2] tiles[1] tiles[0]
        // 00000010 00000010 00000000 00000000 00000000 00000000 00000010 00000000 00000000
    static uint8_t num_components2[9] = {0, 0, 2, 0, 0, 0, 0, 2, 2};
    static const boost::multiprecision::uint128_t lnum_US = store_large_num(num_components2);
    
    // 111112810006485272064
        // tiles[8] tiles[7] tiles[6] tiles[5] tiles[4] tiles[3] tiles[2] tiles[1] tiles[0]
        // 00000110 00000110 00000000 00000000 00000000 00000000 00000110 00000110 00000000
    static uint8_t num_components3_wc[9] = {0, 6, 6, 0, 0, 0, 0, 6, 6};
    static const boost::multiprecision::uint128_t lnum_USE_wc = store_large_num(num_components3_wc);
    
    // 111112810006485271552
        // tiles[8] tiles[7] tiles[6] tiles[5] tiles[4] tiles[3] tiles[2] tiles[1] tiles[0]
        // 00000110 00000110 00000000 00000000 00000000 00000000 00000110 00000100 00000000
    static uint8_t num_components3[9] = {0, 4, 6, 0, 0, 0, 0, 6, 6};
    static const boost::multiprecision::uint128_t lnum_USE = store_large_num(num_components3);
    
    // 36893488147419234304
        // tiles[8] tiles[7] tiles[6] tiles[5] tiles[4] tiles[3] tiles[2] tiles[1] tiles[0]
        // 00000010 00000000 00000000 00000000 00000000 00000000 00000010 00000000 00000000
    static uint8_t num_components4[9] = {0, 0, 2, 0, 0, 0, 0, 0, 2};
    static const boost::multiprecision::uint128_t lnum_US_forcedU = store_large_num(num_components4);
    
    // 37037603335494959104
        // tiles[8] tiles[7] tiles[6] tiles[5] tiles[4] tiles[3] tiles[2] tiles[1] tiles[0]
        // 00000010 00000010 00000000 00000000 00000000 00000000 00000000 00000000 00000000
    static uint8_t num_components5[9] = {0, 0, 0, 0, 0, 0, 0, 2, 2};
    static const boost::multiprecision::uint128_t lnum_US_forcedN = store_large_num(num_components5);
    
    // 18590859261785407488
        // tiles[8] tiles[7] tiles[6] tiles[5] tiles[4] tiles[3] tiles[2] tiles[1] tiles[0]
        // 00000001 00000010 00000000 00000000 00000000 00000000 00000000 00000000 00000000
    static uint8_t num_components6[9] = {0, 0, 0, 0, 0, 0, 0, 2, 1};
    static const boost::multiprecision::uint128_t lnum_US_forcedNE = store_large_num(num_components6);
    
    // 73931091482914062336
        // tiles[8] tiles[7] tiles[6] tiles[5] tiles[4] tiles[3] tiles[2] tiles[1] tiles[0]
        // 00000100 00000010 00000000 00000000 00000000 00000000 00000000 00000000 00000000
    static uint8_t num_components7[9] = {0, 0, 0, 0, 0, 0, 0, 2, 4};
    static const boost::multiprecision::uint128_t lnum_US_forcedNW = store_large_num(num_components7);
    
    // 73786976294838207488
        // tiles[8] tiles[7] tiles[6] tiles[5] tiles[4] tiles[3] tiles[2] tiles[1] tiles[0]
        // 00000100 00000000 00000000 00000000 00000000 00000000 00000000 00000100 00000000
    static uint8_t num_components8[9] = {0, 4, 0, 0, 0, 0, 0, 0, 4};
    static const boost::multiprecision::uint128_t lnum_US_forcedDNE = store_large_num(num_components8);
    
    // 18446744073709551872
        // tiles[8] tiles[7] tiles[6] tiles[5] tiles[4] tiles[3] tiles[2] tiles[1] tiles[0]
        // 00000001 00000000 00000000 00000000 00000000 00000000 00000000 00000001 00000000
    static uint8_t num_components9[9] = {0, 1, 0, 0, 0, 0, 0, 0, 1};
    static const boost::multiprecision::uint128_t lnum_US_forcedDNW = store_large_num(num_components9);
    
    // 18446744073709682688
        // tiles[8] tiles[7] tiles[6] tiles[5] tiles[4] tiles[3] tiles[2] tiles[1] tiles[0]
        // 00000001 00000000 00000000 00000000 00000000 00000000 00000010 00000000 00000000
    static uint8_t num_components10[9] = {0, 0, 2, 0, 0, 0, 0, 0, 1};
    static const boost::multiprecision::uint128_t lnum_US_forcedDE = store_large_num(num_components10);
    
    // 73786976294838337536
        // tiles[8] tiles[7] tiles[6] tiles[5] tiles[4] tiles[3] tiles[2] tiles[1] tiles[0]
        // 00000100 00000000 00000000 00000000 00000000 00000000 00000010 00000000 00000000
    static uint8_t num_components11[9] = {0, 0, 2, 0, 0, 0, 0, 0, 4};
    static const boost::multiprecision::uint128_t lnum_US_forcedDW = store_large_num(num_components11);
    
    // 74075206670989918208
        // tiles[8] tiles[7] tiles[6] tiles[5] tiles[4] tiles[3] tiles[2] tiles[1] tiles[0]
        // 00000100 00000100 00000000 00000000 00000000 00000000 00000000 00000000 00000000
    static uint8_t num_components12[9] = {0, 0, 0, 0, 0, 0, 0, 4, 4};
    static const boost::multiprecision::uint128_t lnum_NdiagUSE = store_large_num(num_components12);
    
    // 18518801667747479552
        // tiles[8] tiles[7] tiles[6] tiles[5] tiles[4] tiles[3] tiles[2] tiles[1] tiles[0]
        // 00000001 00000001 00000000 00000000 00000000 00000000 00000000 00000000 00000000
    static uint8_t num_components13[9] = {0, 0, 0, 0, 0, 0, 0, 1, 1};
    static const boost::multiprecision::uint128_t lnum_NdiagUSW = store_large_num(num_components13);
    
    // 55340232221128654848
        // tiles[8] tiles[7] tiles[6] tiles[5] tiles[4] tiles[3] tiles[2] tiles[1] tiles[0]
        // 00000011 00000000 00000000 00000000 00000000 00000000 00000000 00000000 00000000
    static uint8_t num_components14[9] = {0, 0, 0, 0, 0, 0, 0, 0, 3};
    static const boost::multiprecision::uint128_t lnum_EdiagUSW = store_large_num(num_components14);
    
    // 36893488147419103232
        // tiles[8] tiles[7] tiles[6] tiles[5] tiles[4] tiles[3] tiles[2] tiles[1] tiles[0]
        // 00000010 00000000 00000000 00000000 00000000 00000000 00000000 00000000 00000000
    static uint8_t num_components15[9] = {0, 0, 0, 0, 0, 0, 0, 0, 2};
    static const boost::multiprecision::uint128_t lnum_onlyUS = store_large_num(num_components15);
    
    // 110680464442257309696
        // tiles[8] tiles[7] tiles[6] tiles[5] tiles[4] tiles[3] tiles[2] tiles[1] tiles[0]
        // 00000110 00000000 00000000 00000000 00000000 00000000 00000000 00000000 00000000
    static uint8_t num_components16[9] = {0, 0, 0, 0, 0, 0, 0, 0, 6};
    static const boost::multiprecision::uint128_t lnum_WdiagUSE = store_large_num(num_components16);
    
    // 73786976294838468608
        // tiles[8] tiles[7] tiles[6] tiles[5] tiles[4] tiles[3] tiles[2] tiles[1] tiles[0]
        // 00000100 00000000 00000000 00000000 00000000 00000000 00000100 00000000 00000000
    static uint8_t num_components17[9] = {0, 0, 4, 0, 0, 0, 0, 0, 4};
    static const boost::multiprecision::uint128_t lnum_DdiagUSE = store_large_num(num_components17);

    // 73786976294838206464
        // tiles[8] tiles[7] tiles[6] tiles[5] tiles[4] tiles[3] tiles[2] tiles[1] tiles[0]
        // 00000100 00000000 00000000 00000000 00000000 00000000 00000000 00000000 00000000
    static uint8_t num_components18[9] = {0, 0, 0, 0, 0, 0, 0, 0, 4};
    static const boost::multiprecision::uint128_t lnum_onlyUSE = store_large_num(num_components18);
    
    // 18446744073709551616
        // tiles[8] tiles[7] tiles[6] tiles[5] tiles[4] tiles[3] tiles[2] tiles[1] tiles[0]
        // 00000001 00000000 00000000 00000000 00000000 00000000 00000000 00000000 00000000
    static uint8_t num_components19[9] = {0, 0, 0, 0, 0, 0, 0, 0, 1};
    static const boost::multiprecision::uint128_t lnum_onlyUSW = store_large_num(num_components19);
    
    // 18446744073709617152
        // tiles[8] tiles[7] tiles[6] tiles[5] tiles[4] tiles[3] tiles[2] tiles[1] tiles[0]
        // 00000001 00000000 00000000 00000000 00000000 00000000 00000001 00000000 00000000
    static uint8_t num_components20[9] = {0, 0, 1, 0, 0, 0, 0, 0, 1};
    static const boost::multiprecision::uint128_t lnum_SWandUSW = store_large_num(num_components20);

    // 36893488147419103744
        // tiles[8] tiles[7] tiles[6] tiles[5] tiles[4] tiles[3] tiles[2] tiles[1] tiles[0]
        // 00000010 00000000 00000000 00000000 00000000 00000000 00000000 00000010 00000000
    static uint8_t num_components21[9] = {0, 2, 0, 0, 0, 0, 0, 0, 2};
    static const boost::multiprecision::uint128_t lnum_centerUS = store_large_num(num_components21);

    // 73786976294838206976
        // tiles[8] tiles[7] tiles[6] tiles[5] tiles[4] tiles[3] tiles[2] tiles[1] tiles[0]
        // 00000100 00000000 00000000 00000000 00000000 00000000 00000000 00000010 00000000
    static uint8_t num_components22[9] = {0, 2, 0, 0, 0, 0, 0, 0, 4};
    static const boost::multiprecision::uint128_t lnum_centerUSE = store_large_num(num_components22);

    // 18446744073709552128
        // tiles[8] tiles[7] tiles[6] tiles[5] tiles[4] tiles[3] tiles[2] tiles[1] tiles[0]
        // 00000001 00000000 00000000 00000000 00000000 00000000 00000000 00000010 00000000 
    static uint8_t num_components23[9] = {0, 2, 0, 0, 0, 0, 0, 0, 1};
    static const boost::multiprecision::uint128_t lnum_centerUSW = store_large_num(num_components23);
}

#endif


