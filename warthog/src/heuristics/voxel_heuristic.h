#ifndef WARTHOG_VOXEL_HEURISTIC_H
#define WARTHOG_VOXEL_HEURISTIC_H

// voxel_heuristic3D.h
//
// Analogue of Manhattan Heuristic but for 26C grids.
//
// @author: dharabor, tnobes
// @created: 10/04/2022
//

#include "constants.h"
#include "helpers.h"
#include "jps3D.h"

#include <cstdlib>

namespace warthog
{

class voxel_heuristic
{
	public:
		voxel_heuristic(uint32_t mapwidth, uint32_t mapheight, uint32_t mapdepth) 
	    	: mapwidth_(mapwidth), mapheight_(mapheight), hscale_(1.0)
            { 
                dxyz_to_dir_[0][0][0] = warthog::jps3D::DOWNNORTHWEST;
                dxyz_to_dir_[1][0][0] = warthog::jps3D::DOWNNORTH;
                dxyz_to_dir_[2][0][0] = warthog::jps3D::DOWNNORTHEAST;
                dxyz_to_dir_[0][1][0] = warthog::jps3D::DOWNWEST;
                dxyz_to_dir_[1][1][0] = warthog::jps3D::DOWN;
                dxyz_to_dir_[2][1][0] = warthog::jps3D::DOWNEAST;
                dxyz_to_dir_[0][2][0] = warthog::jps3D::DOWNSOUTHWEST;
                dxyz_to_dir_[1][2][0] = warthog::jps3D::DOWNSOUTH;
                dxyz_to_dir_[2][2][0] = warthog::jps3D::DOWNSOUTHEAST;
                dxyz_to_dir_[0][0][1] = warthog::jps3D::NORTHWEST;
                dxyz_to_dir_[1][0][1] = warthog::jps3D::NORTH;
                dxyz_to_dir_[2][0][1] = warthog::jps3D::NORTHEAST;
                dxyz_to_dir_[0][1][1] = warthog::jps3D::WEST;
                dxyz_to_dir_[1][1][1] = warthog::jps3D::NONE;
                dxyz_to_dir_[2][1][1] = warthog::jps3D::EAST;
                dxyz_to_dir_[0][2][1] = warthog::jps3D::SOUTHWEST;
                dxyz_to_dir_[1][2][1] = warthog::jps3D::SOUTH;
                dxyz_to_dir_[2][2][1] = warthog::jps3D::SOUTHEAST;
                dxyz_to_dir_[0][0][2] = warthog::jps3D::UPNORTHWEST;
                dxyz_to_dir_[1][0][2] = warthog::jps3D::UPNORTH;
                dxyz_to_dir_[2][0][2] = warthog::jps3D::UPNORTHEAST;
                dxyz_to_dir_[0][1][2] = warthog::jps3D::UPWEST;
                dxyz_to_dir_[1][1][2] = warthog::jps3D::UP;
                dxyz_to_dir_[2][1][2] = warthog::jps3D::UPEAST;
                dxyz_to_dir_[0][2][2] = warthog::jps3D::UPSOUTHWEST;
                dxyz_to_dir_[1][2][2] = warthog::jps3D::UPSOUTH;
                dxyz_to_dir_[2][2][2] = warthog::jps3D::UPSOUTHEAST;
            }
		~voxel_heuristic() { }

		inline double
		h(int32_t x, int32_t y, int32_t z, int32_t x2, int32_t y2, int32_t z2)
		{
            // NB: precision loss when warthog::cost_t is an integer
            int32_t dx = abs(x-x2);
			int32_t dy = abs(y-y2);
            int32_t dz = abs(z-z2);
            int32_t delta_min, delta_mid, delta_max;
            if(dx < dy)
            {
                if(dx < dz)
                {
                    delta_min = dx;
                    if(dy < dz)
                    {
                        delta_mid = dy;
                        delta_max = dz;
                    }
                    else
                    {
                        delta_mid = dz;
                        delta_max = dy;
                    }
                }
                else
                {
                    delta_min = dz;
                    delta_mid = dx;
                    delta_max = dy;
                }
            }
            else // dy < dx
            {
                if(dy < dz)
                {
                    delta_min = dy;
                    if(dx < dz)
                    {
                        delta_mid = dx;
                        delta_max = dz;
                    }
                    else
                    {
                        delta_mid = dz;
                        delta_max = dx;
                    }
                }
                else
                {
                    delta_min = dz;
                    delta_mid = dy;
                    delta_max = dx;
                }
            }
            return ((warthog::DBL_ROOT_THREE - warthog::DBL_ROOT_TWO) * delta_min + 
                    (warthog::DBL_ROOT_TWO - warthog::DBL_ONE) * delta_mid + 
                    delta_max) * hscale_;
		}

		inline double
		h(warthog::sn_id_t id, warthog::sn_id_t id2)
		{
			int32_t x, x2;
			int32_t y, y2;
            int32_t z, z2;
			warthog::helpers::index_to_xyz((uint32_t)id, mapwidth_, mapheight_, x, y, z);
			warthog::helpers::index_to_xyz((uint32_t)id2, mapwidth_, mapheight_, x2, y2, z2);
			return this->h(x, y, z, x2, y2, z2);
		}

        inline double
		h_info(warthog::sn_id_t id, warthog::sn_id_t id2, uint32_t* steps_root3, 
            uint32_t* steps_root2, uint32_t* steps_1, 
            warthog::jps3D::direction* dir_root3, warthog::jps3D::direction* dir_root2, 
            warthog::jps3D::direction* dir_1)
		{
			int32_t x, x2;
			int32_t y, y2;
            int32_t z, z2;
			warthog::helpers::index_to_xyz((uint32_t)id, mapwidth_, mapheight_, x, y, z);
			warthog::helpers::index_to_xyz((uint32_t)id2, mapwidth_, mapheight_, x2, y2, z2);
            int32_t sdx = x2 - x;
            int32_t sdy = y2 - y;
            int32_t sdz = z2 - z;
            int32_t dx = abs(sdx);
			int32_t dy = abs(sdy);
            int32_t dz = abs(sdz);
            int32_t delta_min, delta_mid, delta_max;
            if(dx < dy)
            {
                if(dx < dz)
                {
                    delta_min = dx;
                    if(dy < dz)
                    {
                        delta_mid = dy;
                        delta_max = dz;
                    }
                    else
                    {
                        delta_mid = dz;
                        delta_max = dy;
                    }
                }
                else
                {
                    delta_min = dz;
                    delta_mid = dx;
                    delta_max = dy;
                }
            }
            else // dy < dx
            {
                if(dy < dz)
                {
                    delta_min = dy;
                    if(dx < dz)
                    {
                        delta_mid = dx;
                        delta_max = dz;
                    }
                    else
                    {
                        delta_mid = dz;
                        delta_max = dx;
                    }
                }
                else
                {
                    delta_min = dz;
                    delta_mid = dy;
                    delta_max = dx;
                }
            }
            // define number of steps in each move type
            *steps_root3 = delta_min;
            *steps_root2 = delta_mid - delta_min;
            *steps_1 = delta_max;
            // define direction of each move type
            *dir_root3 = dxyz_to_dir_[pos_neg_flatten(sdx)][pos_neg_flatten(sdy)][pos_neg_flatten(sdz)];
            reduce_sdxyz(&sdx, delta_min);
            reduce_sdxyz(&sdy, delta_min);
            reduce_sdxyz(&sdz, delta_min);
            *dir_root2 = dxyz_to_dir_[pos_neg_flatten(sdx)][pos_neg_flatten(sdy)][pos_neg_flatten(sdz)];
            reduce_sdxyz(&sdx, delta_mid - delta_min);
            reduce_sdxyz(&sdy, delta_mid - delta_min);
            reduce_sdxyz(&sdz, delta_mid - delta_min);
            *dir_1 = dxyz_to_dir_[pos_neg_flatten(sdx)][pos_neg_flatten(sdy)][pos_neg_flatten(sdz)];
            // return total heuristic estimate of distance
            return ((warthog::DBL_ROOT_THREE - warthog::DBL_ROOT_TWO) * delta_min + 
                    (warthog::DBL_ROOT_TWO - warthog::DBL_ONE) * delta_mid + 
                    delta_max) * hscale_;
		}

        inline void
        reduce_sdxyz(int32_t* sd, uint32_t sub_value)
        {
            if((*sd) > 0) { (*sd) -= sub_value; }
            else if((*sd) < 0) { (*sd) += sub_value; }
        }

        inline int
        pos_neg_flatten(int32_t n)
        {
            if(n > 0) { return 2; }
            else if(n < 0) { return 0; }
            else { return 1; }
        }

        inline uint32_t
        get_mapwidth()
        {
            return mapwidth_;
        }

        inline uint32_t
        get_mapheight()
        {
            return mapheight_;
        }

        size_t
        mem() { return sizeof(this); }

	private:
		uint32_t mapwidth_;
        uint32_t mapheight_;
        warthog::jps3D::direction dxyz_to_dir_[3][3][3];
        double hscale_;
};

}

#endif

