#ifndef WARTHOG_online_jump_point_locator26C_ASL_H
#define WARTHOG_online_jump_point_locator26C_ASL_H

// online_jump_point_locator26c_asl.h
//
// A class wrapper around some code that finds, online, jump point
// successors of an arbitrary nodes in a uniform-cost grid map.
//
// For theoretical details see:
// [Harabor D. and Grastien A, 2011, 
// Online Graph Pruning Pathfinding on Grid Maps, AAAI]
// [Nobes T. et. al., 2022, JPS in 3D, SoCS] 
//
// @author: dharabor, tnobes
// @created: 10/04/2022
//

#include "jps3D.h"
#include "voxelmap.h"
#include "voxel_heuristic.h"
#include <boost/multiprecision/cpp_int.hpp>
#include <unordered_map>
#include <limits>

namespace warthog
{

class online_jump_point_locator26c_asl
{
	public: 
		online_jump_point_locator26c_asl(warthog::voxelmap* map, warthog::voxel_heuristic* heuristic);
		~online_jump_point_locator26c_asl();

        inline uint32_t
        get_mcost(warthog::jps3D::direction dir)
        {
            return (uint32_t)mcost_arr_[get_dir_ind(dir)];
        }

		void
		jump(warthog::jps3D::direction d, uint32_t node_id, uint32_t goalid, 
				uint32_t& jumpnode_id, warthog::cost_t& jumpcost,uint32_t steps_root3, 
                uint32_t steps_root2, uint32_t steps_1, warthog::jps3D::direction dir_root3, 
                warthog::jps3D::direction dir_root2, warthog::jps3D::direction dir_1,
                uint32_t scan_lim, uint32_t penalty_sum, uint32_t opt_mcost,
                uint32_t* nodes_scanned);

		uint32_t 
		mem()
		{
			return sizeof(this) + 
                rmap_->mem() +
                zmap_->mem() +
                xmap_->mem();
		}

        inline uint32_t
        get_next_id_from_octile_steps(uint32_t id, uint32_t i,
                uint32_t j, uint32_t k, warthog::jps3D::direction dir_i, 
                warthog::jps3D::direction dir_j, warthog::jps3D::direction dir_k,
                uint32_t steps)
        {
            uint32_t count = 0;
            while(count < steps && count < i)
            {
                id = take_step_in_dir(id, dir_i);
                count++;
            }
            while(count < steps && count < j)
            {
                id = take_step_in_dir(id, dir_j);
                count++;
            }
            while(count < steps && count < k)
            {
                id = take_step_in_dir(id, dir_k);
                count++;
            }
            return id;
        }

        inline void
        reduce_hsteps(uint32_t* steps_root3, uint32_t* steps_root2, uint32_t* steps_1,
            warthog::jps3D::direction dir_root3, warthog::jps3D::direction dir_root2, 
            warthog::jps3D::direction dir_1, warthog::jps3D::direction opt_d)
        {
            if(opt_d == dir_root3)
            { 
                (*steps_root3)--;
                return;
            }
            else if(opt_d == dir_root2)
            { 
                (*steps_root2)--;
                return;
            }
            else if(opt_d == dir_1)
            { 
                (*steps_1)--;
                return;
            }
            else
            {
                return;
            }
        }

        inline void
        reduce_hsteps_n_times(uint32_t* steps_root3, uint32_t* steps_root2, uint32_t* steps_1,
            warthog::jps3D::direction dir_root3, warthog::jps3D::direction dir_root2, 
            warthog::jps3D::direction dir_1, warthog::jps3D::direction opt_d, uint32_t n)
        {
            if(opt_d == dir_root3)
            { 
                (*steps_root3)-= n;
                return;
            }
            else if(opt_d == dir_root2)
            { 
                (*steps_root2)-= n;
                return;
            }
            else if(opt_d == dir_1)
            { 
                (*steps_1)-= n;
                return;
            }
            else
            {
                return;
            }
        }

        std::vector<int>
        dir_to_coords(warthog::jps3D::direction d)
        {
            std::vector<int> coords = {0, 0, 0}; // {x, y, z}
            switch(d)
            {
                case warthog::jps3D::NORTH:
                    coords[1]--;
                    break;
                case warthog::jps3D::SOUTH:
                    coords[1]++;
                    break;
                case warthog::jps3D::EAST:
                    coords[0]++;
                    break;
                case warthog::jps3D::WEST:
                    coords[0]--;
                    break;
                case warthog::jps3D::UP:
                    coords[2]++;
                    break;
                case warthog::jps3D::DOWN:
                    coords[2]--;
                    break;
                case warthog::jps3D::NORTHEAST:
                    coords[1]--;
                    coords[0]++;
                    break;
                case warthog::jps3D::NORTHWEST:
                    coords[1]--;
                    coords[0]--;
                    break;
                case warthog::jps3D::SOUTHEAST:
                    coords[1]++;
                    coords[0]++;
                    break;
                case warthog::jps3D::SOUTHWEST:
                    coords[1]++;
                    coords[0]--;
                    break;
                case warthog::jps3D::UPNORTH:
                    coords[2]++;
                    coords[1]--;
                    break;
                case warthog::jps3D::UPSOUTH:
                    coords[2]++;
                    coords[1]++;
                    break;
                case warthog::jps3D::UPEAST:
                    coords[2]++;
                    coords[0]++;
                    break;
                case warthog::jps3D::UPWEST:
                    coords[2]++;
                    coords[0]--;
                    break;
                case warthog::jps3D::DOWNNORTH:
                    coords[2]--;
                    coords[1]--;
                    break;
                case warthog::jps3D::DOWNSOUTH:
                    coords[2]--;
                    coords[1]++;
                    break;
                case warthog::jps3D::DOWNEAST:
                    coords[2]--;
                    coords[0]++;
                    break;
                case warthog::jps3D::DOWNWEST:
                    coords[2]--;
                    coords[0]--;
                    break;
                case warthog::jps3D::UPNORTHEAST:
                    coords[2]++;
                    coords[1]--;
                    coords[0]++;
                    break;
                case warthog::jps3D::UPNORTHWEST:
                    coords[2]++;
                    coords[1]--;
                    coords[0]--;
                    break;
                case warthog::jps3D::UPSOUTHEAST:
                    coords[2]++;
                    coords[1]++;
                    coords[0]++;
                    break;
                case warthog::jps3D::UPSOUTHWEST:
                    coords[2]++;
                    coords[1]++;
                    coords[0]--;
                    break;
                case warthog::jps3D::DOWNNORTHEAST:
                    coords[2]--;
                    coords[1]--;
                    coords[0]++;
                    break;
                case warthog::jps3D::DOWNNORTHWEST:
                    coords[2]--;
                    coords[1]--;
                    coords[0]--;
                    break;
                case warthog::jps3D::DOWNSOUTHEAST:
                    coords[2]--;
                    coords[1]++;
                    coords[0]++;
                    break;
                case warthog::jps3D::DOWNSOUTHWEST:
                    coords[2]--;
                    coords[1]++;
                    coords[0]--;
                    break;
                default:
                    break;
            }
            return coords;
        }

        warthog::jps3D::direction
        coords_to_dir(std::vector<int> coords)
        {
            switch(coords[2])
            {
                case 1: // Up
                    switch(coords[1])
                    {
                        case 1: // South
                            switch(coords[0])
                            {
                                case 1: // East
                                    return warthog::jps3D::UPSOUTHEAST;
                                case 2: // West
                                    return warthog::jps3D::UPSOUTHWEST;
                                default:
                                    return warthog::jps3D::UPSOUTH;
                            }
                        case -1: // North
                            switch(coords[0])
                            {
                                case 1: // East
                                    return warthog::jps3D::UPNORTHEAST;
                                case 2: // West
                                    return warthog::jps3D::UPNORTHWEST;
                                default:
                                    return warthog::jps3D::UPNORTH;
                            }
                        default:
                            switch(coords[0])
                            {
                                case 1: // East
                                    return warthog::jps3D::UPEAST;
                                case 2: // West
                                    return warthog::jps3D::UPWEST;
                                default:
                                    return warthog::jps3D::UP;
                            }
                    }
                case -1: // Down
                    switch(coords[1])
                    {
                        case 1: // South
                            switch(coords[0])
                            {
                                case 1: // East
                                    return warthog::jps3D::DOWNSOUTHEAST;
                                case 2: // West
                                    return warthog::jps3D::DOWNSOUTHWEST;
                                default:
                                    return warthog::jps3D::DOWNSOUTH;
                            }
                        case -1: // North
                            switch(coords[0])
                            {
                                case 1: // East
                                    return warthog::jps3D::DOWNNORTHEAST;
                                case 2: // West
                                    return warthog::jps3D::DOWNNORTHWEST;
                                default:
                                    return warthog::jps3D::DOWNNORTH;
                            }
                        default:
                            switch(coords[0])
                            {
                                case 1: // East
                                    return warthog::jps3D::DOWNEAST;
                                case 2: // West
                                    return warthog::jps3D::DOWNWEST;
                                default:
                                    return warthog::jps3D::DOWN;
                            }
                    }
                default:
                    switch(coords[1])
                        {
                            case 1: // South
                                switch(coords[0])
                                {
                                    case 1: // East
                                        return warthog::jps3D::SOUTHEAST;
                                    case 2: // West
                                        return warthog::jps3D::SOUTHWEST;
                                    default:
                                        return warthog::jps3D::SOUTH;
                                }
                            case -1: // North
                                switch(coords[0])
                                {
                                    case 1: // East
                                        return warthog::jps3D::NORTHEAST;
                                    case 2: // West
                                        return warthog::jps3D::NORTHWEST;
                                    default:
                                        return warthog::jps3D::NORTH;
                                }
                            default:
                                switch(coords[0])
                                {
                                    case 1: // East
                                        return warthog::jps3D::EAST;
                                    case 2: // West
                                        return warthog::jps3D::WEST;
                                    default:
                                        return warthog::jps3D::NONE;
                                }
                        }
                }
        }

        inline void
        flip_axis(std::vector<int>* coords, uint8_t axis, uint8_t w)
        {
            (*coords)[axis] = w - (*coords)[axis] - 1;
        }

        inline void
        flip_diag_axis(std::vector<int>* coords, uint8_t w)
        {
            (*coords)[0] = w - (*coords)[0] - 1;
            (*coords)[1] = w - (*coords)[1] - 1;
        }

        inline void
        rotate_counterclockwise(std::vector<int>* coords, uint8_t w)
        {
            uint8_t tmp = (*coords)[1];
            (*coords)[1] = (*coords)[0];
            (*coords)[0] = w - tmp - 1;
        }

        inline void
        rotate_clockwise(std::vector<int>* coords, uint8_t w)
        {
            uint8_t tmp = (*coords)[1];
            (*coords)[1] = (*coords)[0];
            (*coords)[0] = tmp;
        }

        inline void
        rotate_up(std::vector<int>* coords, uint8_t w)
        {
            uint8_t tmp = (*coords)[2];
            (*coords)[2] = (*coords)[0];
            (*coords)[0] = tmp;
        }

        inline void
        rotate_down(std::vector<int>* coords, uint8_t w)
        {
            uint8_t tmp = (*coords)[0];
            (*coords)[0] = (*coords)[2];
            (*coords)[2] = w - tmp - 1;
        }

        std::vector<int>
        rotate_coords(std::vector<int> coords, warthog::jps3D::direction d)
        {
            uint8_t w = 3;
            uint8_t xaxis = 0;
            uint8_t yaxis = 1;
            uint8_t zaxis = 2;
            switch(d)
            {
                case warthog::jps3D::NORTH:
                    rotate_counterclockwise(&coords, w);
                    break;
                case warthog::jps3D::SOUTH:
                    rotate_clockwise(&coords, w);
                    break;
                case warthog::jps3D::EAST:
                    break;
                case warthog::jps3D::WEST:
                    flip_axis(&coords, xaxis, w);
                    break;
                case warthog::jps3D::UP:
                    rotate_up(&coords, w);
                    break;
                case warthog::jps3D::DOWN:
                    rotate_down(&coords, w);
                    break;
                case warthog::jps3D::NORTHEAST:
                    break;
                case warthog::jps3D::NORTHWEST:
                    rotate_counterclockwise(&coords, w);
                    break;
                case warthog::jps3D::SOUTHEAST:
                    rotate_clockwise(&coords, w);
                    break;
                case warthog::jps3D::SOUTHWEST:
                    flip_diag_axis(&coords, w);
                    break;
                case warthog::jps3D::UPNORTH:
                    rotate_up(&coords, w);
                    break;
                case warthog::jps3D::UPSOUTH:
                    rotate_up(&coords, w);
                    flip_axis(&coords, yaxis, w);
                    break;
                case warthog::jps3D::UPEAST:
                    rotate_up(&coords, w);
                    rotate_clockwise(&coords, w);
                    break;
                case warthog::jps3D::UPWEST:
                    rotate_up(&coords, w);
                    rotate_counterclockwise(&coords, w);
                    break;
                case warthog::jps3D::DOWNNORTH:
                    rotate_down(&coords, w);
                    break;
                case warthog::jps3D::DOWNSOUTH:
                    rotate_down(&coords, w);
                    flip_axis(&coords, yaxis, w);
                    break;
                case warthog::jps3D::DOWNEAST:
                    rotate_up(&coords, w);
                    rotate_clockwise(&coords, w);
                    break;
                case warthog::jps3D::DOWNWEST:
                    rotate_up(&coords, w);
                    rotate_counterclockwise(&coords, w);
                    break;
                case warthog::jps3D::UPNORTHEAST:
                    break;
                case warthog::jps3D::UPNORTHWEST:
                    rotate_counterclockwise(&coords, w);
                    break;
                case warthog::jps3D::UPSOUTHEAST:
                    rotate_clockwise(&coords, w);
                    break;
                case warthog::jps3D::UPSOUTHWEST:
                    flip_diag_axis(&coords, w);
                    break;
                case warthog::jps3D::DOWNNORTHEAST:
                    flip_axis(&coords, zaxis, w);
                    break;
                case warthog::jps3D::DOWNNORTHWEST:
                    flip_axis(&coords, zaxis, w);
                    rotate_counterclockwise(&coords, w);
                    break;
                case warthog::jps3D::DOWNSOUTHEAST:
                    flip_axis(&coords, zaxis, w);
                    rotate_clockwise(&coords, w);
                    break;
                case warthog::jps3D::DOWNSOUTHWEST:
                    flip_axis(&coords, zaxis, w);
                    flip_diag_axis(&coords, w);
                    break;
                default:
                    return coords;
            }
            return coords;
        }

        inline int
        coords_to_label(std::vector<int> coords, std::vector<uint8_t> map)
        {
            return (int)map[(coords[0] + 1) + (coords[1] + 1) * 3 + (coords[2] + 1) * 3 * 3]; // x + w*y + w*w*z
        }

        inline int
        get_rotated_penalty(warthog::jps3D::direction curr_d, warthog::jps3D::direction opt_d, std::vector<uint8_t> map)
        {
            std::vector<int> coords = dir_to_coords(opt_d); // convert direction to coordinate transform
            std::vector<int> rotated_coords = rotate_coords(coords, curr_d); // apply rotation to the coordinate transform to match correct orientation
            int order = coords_to_label(coords, map); // track the priority order defined by the new location
            // warthog::jps3D::direction d = coords_to_dir(coords); // convert new coordinate transform into correct direction
            return order;
        }

        std::vector<uint8_t>
        get_dir_map(warthog::jps3D::direction d)
        {
            switch(d)
            {
                case warthog::jps3D::NORTH:
                    return dirmap_east_;
                case warthog::jps3D::SOUTH:
                    return dirmap_east_;
                case warthog::jps3D::EAST:
                    return dirmap_east_;
                case warthog::jps3D::WEST:
                    return dirmap_east_;
                case warthog::jps3D::UP:
                    return dirmap_east_;
                case warthog::jps3D::DOWN:
                    return dirmap_east_;
                case warthog::jps3D::NORTHEAST:
                    return dirmap_northeast_;
                case warthog::jps3D::NORTHWEST:
                    return dirmap_northeast_;
                case warthog::jps3D::SOUTHEAST:
                    return dirmap_northeast_;
                case warthog::jps3D::SOUTHWEST:
                    return dirmap_northeast_;
                case warthog::jps3D::UPNORTH:
                    return dirmap_northeast_;
                case warthog::jps3D::UPSOUTH:
                    return dirmap_northeast_;
                case warthog::jps3D::UPEAST:
                    return dirmap_northeast_;
                case warthog::jps3D::UPWEST:
                    return dirmap_northeast_;
                case warthog::jps3D::DOWNNORTH:
                    return dirmap_northeast_;
                case warthog::jps3D::DOWNSOUTH:
                    return dirmap_northeast_;
                case warthog::jps3D::DOWNEAST:
                    return dirmap_northeast_;
                case warthog::jps3D::DOWNWEST:
                    return dirmap_northeast_;
                case warthog::jps3D::UPNORTHEAST:
                    return dirmap_upnortheast_;
                case warthog::jps3D::UPNORTHWEST:
                    return dirmap_upnortheast_;
                case warthog::jps3D::UPSOUTHEAST:
                    return dirmap_upnortheast_;
                case warthog::jps3D::UPSOUTHWEST:
                    return dirmap_upnortheast_;
                case warthog::jps3D::DOWNNORTHEAST:
                    return dirmap_upnortheast_;
                case warthog::jps3D::DOWNNORTHWEST:
                    return dirmap_upnortheast_;
                case warthog::jps3D::DOWNSOUTHEAST:
                    return dirmap_upnortheast_;
                case warthog::jps3D::DOWNSOUTHWEST:
                    return dirmap_upnortheast_;
                default:
                    return dirmap_east_;
            }
        }

        inline int
        read_order(warthog::jps3D::direction d, warthog::jps3D::direction inn_d)
        {
            return opt_dir_arr_[get_dir_ind(d)][get_dir_ind(inn_d)];
        }

        inline warthog::jps3D::direction
        get_opt_dir(warthog::jps3D::direction curr_d, 
                warthog::jps3D::direction dir_1, 
                warthog::jps3D::direction dir_root2, 
                warthog::jps3D::direction dir_root3,
                uint32_t steps_1,
                uint32_t steps_root2,
                uint32_t steps_root3)
        {        
            // repeat for each heuristic recommended direction
            int ord_root3 = std::numeric_limits<int>::max();
            int ord_root2 = std::numeric_limits<int>::max();
            int ord_1 = std::numeric_limits<int>::max();
            if(steps_root3 > 0)
            {
                ord_root3 = read_order(curr_d, dir_root3);
            }
            if(steps_root2 > 0)
            {
                ord_root2 = read_order(curr_d, dir_root2);
            }
            if(steps_1 > 0)
            {
                ord_1 = read_order(curr_d, dir_1);
            }
            if(steps_1 == 0 && steps_root2 == 0 && steps_root3 == 0)
            {
                return warthog::jps3D::NONE;
            }
            // determine index of direction with the highest order priority
            int index = 3;
            int best_ord = ord_root3;
            if (ord_root2 < best_ord) { index = 2; best_ord = ord_root2; }
            if (ord_1 < best_ord) { index = 1;}
            
            if(index == 3) { return dir_root3;}
            else if(index == 2) { return dir_root2;}
            else { return dir_1;} // if index == 1
        }

        inline int
        get_dir_ind(warthog::jps3D::direction d)
        {
            return __builtin_ffs(d);
        }

        void
        generate_opt_dir_lookup_array()
        {
            // loop through each direction
            for(uint32_t i = 0; i < 26; i++)
            {
                warthog::jps3D::direction d = (warthog::jps3D::direction) (1 << i);
                
                // loop through each inner direction 
                for(uint32_t j = 0; j < 26; j++)
                {
                    warthog::jps3D::direction inn_d = (warthog::jps3D::direction) (1 << j);
                    int ord;

                    // determine if the direction is straight, 2D or 3D diagonal
                    if(j < 6)
                    {
                        // find its relative penalty 
                        ord = get_rotated_penalty(d, inn_d, dirmap_east_);
                    }
                    else if(j < 18)
                    {
                        // find its relative penalty 
                        ord = get_rotated_penalty(d, inn_d, dirmap_northeast_);
                    }
                    else
                    {
                        // find its relative penalty 
                        ord = get_rotated_penalty(d, inn_d, dirmap_upnortheast_);
                    }

                    // add each direction and relative penalty to inner dictionary
                    opt_dir_arr_[i][j] = ord;
                }
            }
        }

        inline bool
        check_dir_change(warthog::jps3D::direction prev_opt_d,
                        warthog::jps3D::direction opt_d)
        {
            if(prev_opt_d ==  opt_d) { return true; }
            else { return false; }
        }

        uint32_t
        calc_penalty_aux(warthog::jps3D::direction d1, warthog::jps3D::direction d2)
        {
            // We calculate the penalty of moving in direction d2
            // relative to the recommended direction d1
            int x1, y1, z1, x2, y2, z2;
            get_dir_xyz(d1, &x1, &y1, &z1);
            get_dir_xyz(d2, &x2, &y2, &z2);
            return heuristic_->h(1, 1, 1, x2, y2, z2) 
                    + heuristic_->h(x2, y2, z2, x1, y1, z1)
                    - heuristic_->h(1, 1, 1, x1, y1, z1);
        }
        
        inline uint32_t
        calc_penalty(warthog::jps3D::direction d1, warthog::jps3D::direction d2)
        {
            if(d1 == warthog::jps3D::NONE) { return warthog::INF32; }
            return penalty_arr_[get_dir_ind(d1)][get_dir_ind(d2)];
        }

        inline uint32_t
        get_current_steps(warthog::jps3D::direction opt_d, uint32_t mcost, uint32_t steps_1, 
            uint32_t steps_root2, uint32_t steps_root3)
        {
            if(mcost == warthog::DBL_ONE) { return steps_1; }
            else if(mcost == warthog::DBL_ROOT_TWO) { return steps_root2; }
            else { return steps_root3; }
        }

        inline uint32_t
        get_stop_steps(uint32_t penalty, uint32_t penalty_sum, uint32_t scan_lim)
        {
            if(scan_lim < warthog::DBL_ONE) { return 1; }
            uint32_t steps = 31 - (penalty_sum - scan_lim) / penalty;
            if(steps == 0) { return 1; }
            else { return steps; }// add 1 to correct rounding
        }

        void
        generate_penalty_array()
        {
            // loop through each direction
            for(uint32_t i = 0; i < 26; i++)
            {
                warthog::jps3D::direction d = (warthog::jps3D::direction) (1 << i);

                // loop through each inner direction 
                for(uint32_t j = 0; j < 26; j++)
                {
                    warthog::jps3D::direction inn_d = (warthog::jps3D::direction) (1 << j);

                    // find its relative penalty 
                    uint32_t pen = calc_penalty_aux(d, inn_d);

                    // add each direction and relative penalty to inner dictionary
                    penalty_arr_[i][j] = pen;
                }
            }
        }

        void
        get_dir_xyz(warthog::jps3D::direction d, int* x, int* y, int* z)
        {
            switch (d) 
            {
                case warthog::jps3D::DOWNNORTHWEST:
                    *x = 0; *y = 0; *z = 0;
                    break;
                case warthog::jps3D::DOWNNORTH:
                    *x = 1; *y = 0; *z = 0;
                    break;
                case warthog::jps3D::DOWNNORTHEAST:
                    *x = 2; *y = 0; *z = 0;
                    break;
                case warthog::jps3D::DOWNWEST:
                    *x = 0; *y = 1; *z = 0;
                    break;
                case warthog::jps3D::DOWN:
                    *x = 1; *y = 1; *z = 0;
                    break;
                case warthog::jps3D::DOWNEAST:
                    *x = 2; *y = 1; *z = 0;
                    break;
                case warthog::jps3D::DOWNSOUTHWEST:
                    *x = 0; *y = 2; *z = 0;
                    break;
                case warthog::jps3D::DOWNSOUTH:
                    *x = 1; *y = 2; *z = 0;
                    break;
                case warthog::jps3D::DOWNSOUTHEAST:
                    *x = 2; *y = 2; *z = 0;
                    break;
                case warthog::jps3D::NORTHWEST:
                    *x = 0; *y = 0; *z = 1;
                    break;
                case warthog::jps3D::NORTH:
                    *x = 1; *y = 0; *z = 1;
                    break;
                case warthog::jps3D::NORTHEAST:
                    *x = 2; *y = 0; *z = 1;
                    break;
                case warthog::jps3D::WEST:
                    *x = 0; *y = 1; *z = 1;
                    break;
                // skip center of the 3x3x3 expansion
                case warthog::jps3D::EAST:
                    *x = 2; *y = 1; *z = 1;
                    break;
                case warthog::jps3D::SOUTHWEST:
                    *x = 0; *y = 2; *z = 1;
                    break;
                case warthog::jps3D::SOUTH:
                    *x = 1; *y = 2; *z = 1;
                    break;
                case warthog::jps3D::SOUTHEAST:
                    *x = 2; *y = 2; *z = 1;
                    break;
                case warthog::jps3D::UPNORTHWEST:
                    *x = 0; *y = 0; *z = 2;
                    break;
                case warthog::jps3D::UPNORTH:
                    *x = 1; *y = 0; *z = 2;
                    break;
                case warthog::jps3D::UPNORTHEAST:
                    *x = 2; *y = 0; *z = 2;
                    break;
                case warthog::jps3D::UPWEST:
                    *x = 0; *y = 1; *z = 2;
                    break;
                case warthog::jps3D::UP:
                    *x = 1; *y = 1; *z = 2;
                    break;
                case warthog::jps3D::UPEAST:
                    *x = 2; *y = 1; *z = 2;
                    break;
                case warthog::jps3D::UPSOUTHWEST:
                    *x = 0; *y = 2; *z = 2;
                    break;
                case warthog::jps3D::UPSOUTH:
                    *x = 1; *y = 2; *z = 2;
                    break;
                case warthog::jps3D::UPSOUTHEAST:
                    *x = 2; *y = 2; *z = 2;
                    break;
                default:
                    *x = 0; *y = 0; *z = 0;
                    break;
            }
        }

        uint32_t
        take_step_in_dir(uint32_t id, warthog::jps3D::direction d)
        {
            uint32_t corr = 0;
            uint32_t wx = map_->width();
            uint32_t wy = map_->height();
            switch(d)
            {
                case warthog::jps3D::NORTH:
                    corr = -wx;
                    break;
                case warthog::jps3D::SOUTH:
                    corr = wx;
                    break;
                case warthog::jps3D::EAST:
                    corr = 1;
                    break;
                case warthog::jps3D::WEST:
                    corr = -1;
                    break;
                case warthog::jps3D::NORTHEAST:
                    corr = -wx + 1;
                    break;
                case warthog::jps3D::NORTHWEST:
                    corr = -wx - 1;
                    break;
                case warthog::jps3D::SOUTHEAST:
                    corr = wx + 1;
                    break;
                case warthog::jps3D::SOUTHWEST:
                    corr = wx - 1;
                    break;
                case warthog::jps3D::DOWN:
                    corr = -wx*wy;
                    break;
                case warthog::jps3D::DOWNNORTH:
                    corr = -wx*wy - wx;
                    break;
                case warthog::jps3D::DOWNSOUTH:
                    corr = -wx*wy + wx;
                    break;
                case warthog::jps3D::DOWNEAST:
                    corr = -wx*wy + 1;
                    break;
                case warthog::jps3D::DOWNWEST:
                    corr = -wx*wy - 1;
                    break;
                case warthog::jps3D::DOWNNORTHEAST:
                    corr = -wx*wy - wx + 1;
                    break;
                case warthog::jps3D::DOWNNORTHWEST:
                    corr = -wx*wy - wx - 1;
                    break;
                case warthog::jps3D::DOWNSOUTHEAST:
                    corr = -wx*wy + wx + 1;
                    break;
                case warthog::jps3D::DOWNSOUTHWEST:
                    corr = -wx*wy + wx - 1;
                    break;
                case warthog::jps3D::UP:
                    corr = wx*wy;
                    break;
                case warthog::jps3D::UPNORTH:
                    corr = wx*wy - wx;
                    break;
                case warthog::jps3D::UPSOUTH:
                    corr = wx*wy + wx;
                    break;
                case warthog::jps3D::UPEAST:
                    corr = wx*wy + 1;
                    break;
                case warthog::jps3D::UPWEST:
                    corr = wx*wy - 1;
                    break;
                case warthog::jps3D::UPNORTHEAST:
                    corr = wx*wy - wx + 1;
                    break;
                case warthog::jps3D::UPNORTHWEST:
                    corr = wx*wy - wx - 1;
                    break;
                case warthog::jps3D::UPSOUTHEAST:
                    corr = wx*wy + wx + 1;
                    break;
                case warthog::jps3D::UPSOUTHWEST:
                    corr = wx*wy + wx - 1;
                    break;
                default:
                    break;
            }
            return id + corr;
        }

	private:
        uint32_t mcost_arr_[27];
        std::vector<uint8_t> dirmap_east_;
        std::vector<uint8_t> dirmap_northeast_;
        std::vector<uint8_t> dirmap_upnortheast_;
        int opt_dir_arr_[26][26];
        uint32_t penalty_arr_[26][26];


		void
		jump_northwest(uint32_t node_id, uint32_t goal_id,
                uint32_t& jumpnode_id, warthog::cost_t& jumpcost,
                uint32_t steps_root3, uint32_t steps_root2, uint32_t steps_1,
                warthog::jps3D::direction dir_root3, warthog::jps3D::direction dir_root2,
                warthog::jps3D::direction dir_1, uint32_t opt_mcost,
                warthog::jps3D::direction d, uint32_t scan_lim, uint32_t penalty, uint32_t penalty_sum,
                warthog::jps3D::direction prev_opt_d, uint32_t* nodes_scanned);
		void
		jump_northeast(uint32_t node_id, uint32_t goal_id,
                uint32_t& jumpnode_id, warthog::cost_t& jumpcost,
                uint32_t steps_root3, uint32_t steps_root2, uint32_t steps_1,
                warthog::jps3D::direction dir_root3, warthog::jps3D::direction dir_root2,
                warthog::jps3D::direction dir_1, uint32_t opt_mcost,
                warthog::jps3D::direction d, uint32_t scan_lim, uint32_t penalty, uint32_t penalty_sum,
                warthog::jps3D::direction prev_opt_d, uint32_t* nodes_scanned);
		void
		jump_southwest(uint32_t node_id, uint32_t goal_id,
                uint32_t& jumpnode_id, warthog::cost_t& jumpcost,
                uint32_t steps_root3, uint32_t steps_root2, uint32_t steps_1,
                warthog::jps3D::direction dir_root3, warthog::jps3D::direction dir_root2,
                warthog::jps3D::direction dir_1, uint32_t opt_mcost,
                warthog::jps3D::direction d, uint32_t scan_lim, uint32_t penalty, uint32_t penalty_sum,
                warthog::jps3D::direction prev_opt_d, uint32_t* nodes_scanned);
		void
		jump_southeast(uint32_t node_id, uint32_t goal_id,
                uint32_t& jumpnode_id, warthog::cost_t& jumpcost,
                uint32_t steps_root3, uint32_t steps_root2, uint32_t steps_1,
                warthog::jps3D::direction dir_root3, warthog::jps3D::direction dir_root2,
                warthog::jps3D::direction dir_1, uint32_t opt_mcost,
                warthog::jps3D::direction d, uint32_t scan_lim, uint32_t penalty, uint32_t penalty_sum,
                warthog::jps3D::direction prev_opt_d, uint32_t* nodes_scanned);
		void
		jump_north(uint32_t node_id, uint32_t goal_id,
                uint32_t& jumpnode_id, warthog::cost_t& jumpcost,
                uint32_t steps_root3, uint32_t steps_root2, uint32_t steps_1,
                warthog::jps3D::direction dir_root3, warthog::jps3D::direction dir_root2,
                warthog::jps3D::direction dir_1, uint32_t opt_mcost,
                warthog::jps3D::direction d, uint32_t scan_lim, uint32_t penalty, uint32_t penalty_sum,
                warthog::jps3D::direction prev_opt_d, uint32_t* nodes_scanned);
		void
		jump_south(uint32_t node_id, uint32_t goal_id,
                uint32_t& jumpnode_id, warthog::cost_t& jumpcost,
                uint32_t steps_root3, uint32_t steps_root2, uint32_t steps_1,
                warthog::jps3D::direction dir_root3, warthog::jps3D::direction dir_root2,
                warthog::jps3D::direction dir_1, uint32_t opt_mcost,
                warthog::jps3D::direction d, uint32_t scan_lim, uint32_t penalty, uint32_t penalty_sum,
                warthog::jps3D::direction prev_opt_d, uint32_t* nodes_scanned);
		void
		jump_east(uint32_t node_id, uint32_t goal_id,
                uint32_t& jumpnode_id, warthog::cost_t& jumpcost,
                uint32_t steps_root3, uint32_t steps_root2, uint32_t steps_1,
                warthog::jps3D::direction dir_root3, warthog::jps3D::direction dir_root2,
                warthog::jps3D::direction dir_1, uint32_t opt_mcost,
                warthog::jps3D::direction d, uint32_t scan_lim, uint32_t penalty, uint32_t penalty_sum,
                warthog::jps3D::direction prev_opt_d, uint32_t* nodes_scanned);
		void
		jump_west(uint32_t node_id, uint32_t goal_id,
                uint32_t& jumpnode_id, warthog::cost_t& jumpcost,
                uint32_t steps_root3, uint32_t steps_root2, uint32_t steps_1,
                warthog::jps3D::direction dir_root3, warthog::jps3D::direction dir_root2,
                warthog::jps3D::direction dir_1, uint32_t opt_mcost,
                warthog::jps3D::direction d, uint32_t scan_lim, uint32_t penalty, uint32_t penalty_sum,
                warthog::jps3D::direction prev_opt_d, uint32_t* nodes_scanned);
		void
		jump_down(uint32_t node_id, uint32_t goal_id,
                uint32_t& jumpnode_id, warthog::cost_t& jumpcost,
                uint32_t steps_root3, uint32_t steps_root2, uint32_t steps_1,
                warthog::jps3D::direction dir_root3, warthog::jps3D::direction dir_root2,
                warthog::jps3D::direction dir_1, uint32_t opt_mcost,
                warthog::jps3D::direction d, uint32_t scan_lim, uint32_t penalty, uint32_t penalty_sum,
                warthog::jps3D::direction prev_opt_d, uint32_t* nodes_scanned);
		void
		jump_downnorth(uint32_t node_id, uint32_t goal_id,
                uint32_t& jumpnode_id, warthog::cost_t& jumpcost,
                uint32_t steps_root3, uint32_t steps_root2, uint32_t steps_1,
                warthog::jps3D::direction dir_root3, warthog::jps3D::direction dir_root2,
                warthog::jps3D::direction dir_1, uint32_t opt_mcost,
                warthog::jps3D::direction d, uint32_t scan_lim, uint32_t penalty, uint32_t penalty_sum,
                warthog::jps3D::direction prev_opt_d, uint32_t* nodes_scanned);
		void
		jump_downsouth(uint32_t node_id, uint32_t goal_id,
                uint32_t& jumpnode_id, warthog::cost_t& jumpcost,
                uint32_t steps_root3, uint32_t steps_root2, uint32_t steps_1,
                warthog::jps3D::direction dir_root3, warthog::jps3D::direction dir_root2,
                warthog::jps3D::direction dir_1, uint32_t opt_mcost,
                warthog::jps3D::direction d, uint32_t scan_lim, uint32_t penalty, uint32_t penalty_sum,
                warthog::jps3D::direction prev_opt_d, uint32_t* nodes_scanned);
		void
		jump_downeast(uint32_t node_id, uint32_t goal_id,
                uint32_t& jumpnode_id, warthog::cost_t& jumpcost,
                uint32_t steps_root3, uint32_t steps_root2, uint32_t steps_1,
                warthog::jps3D::direction dir_root3, warthog::jps3D::direction dir_root2,
                warthog::jps3D::direction dir_1, uint32_t opt_mcost,
                warthog::jps3D::direction d, uint32_t scan_lim, uint32_t penalty, uint32_t penalty_sum,
                warthog::jps3D::direction prev_opt_d, uint32_t* nodes_scanned);
		void
		jump_downwest(uint32_t node_id, uint32_t goal_id,
                uint32_t& jumpnode_id, warthog::cost_t& jumpcost,
                uint32_t steps_root3, uint32_t steps_root2, uint32_t steps_1,
                warthog::jps3D::direction dir_root3, warthog::jps3D::direction dir_root2,
                warthog::jps3D::direction dir_1, uint32_t opt_mcost,
                warthog::jps3D::direction d, uint32_t scan_lim, uint32_t penalty, uint32_t penalty_sum,
                warthog::jps3D::direction prev_opt_d, uint32_t* nodes_scanned);
		void
		jump_downnortheast(uint32_t node_id, uint32_t goal_id,
                uint32_t& jumpnode_id, warthog::cost_t& jumpcost,
                uint32_t steps_root3, uint32_t steps_root2, uint32_t steps_1,
                warthog::jps3D::direction dir_root3, warthog::jps3D::direction dir_root2,
                warthog::jps3D::direction dir_1, uint32_t opt_mcost,
                warthog::jps3D::direction d, uint32_t scan_lim, uint32_t penalty, uint32_t penalty_sum,
                warthog::jps3D::direction prev_opt_d, uint32_t* nodes_scanned);
		void
		jump_downnorthwest(uint32_t node_id, uint32_t goal_id,
                uint32_t& jumpnode_id, warthog::cost_t& jumpcost,
                uint32_t steps_root3, uint32_t steps_root2, uint32_t steps_1,
                warthog::jps3D::direction dir_root3, warthog::jps3D::direction dir_root2,
                warthog::jps3D::direction dir_1, uint32_t opt_mcost,
                warthog::jps3D::direction d, uint32_t scan_lim, uint32_t penalty, uint32_t penalty_sum,
                warthog::jps3D::direction prev_opt_d, uint32_t* nodes_scanned);
		void
		jump_downsoutheast(uint32_t node_id, uint32_t goal_id,
                uint32_t& jumpnode_id, warthog::cost_t& jumpcost,
                uint32_t steps_root3, uint32_t steps_root2, uint32_t steps_1,
                warthog::jps3D::direction dir_root3, warthog::jps3D::direction dir_root2,
                warthog::jps3D::direction dir_1, uint32_t opt_mcost,
                warthog::jps3D::direction d, uint32_t scan_lim, uint32_t penalty, uint32_t penalty_sum,
                warthog::jps3D::direction prev_opt_d, uint32_t* nodes_scanned);
		void
		jump_downsouthwest(uint32_t node_id, uint32_t goal_id,
                uint32_t& jumpnode_id, warthog::cost_t& jumpcost,
                uint32_t steps_root3, uint32_t steps_root2, uint32_t steps_1,
                warthog::jps3D::direction dir_root3, warthog::jps3D::direction dir_root2,
                warthog::jps3D::direction dir_1, uint32_t opt_mcost,
                warthog::jps3D::direction d, uint32_t scan_lim, uint32_t penalty, uint32_t penalty_sum,
                warthog::jps3D::direction prev_opt_d, uint32_t* nodes_scanned);
		void
		jump_up(uint32_t node_id, uint32_t goal_id,
                uint32_t& jumpnode_id, warthog::cost_t& jumpcost,
                uint32_t steps_root3, uint32_t steps_root2, uint32_t steps_1,
                warthog::jps3D::direction dir_root3, warthog::jps3D::direction dir_root2,
                warthog::jps3D::direction dir_1, uint32_t opt_mcost,
                warthog::jps3D::direction d, uint32_t scan_lim, uint32_t penalty, uint32_t penalty_sum,
                warthog::jps3D::direction prev_opt_d, uint32_t* nodes_scanned);
		void
		jump_upnorth(uint32_t node_id, uint32_t goal_id,
                uint32_t& jumpnode_id, warthog::cost_t& jumpcost,
                uint32_t steps_root3, uint32_t steps_root2, uint32_t steps_1,
                warthog::jps3D::direction dir_root3, warthog::jps3D::direction dir_root2,
                warthog::jps3D::direction dir_1, uint32_t opt_mcost,
                warthog::jps3D::direction d, uint32_t scan_lim, uint32_t penalty, uint32_t penalty_sum,
                warthog::jps3D::direction prev_opt_d, uint32_t* nodes_scanned);
		void
		jump_upsouth(uint32_t node_id, uint32_t goal_id,
                uint32_t& jumpnode_id, warthog::cost_t& jumpcost,
                uint32_t steps_root3, uint32_t steps_root2, uint32_t steps_1,
                warthog::jps3D::direction dir_root3, warthog::jps3D::direction dir_root2,
                warthog::jps3D::direction dir_1, uint32_t opt_mcost,
                warthog::jps3D::direction d, uint32_t scan_lim, uint32_t penalty, uint32_t penalty_sum,
                warthog::jps3D::direction prev_opt_d, uint32_t* nodes_scanned);
		void
		jump_upeast(uint32_t node_id, uint32_t goal_id,
                uint32_t& jumpnode_id, warthog::cost_t& jumpcost,
                uint32_t steps_root3, uint32_t steps_root2, uint32_t steps_1,
                warthog::jps3D::direction dir_root3, warthog::jps3D::direction dir_root2,
                warthog::jps3D::direction dir_1, uint32_t opt_mcost,
                warthog::jps3D::direction d, uint32_t scan_lim, uint32_t penalty, uint32_t penalty_sum,
                warthog::jps3D::direction prev_opt_d, uint32_t* nodes_scanned);
		void
		jump_upwest(uint32_t node_id, uint32_t goal_id,
                uint32_t& jumpnode_id, warthog::cost_t& jumpcost,
                uint32_t steps_root3, uint32_t steps_root2, uint32_t steps_1,
                warthog::jps3D::direction dir_root3, warthog::jps3D::direction dir_root2,
                warthog::jps3D::direction dir_1, uint32_t opt_mcost,
                warthog::jps3D::direction d, uint32_t scan_lim, uint32_t penalty, uint32_t penalty_sum,
                warthog::jps3D::direction prev_opt_d, uint32_t* nodes_scanned);
		void
		jump_upnortheast(uint32_t node_id, uint32_t goal_id,
                uint32_t& jumpnode_id, warthog::cost_t& jumpcost,
                uint32_t steps_root3, uint32_t steps_root2, uint32_t steps_1,
                warthog::jps3D::direction dir_root3, warthog::jps3D::direction dir_root2,
                warthog::jps3D::direction dir_1, uint32_t opt_mcost,
                warthog::jps3D::direction d, uint32_t scan_lim, uint32_t penalty, uint32_t penalty_sum,
                warthog::jps3D::direction prev_opt_d, uint32_t* nodes_scanned);
		void
		jump_upnorthwest(uint32_t node_id, uint32_t goal_id,
                uint32_t& jumpnode_id, warthog::cost_t& jumpcost,
                uint32_t steps_root3, uint32_t steps_root2, uint32_t steps_1,
                warthog::jps3D::direction dir_root3, warthog::jps3D::direction dir_root2,
                warthog::jps3D::direction dir_1, uint32_t opt_mcost,
                warthog::jps3D::direction d, uint32_t scan_lim, uint32_t penalty, uint32_t penalty_sum,
                warthog::jps3D::direction prev_opt_d, uint32_t* nodes_scanned);
		void
		jump_upsoutheast(uint32_t node_id, uint32_t goal_id,
                uint32_t& jumpnode_id, warthog::cost_t& jumpcost,
                uint32_t steps_root3, uint32_t steps_root2, uint32_t steps_1,
                warthog::jps3D::direction dir_root3, warthog::jps3D::direction dir_root2,
                warthog::jps3D::direction dir_1, uint32_t opt_mcost,
                warthog::jps3D::direction d, uint32_t scan_lim, uint32_t penalty, uint32_t penalty_sum,
                warthog::jps3D::direction prev_opt_d, uint32_t* nodes_scanned);
		void
		jump_upsouthwest(uint32_t node_id, uint32_t goal_id,
                uint32_t& jumpnode_id, warthog::cost_t& jumpcost,
                uint32_t steps_root3, uint32_t steps_root2, uint32_t steps_1,
                warthog::jps3D::direction dir_root3, warthog::jps3D::direction dir_root2,
                warthog::jps3D::direction dir_1, uint32_t opt_mcost,
                warthog::jps3D::direction d, uint32_t scan_lim, uint32_t penalty, uint32_t penalty_sum,
                warthog::jps3D::direction prev_opt_d, uint32_t* nodes_scanned);
                
		// these versions can be passed a map parameter to
		// use when jumping. they allow switching between
		// map_ and rmap_ (a rotated counterpart).
		void
		__jump_east(uint32_t node_id, uint32_t goal_id, 
				uint32_t& jumpnode_id, warthog::cost_t& jumpcost, 
				warthog::voxelmap* mymap, 
                uint32_t steps_root3, uint32_t steps_root2, 
                uint32_t steps_1, warthog::jps3D::direction dir_root3,
                warthog::jps3D::direction dir_root2, warthog::jps3D::direction dir_1,
                uint32_t opt_mcost, warthog::jps3D::direction d,
                uint32_t scan_lim, uint32_t penalty, uint32_t penalty_sum, 
                warthog::jps3D::direction prev_opt_d, uint32_t* nodes_scanned);
		void
		__jump_west(uint32_t node_id, uint32_t goal_id, 
				uint32_t& jumpnode_id, warthog::cost_t& jumpcost, 
				warthog::voxelmap* mymap, 
                uint32_t steps_root3, uint32_t steps_root2, 
                uint32_t steps_1, warthog::jps3D::direction dir_root3,
                warthog::jps3D::direction dir_root2, warthog::jps3D::direction dir_1,
                uint32_t opt_mcost, warthog::jps3D::direction d,
                uint32_t scan_lim, uint32_t penalty, uint32_t penalty_sum, 
                warthog::jps3D::direction prev_opt_d, uint32_t* nodes_scanned);
		void
		__jump_north(uint32_t node_id, uint32_t goal_id, 
				uint32_t& jumpnode_id, warthog::cost_t& jumpcost, 
				warthog::voxelmap* mymap, 
                uint32_t steps_root3, uint32_t steps_root2, 
                uint32_t steps_1, warthog::jps3D::direction dir_root3,
                warthog::jps3D::direction dir_root2, warthog::jps3D::direction dir_1,
                uint32_t opt_mcost, warthog::jps3D::direction d,
                uint32_t scan_lim, uint32_t penalty, uint32_t penalty_sum, 
                warthog::jps3D::direction prev_opt_d, uint32_t* nodes_scanned);
		void
		__jump_south(uint32_t node_id, uint32_t goal_id, 
				uint32_t& jumpnode_id, warthog::cost_t& jumpcost, 
				warthog::voxelmap* mymap, 
                uint32_t steps_root3, uint32_t steps_root2, 
                uint32_t steps_1, warthog::jps3D::direction dir_root3,
                warthog::jps3D::direction dir_root2, warthog::jps3D::direction dir_1,
                uint32_t opt_mcost, warthog::jps3D::direction d,
                uint32_t scan_lim, uint32_t penalty, uint32_t penalty_sum, 
                warthog::jps3D::direction prev_opt_d, uint32_t* nodes_scanned);
        void
		__jump_down(uint32_t node_id, uint32_t goal_id, 
				uint32_t& jumpnode_id, warthog::cost_t& jumpcost, 
				warthog::voxelmap* mymap, 
                uint32_t steps_root3, uint32_t steps_root2, 
                uint32_t steps_1, warthog::jps3D::direction dir_root3,
                warthog::jps3D::direction dir_root2, warthog::jps3D::direction dir_1,
                uint32_t opt_mcost, warthog::jps3D::direction d,
                uint32_t scan_lim, uint32_t penalty, uint32_t penalty_sum, 
                warthog::jps3D::direction prev_opt_d, uint32_t* nodes_scanned);
        void
		__jump_up(uint32_t node_id, uint32_t goal_id, 
				uint32_t& jumpnode_id, warthog::cost_t& jumpcost, 
				warthog::voxelmap* mymap, 
                uint32_t steps_root3, uint32_t steps_root2, 
                uint32_t steps_1, warthog::jps3D::direction dir_root3,
                warthog::jps3D::direction dir_root2, warthog::jps3D::direction dir_1,
                uint32_t opt_mcost, warthog::jps3D::direction d,
                uint32_t scan_lim, uint32_t penalty, uint32_t penalty_sum, 
                warthog::jps3D::direction prev_opt_d, uint32_t* nodes_scanned);
        
		inline uint32_t
		map_id_to_rmap_id(uint32_t mapid)
		{
			if(mapid == warthog::INF32) { return mapid; }

			uint32_t x, y, z;
			uint32_t rx, ry, rz;
			map_->to_unpadded_xyz(mapid, x, y, z);
			rx = map_->header_height() - y - 1;
            ry = x;
            rz = z;
			return rmap_->to_padded_id(rx, ry, rz);
		}

		inline uint32_t
		rmap_id_to_map_id(uint32_t rmapid)
		{
			if(rmapid == warthog::INF32) { return rmapid; }

			uint32_t x, y, z;
			uint32_t rx, ry, rz;
			rmap_->to_unpadded_xyz(rmapid, rx, ry, rz);
			x = ry;
			y = rmap_->header_width() - rx - 1;
            z = rz;
			return map_->to_padded_id(x, y, z);
		}

		warthog::voxelmap*
		create_rmap();

        inline uint32_t
		map_id_to_zmap_id(uint32_t mapid)
		{
			if(mapid == warthog::INF32) { return mapid; }

			uint32_t x, y, z;
			uint32_t rx, ry, rz;
			map_->to_unpadded_xyz(mapid, x, y, z);
            rx = z;
			ry = y;
            rz = x;
			return zmap_->to_padded_id(rx, ry, rz);
			// map_->to_unpadded_xyz(mapid, x, y, z);
            // rx = y;
			// ry = map_->header_depth() - z - 1;
            // rz = x;
			// return zmap_->to_padded_id(rx, ry, rz);
		}

		inline uint32_t
		zmap_id_to_map_id(uint32_t zmapid)
		{
			if(zmapid == warthog::INF32) { return zmapid; }

			uint32_t x, y, z;
			uint32_t rx, ry, rz;
			zmap_->to_unpadded_xyz(zmapid, rx, ry, rz);
			x = rz;
			y = ry;
            z = rx;
			return map_->to_padded_id(x, y, z);
		}

        warthog::voxelmap*
        create_zmap();

        inline uint32_t
		map_id_to_xmap_id(uint32_t mapid)
		{
			if(mapid == warthog::INF32) { return mapid; }

			uint32_t x, y, z;
			uint32_t rx, ry, rz;
			map_->to_unpadded_xyz(mapid, x, y, z);
            rx = x;
			ry = z;
            rz = y;
			return xmap_->to_padded_id(rx, ry, rz);
		}

        inline uint32_t
		xmap_id_to_map_id(uint32_t xmapid)
		{
			if(xmapid == warthog::INF32) { return xmapid; }

			uint32_t x, y, z;
			uint32_t rx, ry, rz;
		    xmap_->to_unpadded_xyz(xmapid, rx, ry, rz);
			x = rx;
			y = rz;
            z = ry;
			return map_->to_padded_id(x, y, z);	
		}

        warthog::voxelmap*
        create_xmap();

		warthog::voxelmap* map_;
		warthog::voxelmap* rmap_;
        warthog::voxelmap* zmap_;
        warthog::voxelmap* xmap_;
        warthog::voxel_heuristic* heuristic_;
		//uint32_t jumplimit_;
};

}

#endif

