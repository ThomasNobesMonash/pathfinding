#include "voxelmap.h"
#include "jps3D.h"
#include "online_jump_point_locator26c_csl.h"
#include "helpers.h"

#include <cassert>
#include <climits>
#include <bitset>
#include <cmath>


warthog::online_jump_point_locator26c_csl::online_jump_point_locator26c_csl(warthog::voxelmap* map)
	: map_(map)
{
	rmap_ = create_rmap();
	zmap_ = create_zmap();
	xmap_ = create_xmap();
}

warthog::online_jump_point_locator26c_csl::~online_jump_point_locator26c_csl()
{
	delete rmap_;
	delete zmap_;
    delete xmap_;
}

// create a copy of the grid map which is rotated by 90 degrees clockwise.
// this version will be used when jumping North or South. 
warthog::voxelmap*
warthog::online_jump_point_locator26c_csl::create_rmap()
{
	uint32_t maph = map_->header_depth();
	uint32_t mapwx = map_->header_width();
	uint32_t mapwy = map_->header_height();
	uint32_t rmapwy = mapwx;
	uint32_t rmapwx = mapwy;
    uint32_t rmaph = maph;
	warthog::voxelmap* rmap = new warthog::voxelmap(rmapwx, rmapwy, rmaph);

    for (uint32_t z = 0; z < maph; z++)
    {
        for(uint32_t x = 0; x < mapwx; x++) 
        {
            for(uint32_t y = 0; y < mapwy; y++)
            {
                uint32_t label = map_->get_label(map_->to_padded_id(x, y, z));
                uint32_t rx = ((rmapwx-1) - y);
                uint32_t ry = x;
                uint32_t rz = z;
                uint32_t rid = rmap->to_padded_id(rx, ry, rz);
                rmap->set_label(rid, label);
            }
        }
    }
	
	return rmap;
}

// create a copy of the grid map which is rotated by 90 degrees such that the XZ plane is now the bottom of the cube
// and is then rotated 90 degrees in the new XY plane
// this version will be used when jumping Down or Up and North or South.
warthog::voxelmap*
warthog::online_jump_point_locator26c_csl::create_zmap()
{
	uint32_t maph = map_->header_depth();
	uint32_t mapwx = map_->header_width();
	uint32_t mapwy = map_->header_height();
    // we swap the map depth and width
	uint32_t zmapwy = mapwy;
	uint32_t zmapwx = maph;
    uint32_t zmaph = mapwx; 
	warthog::voxelmap* zmap = new warthog::voxelmap(zmapwx, zmapwy, zmaph);

    for (uint32_t z = 0; z < maph; z++)
    {
        for(uint32_t x = 0; x < mapwx; x++) 
        {
            for(uint32_t y = 0; y < mapwy; y++)
            {
                uint32_t label = map_->get_label(map_->to_padded_id(x, y, z));
                uint32_t rx = z;
                uint32_t ry = y;
                uint32_t rz = x;
                uint32_t rid = zmap->to_padded_id(rx, ry, rz);
                zmap->set_label(rid, label);
            }
        }
    }
	
	return zmap;
}

// create a copy of the grid map which is rotated by 90 degrees such that the XZ plane is now the bottom of the cube
// and is then rotated 90 degrees in the new XY plane
// this version will be used when jumping Down or Up and North or South.
warthog::voxelmap*
warthog::online_jump_point_locator26c_csl::create_xmap()
{
	uint32_t maph = map_->header_depth();
	uint32_t mapwx = map_->header_width();
	uint32_t mapwy = map_->header_height();
    // we swap the map depth and width
	uint32_t xmapwy = mapwy;
	uint32_t xmapwx = maph;
    uint32_t xmaph = mapwx; 
	warthog::voxelmap* xmap = new warthog::voxelmap(xmapwx, xmapwy, xmaph);

    for (uint32_t z = 0; z < maph; z++)
    {
        for(uint32_t x = 0; x < mapwx; x++) 
        {
            for(uint32_t y = 0; y < mapwy; y++)
            {
                uint32_t label = map_->get_label(map_->to_padded_id(x, y, z));
                uint32_t rx = x;
                uint32_t ry = z;
                uint32_t rz = y;
                uint32_t rid = xmap->to_padded_id(rx, ry, rz);
                xmap->set_label(rid, label);
            }
        }
    }
	
	return xmap;
}

// Finds a jump point successor of node (x, y, z) in Direction d.
// Also given is the location of the goal node (goalx, goaly, goalz) for a particular
// search instance. If encountered, the goal node is always returned as a 
// jump point successor.
//
// @return: the id of a jump point successor or warthog::INF32 if no jp exists.
void
warthog::online_jump_point_locator26c_csl::jump(warthog::jps3D::direction d,
        uint32_t node_id, uint32_t goal_id, uint32_t& jumpnode_id,
        warthog::cost_t& jumpcost, uint32_t scan_lim, uint32_t* nodes_scanned)
{
	switch(d)
	{
		case warthog::jps3D::NORTH:
			jump_north(node_id, goal_id, jumpnode_id, jumpcost, scan_lim, nodes_scanned);
			break;
		case warthog::jps3D::SOUTH:
			jump_south(node_id, goal_id, jumpnode_id, jumpcost, scan_lim, nodes_scanned);
			break;
		case warthog::jps3D::EAST:
			jump_east(node_id, goal_id, jumpnode_id, jumpcost, scan_lim, nodes_scanned);
			break;
		case warthog::jps3D::WEST:
			jump_west(node_id, goal_id, jumpnode_id, jumpcost, scan_lim, nodes_scanned);
			break;
		case warthog::jps3D::NORTHEAST:
			jump_northeast(node_id, goal_id, jumpnode_id, jumpcost, scan_lim, nodes_scanned);
			break;
		case warthog::jps3D::NORTHWEST:
			jump_northwest(node_id, goal_id, jumpnode_id, jumpcost, scan_lim, nodes_scanned);
			break;
		case warthog::jps3D::SOUTHEAST:
			jump_southeast(node_id, goal_id, jumpnode_id, jumpcost, scan_lim, nodes_scanned);
			break;
		case warthog::jps3D::SOUTHWEST:
			jump_southwest(node_id, goal_id, jumpnode_id, jumpcost, scan_lim, nodes_scanned);
			break;
        case warthog::jps3D::DOWN:
            jump_down(node_id, goal_id, jumpnode_id, jumpcost, scan_lim, nodes_scanned);
            break;
        case warthog::jps3D::DOWNNORTH:
            jump_downnorth(node_id, goal_id, jumpnode_id, jumpcost, scan_lim, nodes_scanned);
            break;
        case warthog::jps3D::DOWNSOUTH:
            jump_downsouth(node_id, goal_id, jumpnode_id, jumpcost, scan_lim, nodes_scanned);
            break;
        case warthog::jps3D::DOWNEAST:
            jump_downeast(node_id, goal_id, jumpnode_id, jumpcost, scan_lim, nodes_scanned);
            break;
        case warthog::jps3D::DOWNWEST:
            jump_downwest(node_id, goal_id, jumpnode_id, jumpcost, scan_lim, nodes_scanned);
            break;
        case warthog::jps3D::DOWNNORTHEAST:
            jump_downnortheast(node_id, goal_id, jumpnode_id, jumpcost, scan_lim, nodes_scanned);
            break;
        case warthog::jps3D::DOWNNORTHWEST:
            jump_downnorthwest(node_id, goal_id, jumpnode_id, jumpcost, scan_lim, nodes_scanned);
            break;
        case warthog::jps3D::DOWNSOUTHEAST:
            jump_downsoutheast(node_id, goal_id, jumpnode_id, jumpcost, scan_lim, nodes_scanned);
            break;
        case warthog::jps3D::DOWNSOUTHWEST:
            jump_downsouthwest(node_id, goal_id, jumpnode_id, jumpcost, scan_lim, nodes_scanned);
            break;
        case warthog::jps3D::UP:
            jump_up(node_id, goal_id, jumpnode_id, jumpcost, scan_lim, nodes_scanned);
            break;
        case warthog::jps3D::UPNORTH:
            jump_upnorth(node_id, goal_id, jumpnode_id, jumpcost, scan_lim, nodes_scanned);
            break;
        case warthog::jps3D::UPSOUTH:
            jump_upsouth(node_id, goal_id, jumpnode_id, jumpcost, scan_lim, nodes_scanned);
            break;
        case warthog::jps3D::UPEAST:
            jump_upeast(node_id, goal_id, jumpnode_id, jumpcost, scan_lim, nodes_scanned);
            break;
        case warthog::jps3D::UPWEST:
            jump_upwest(node_id, goal_id, jumpnode_id, jumpcost, scan_lim, nodes_scanned);
            break;
        case warthog::jps3D::UPNORTHEAST:
            jump_upnortheast(node_id, goal_id, jumpnode_id, jumpcost, scan_lim, nodes_scanned);
            break;
        case warthog::jps3D::UPNORTHWEST:
            jump_upnorthwest(node_id, goal_id, jumpnode_id, jumpcost, scan_lim, nodes_scanned);
            break;
        case warthog::jps3D::UPSOUTHEAST:
            jump_upsoutheast(node_id, goal_id, jumpnode_id, jumpcost, scan_lim, nodes_scanned);
            break;
        case warthog::jps3D::UPSOUTHWEST:
            jump_upsouthwest(node_id, goal_id, jumpnode_id, jumpcost, scan_lim, nodes_scanned);
            break;
		default:
			break;
	}
}

void
warthog::online_jump_point_locator26c_csl::jump_north(uint32_t node_id,
        uint32_t goal_id, uint32_t& jumpnode_id, warthog::cost_t& jumpcost,
        uint32_t scan_lim, uint32_t* nodes_scanned)
{
	node_id = this->map_id_to_rmap_id(node_id);
	goal_id = this->map_id_to_rmap_id(goal_id);
	__jump_north(node_id, goal_id, jumpnode_id, jumpcost, rmap_, scan_lim, nodes_scanned);
	jumpnode_id = this->rmap_id_to_map_id(jumpnode_id);
}

void
warthog::online_jump_point_locator26c_csl::__jump_north(uint32_t node_id,
        uint32_t goal_id, uint32_t& jumpnode_id, warthog::cost_t& jumpcost, 
        warthog::voxelmap* mymap, uint32_t scan_lim, uint32_t* nodes_scanned)
{
	// jumping north in the original map is the same as jumping
	// east when we use a version of the map rotated 90 degrees.
	__jump_east(node_id, goal_id, jumpnode_id, jumpcost, mymap, scan_lim, nodes_scanned);
}

void
warthog::online_jump_point_locator26c_csl::jump_south(uint32_t node_id,
        uint32_t goal_id, uint32_t& jumpnode_id, warthog::cost_t& jumpcost,
        uint32_t scan_lim, uint32_t* nodes_scanned)
{
	node_id = this->map_id_to_rmap_id(node_id);
	goal_id = this->map_id_to_rmap_id(goal_id);
	__jump_south(node_id, goal_id, jumpnode_id, jumpcost, rmap_, scan_lim, nodes_scanned);
	jumpnode_id = this->rmap_id_to_map_id(jumpnode_id);
}

void
warthog::online_jump_point_locator26c_csl::__jump_south(uint32_t node_id,
        uint32_t goal_id, uint32_t& jumpnode_id, warthog::cost_t& jumpcost, 
        warthog::voxelmap* mymap, uint32_t scan_lim, uint32_t* nodes_scanned)
{
	// jumping north in the original map is the same as jumping
	// west when we use a version of the map rotated 90 degrees.
	__jump_west(node_id, goal_id, jumpnode_id, jumpcost, mymap, scan_lim, nodes_scanned);
}

void
warthog::online_jump_point_locator26c_csl::jump_east(uint32_t node_id,
        uint32_t goal_id, uint32_t& jumpnode_id, warthog::cost_t& jumpcost,
        uint32_t scan_lim, uint32_t* nodes_scanned)
{
	__jump_east(node_id, goal_id, jumpnode_id, jumpcost, map_, scan_lim, nodes_scanned);
}


void
warthog::online_jump_point_locator26c_csl::__jump_east(uint32_t node_id,
        uint32_t goal_id, uint32_t& jumpnode_id, warthog::cost_t& jumpcost, 
        warthog::voxelmap* mymap, uint32_t scan_lim, uint32_t* nodes_scanned)
{
	uint32_t neis[9] = {0, 0, 0, 0, 0, 0, 0, 0, 0};
	bool deadend = false;

    jumpnode_id = node_id;
	while(true)
	{
		// read in tiles from 3 adjacent rows. the curent node 
		// is in the low byte of the middle row
		mymap->get_neighbours_32bit(jumpnode_id, neis); 

		// identity forced neighbours and deadend tiles. 
		// forced neighbours are found in the top or bottom row. they 
		// can be identified as a non-obstacle tile that follows
		// immediately  after an obstacle tile. A dead-end tile is
		// an obstacle found  on the middle row; 
		uint32_t 
		forced_bits = (~neis[0] << 1) & neis[0];    // row above
		forced_bits |= (~neis[2] << 1) & neis[2];   // row below
		forced_bits |= (~neis[4] << 1) & neis[4];   // same row, plane below
		forced_bits |= (~neis[7] << 1) & neis[7];   // same row, plane above
        // diagonal obstacle checks:
        forced_bits |= (~neis[3] << 1) & neis[3];   // row above, plane below
		forced_bits |= (~neis[5] << 1) & neis[5];   // row below, pkane below
		forced_bits |= (~neis[6] << 1) & neis[6];   // row above, plane above
		forced_bits |= (~neis[8] << 1) & neis[8];   // row below, plane above
		uint32_t 
		deadend_bits = ~neis[1];

		// stop if we found any forced or dead-end tiles
		uint32_t stop_bits = (forced_bits | deadend_bits);
		if(stop_bits)
		{
			uint32_t stop_pos = __builtin_ffs(stop_bits)-1; // returns idx+1
			jumpnode_id += stop_pos; 
			deadend = deadend_bits & (1 << stop_pos);
			break;
		}

        // stop if we scan too far
        uint32_t num_steps = jumpnode_id + 31 - node_id; // assume we have already taken 31 steps
        if(num_steps > scan_lim)
        { 
            uint32_t stop_pos = get_stop_steps(num_steps, scan_lim);
            jumpnode_id += stop_pos;
            break; 
        }

        // jump to the last position in the cache. we do not jump past the end
		// in case the last tile from the row above or below is an obstacle.
		// Such a tile, followed by a non-obstacle tile, would yield a forced 
		// neighbour that we don't want to miss.
		jumpnode_id += 31; 
	}

	uint32_t steps = jumpnode_id - node_id;
	uint32_t goal_dist = goal_id - node_id;
    (*nodes_scanned) += steps;
	if(steps > goal_dist)
	{
		jumpnode_id = goal_id;
		jumpcost = goal_dist;
		return;
	}

	if(deadend)
	{
		// number of steps to reach the deadend tile is not
		// correct here since we just inverted neis[1] and then
		// looked for the first set bit. need -1 to fix it.
		steps -= (1 && steps);
		jumpnode_id = warthog::INF32;
	}
	jumpcost = steps;
}

// analogous to ::jump_east 
void
warthog::online_jump_point_locator26c_csl::jump_west(uint32_t node_id,
        uint32_t goal_id, uint32_t& jumpnode_id, warthog::cost_t& jumpcost,
        uint32_t scan_lim, uint32_t* nodes_scanned)
{
	__jump_west(node_id, goal_id, jumpnode_id, jumpcost, map_, scan_lim, nodes_scanned);
}

void
warthog::online_jump_point_locator26c_csl::__jump_west(uint32_t node_id,
        uint32_t goal_id, uint32_t& jumpnode_id, warthog::cost_t& jumpcost, 
        warthog::voxelmap* mymap, uint32_t scan_lim, uint32_t* nodes_scanned)
{
	bool deadend = false;
	uint32_t neis[9] = {0, 0, 0, 0, 0, 0, 0, 0, 0};

	jumpnode_id = node_id;
	while(true)
	{
		// cache 32 tiles from three adjacent rows.
		// current tile is in the high byte of the middle row
		mymap->get_neighbours_upper_32bit(jumpnode_id, neis); 

		// identify forced and dead-end nodes
        uint32_t 
		forced_bits = (~neis[0] >> 1) & neis[0];    // row above
		forced_bits |= (~neis[2] >> 1) & neis[2];   // row below
		forced_bits |= (~neis[4] >> 1) & neis[4];   // same row, plane below
		forced_bits |= (~neis[7] >> 1) & neis[7];   // same row, plane above
        // diagonal obstacle checks:
        forced_bits |= (~neis[3] >> 1) & neis[3];   // row above, plane below
		forced_bits |= (~neis[5] >> 1) & neis[5];   // row below, plane below
		forced_bits |= (~neis[6] >> 1) & neis[6];   // row above, plane above
		forced_bits |= (~neis[8] >> 1) & neis[8];   // row below, plane above
		uint32_t 
		deadend_bits = ~neis[1];

		// stop if we encounter any forced or deadend nodes
		uint32_t stop_bits = (forced_bits | deadend_bits);
		if(stop_bits)
		{
			uint32_t stop_pos = __builtin_clz(stop_bits);
			jumpnode_id -= stop_pos;
			deadend = deadend_bits & (0x80000000 >> stop_pos);
			break;
		}

        // stop if we scan too far
        uint32_t num_steps = node_id - jumpnode_id + 31; // assume we have already taken 31 steps
        if(num_steps > scan_lim)
        { 
            uint32_t stop_pos = get_stop_steps(num_steps, scan_lim);
            jumpnode_id -= stop_pos;
            break;
        }

		// jump to the end of cache. jumping +32 involves checking
		// for forced neis between adjacent sets of contiguous tiles
		jumpnode_id -= 31;
	}

	uint32_t steps = node_id - jumpnode_id;
	uint32_t goal_dist = node_id - goal_id;
    (*nodes_scanned) += steps;

	if(steps > goal_dist)
	{
		jumpnode_id = goal_id;
		jumpcost = goal_dist;
 		return;
	}

	if(deadend)
	{
		// number of steps to reach the deadend tile is not
		// correct here since we just inverted neis[1] and then
		// counted leading zeroes. need -1 to fix it.
		steps -= (1 && steps);
		jumpnode_id = warthog::INF32;
	}
	jumpcost = steps;
}

void
warthog::online_jump_point_locator26c_csl::jump_down(uint32_t node_id,
        uint32_t goal_id, uint32_t& jumpnode_id, warthog::cost_t& jumpcost,
        uint32_t scan_lim, uint32_t* nodes_scanned)
{
    // jumping down in the original map is the same as jumping
    // west when we use the rotated zmap
    node_id = this->map_id_to_zmap_id(node_id);
	goal_id = this->map_id_to_zmap_id(goal_id);
	__jump_down(node_id, goal_id, jumpnode_id, jumpcost, zmap_, scan_lim, nodes_scanned);
	jumpnode_id = this->zmap_id_to_map_id(jumpnode_id);
}

void
warthog::online_jump_point_locator26c_csl::__jump_down(uint32_t node_id,
        uint32_t goal_id, uint32_t& jumpnode_id, warthog::cost_t& jumpcost, 
        warthog::voxelmap* mymap, uint32_t scan_lim, uint32_t* nodes_scanned)
{
	// jumping down in the original map is the same as jumping
	// east when we use a version of the map rotated
	__jump_west(node_id, goal_id, jumpnode_id, jumpcost, mymap, scan_lim, nodes_scanned);
}

// analogous to ::jump_down
void
warthog::online_jump_point_locator26c_csl::jump_up(uint32_t node_id,
        uint32_t goal_id, uint32_t& jumpnode_id, warthog::cost_t& jumpcost,
        uint32_t scan_lim, uint32_t* nodes_scanned)
{
    // jumping down in the original map is the same as jumping
    // east when we use the rotated zmap
	node_id = this->map_id_to_zmap_id(node_id);
	goal_id = this->map_id_to_zmap_id(goal_id);
	__jump_up(node_id, goal_id, jumpnode_id, jumpcost, zmap_, scan_lim, nodes_scanned);
	jumpnode_id = this->zmap_id_to_map_id(jumpnode_id);
}

void
warthog::online_jump_point_locator26c_csl::__jump_up(uint32_t node_id,
        uint32_t goal_id, uint32_t& jumpnode_id, warthog::cost_t& jumpcost, 
        warthog::voxelmap* mymap, uint32_t scan_lim, uint32_t* nodes_scanned)
{
	// jumping up in the original map is the same as jumping
	// west when we use a version of the map rotated
	__jump_east(node_id, goal_id, jumpnode_id, jumpcost, mymap, scan_lim, nodes_scanned);
}

void
warthog::online_jump_point_locator26c_csl::jump_northeast(uint32_t node_id,
        uint32_t goal_id, uint32_t& jumpnode_id, warthog::cost_t& jumpcost,
        uint32_t scan_lim, uint32_t* nodes_scanned)
{
	uint32_t num_steps = 0;

	// first 3 bits of first 3 bytes represent a 3x3 cell of tiles
	// from the grid. next_id at centre. Assume little endian format.
	uint32_t next_id = node_id;
	uint32_t mapw = map_->width();

	// early return if the first diagonal step is invalid
	// (validity of subsequent steps is checked by straight jump functions)
	boost::multiprecision::uint128_t neis = 0;
	map_->get_neighbours(next_id, (uint8_t*)&neis);
	if((neis & 1542) != 1542) { jumpnode_id = warthog::INF32; jumpcost=0; return; }

	// jump a single step at a time (no corner cutting)
	uint32_t rnext_id = map_id_to_rmap_id(next_id);
	uint32_t rgoal_id = map_id_to_rmap_id(goal_id);
	uint32_t rmapw = rmap_->width();

	while(true)
	{
		num_steps++;
		next_id = next_id - mapw + 1;
		rnext_id = rnext_id + rmapw + 1;

        // before searching further, we must identify if the current node 
        // has any forced neighbours -- if so, we do not need to recurse
        boost::multiprecision::uint128_t neis = 0;
        map_->get_neighbours(next_id, (uint8_t*)&neis);
        if((neis & warthog::lnum_US_forcedNE) == 144115188075855872) {break;}
        if((neis & warthog::lnum_US_forcedN) == 144115188075855872) {break;}
        if((neis & 216172782113783808) == 144115188075855872) {break;}
        if((neis & 1108101562368) == 8589934592)  {break;}
        if((neis & 2207613190144) == 8589934592) {break;}
        if((neis & 12884901888) == 8589934592) {break;}

		// recurse straight before stepping again diagonally;
		// (ensures we do not miss any optimal turning points)
		// uint32_t jp_id1, jp_id2;
		uint32_t jp_id1, jp_id2;
		warthog::cost_t cost1, cost2;
		__jump_north(rnext_id, rgoal_id, jp_id1, cost1, rmap_, scan_lim, nodes_scanned);
		if(jp_id1 != warthog::INF32) { break; }
        __jump_east(next_id, goal_id, jp_id2, cost2, map_, scan_lim, nodes_scanned);
		if(jp_id2 != warthog::INF32) { break; }

		// couldn't move in either straight dir; node_id is an obstacle
		if(!(cost1 && cost2)) { next_id = warthog::INF32; break; }

        // stop scanning and return current node at the defined limit
        if(num_steps > scan_lim) { break; }
	}
    (*nodes_scanned) += num_steps;
	jumpnode_id = next_id;
	jumpcost = num_steps * warthog::DBL_ROOT_TWO;
}

void
warthog::online_jump_point_locator26c_csl::jump_northwest(uint32_t node_id,
        uint32_t goal_id, uint32_t& jumpnode_id, warthog::cost_t& jumpcost,
        uint32_t scan_lim, uint32_t* nodes_scanned)
{
	uint32_t num_steps = 0;

	// first 3 bits of first 3 bytes represent a 3x3 cell of tiles
	// from the grid. next_id at centre. Assume little endian format.
	uint32_t next_id = node_id;
	uint32_t mapw = map_->width();

	// early termination (invalid first step)
	boost::multiprecision::uint128_t neis = 0;
	map_->get_neighbours(next_id, (uint8_t*)&neis);
	if((neis & 771) != 771) { jumpnode_id = warthog::INF32; jumpcost = 0; return; }

	// jump a single step at a time (no corner cutting)
	uint32_t rnext_id = map_id_to_rmap_id(next_id);
	uint32_t rgoal_id = map_id_to_rmap_id(goal_id);
	uint32_t rmapw = rmap_->width();

	while(true)
	{
		num_steps++;
		next_id = next_id - mapw - 1;
		rnext_id = rnext_id - rmapw + 1;

        // before searching further, we must identify if the current node 
        // has any forced neighbours -- if so, we do not need to recurse
        boost::multiprecision::uint128_t neis = 0;
        map_->get_neighbours(next_id, (uint8_t*)&neis);
        if((neis & warthog::lnum_US_forcedNW) == 144115188075855872) {break;}
        if((neis & warthog::lnum_US_forcedN) == 144115188075855872) {break;}
        if((neis & 432345564227567616) == 144115188075855872) {break;}
        if((neis & 4406636445696) == 8589934592) {break;}
        if((neis & 2207613190144) == 8589934592) {break;}
        if((neis & 25769803776) == 8589934592) {break;}

		// recurse straight before stepping again diagonally;
		// (ensures we do not miss any optimal turning points)
		uint32_t jp_id1, jp_id2;
		warthog::cost_t cost1, cost2;
        __jump_north(rnext_id, rgoal_id, jp_id1, cost1, rmap_, scan_lim, nodes_scanned);
		if(jp_id1 != warthog::INF32) { break; }
		__jump_west(next_id, goal_id, jp_id2, cost2, map_, scan_lim, nodes_scanned);
		if(jp_id2 != warthog::INF32) { break; }

		// couldn't move in either straight dir; node_id is an obstacle
		if(!(cost1 && cost2)) { next_id = warthog::INF32; break; }
        
        // stop scanning and return current node at the defined limit
        if(num_steps > scan_lim) { break; }
        
	}
	(*nodes_scanned) += num_steps;
	jumpnode_id = next_id;
	jumpcost = num_steps * warthog::DBL_ROOT_TWO;
}

void
warthog::online_jump_point_locator26c_csl::jump_southeast(uint32_t node_id,
        uint32_t goal_id, uint32_t& jumpnode_id, warthog::cost_t& jumpcost,
        uint32_t scan_lim, uint32_t* nodes_scanned)
{
	uint32_t num_steps = 0;

	// first 3 bits of first 3 bytes represent a 3x3 cell of tiles
	// from the grid. next_id at centre. Assume little endian format.
	uint32_t next_id = node_id;
	uint32_t mapw = map_->width();
	
	// early return if the first diagonal step is invalid
	// (validity of subsequent steps is checked by straight jump functions)
	boost::multiprecision::uint128_t neis = 0;
	map_->get_neighbours(next_id, (uint8_t*)&neis);
	if((neis & 394752) != 394752) { jumpnode_id = warthog::INF32; jumpcost = 0; return; }

    // jump a single step at a time (no corner cutting)
	uint32_t rnext_id = map_id_to_rmap_id(next_id);
	uint32_t rgoal_id = map_id_to_rmap_id(goal_id);
	uint32_t rmapw = rmap_->width();

	while(true)
	{
		num_steps++;
		next_id = next_id + mapw + 1;
		rnext_id = rnext_id + rmapw - 1;

        // before searching further, we must identify if the current node 
        // has any forced neighbours -- if so, we do not need to recurse
        boost::multiprecision::uint128_t neis = 0;
        map_->get_neighbours(next_id, (uint8_t*)&neis);
        if((neis & 144396663052566528) == 144115188075855872) {break;}
        if((neis & 144678138029277184) == 144115188075855872) {break;}
        if((neis & 216172782113783808) == 144115188075855872) {break;}
        if((neis & 8606711808) == 8589934592) {break;}
        if((neis & 8623489024) == 8589934592) {break;}
        if((neis & 12884901888) == 8589934592) {break;}

		// recurse straight before stepping again diagonally;
		// (ensures we do not miss any optimal turning points)
		uint32_t jp_id1, jp_id2;
		warthog::cost_t cost1, cost2;
        __jump_south(rnext_id, rgoal_id, jp_id1, cost1, rmap_, scan_lim, nodes_scanned);
		if(jp_id1 != warthog::INF32) { break; }
		__jump_east(next_id, goal_id, jp_id2, cost2, map_, scan_lim, nodes_scanned);
		if(jp_id2 != warthog::INF32) { break; }

		// couldn't move in either straight dir; node_id is an obstacle
		if(!(cost1 && cost2)) { next_id = warthog::INF32; break; }
        
        // stop scanning and return current node at the defined limit
        if(num_steps > scan_lim) { break; }
	}
	(*nodes_scanned) += num_steps;
	jumpnode_id = next_id;
	jumpcost = num_steps * warthog::DBL_ROOT_TWO;
}

void
warthog::online_jump_point_locator26c_csl::jump_southwest(uint32_t node_id,
        uint32_t goal_id, uint32_t& jumpnode_id, warthog::cost_t& jumpcost,
        uint32_t scan_lim, uint32_t* nodes_scanned)
{
	uint32_t num_steps = 0;

	// first 3 bits of first 3 bytes represent a 3x3 cell of tiles
	// from the grid. next_id at centre. Assume little endian format.
	boost::multiprecision::uint128_t neis = 0;
	uint32_t next_id = node_id;
	uint32_t mapw = map_->width();

	// early termination (first step is invalid)
	map_->get_neighbours(next_id, (uint8_t*)&neis);
	if((neis & 197376) != 197376) { jumpnode_id = warthog::INF32; jumpcost = 0; return; }

	// jump a single step (no corner cutting)
	uint32_t rnext_id = map_id_to_rmap_id(next_id);
	uint32_t rgoal_id = map_id_to_rmap_id(goal_id);
	uint32_t rmapw = rmap_->width();

	while(true)
	{
		num_steps++;
		next_id = next_id + mapw - 1;
		rnext_id = rnext_id - rmapw - 1;

        // before searching further, we must identify if the current node 
        // has any forced neighbours -- if so, we do not need to recurse
        boost::multiprecision::uint128_t neis = 0;
        map_->get_neighbours(next_id, (uint8_t*)&neis);
        if((neis & 145241087982698496) == 144115188075855872) {break;}
        if((neis & 144678138029277184) == 144115188075855872) {break;}
        if((neis & 432345564227567616) == 144115188075855872) {break;}
        if((neis & 8657043456) == 8589934592) {break;}
        if((neis & 8623489024) == 8589934592) {break;}
        if((neis & 25769803776) == 8589934592) {break;}

		// recurse straight before stepping again diagonally;
		// (ensures we do not miss any optimal turning points)
		uint32_t jp_id1, jp_id2;
		warthog::cost_t cost1, cost2;
        __jump_south(rnext_id, rgoal_id, jp_id1, cost1, rmap_, scan_lim, nodes_scanned);
		if(jp_id1 != warthog::INF32) { break; }
		__jump_west(next_id, goal_id, jp_id2, cost2, map_, scan_lim, nodes_scanned);
		if(jp_id2 != warthog::INF32) { break; }

		// couldn't move in either straight dir; node_id is an obstacle
		if(!(cost1 && cost2)) { next_id = warthog::INF32; break; }
        
        // stop scanning and return current node at the defined limit
        if(num_steps > scan_lim) { break; }
	}
	(*nodes_scanned) += num_steps;
	jumpnode_id = next_id;
	jumpcost = num_steps * warthog::DBL_ROOT_TWO;
}

void
warthog::online_jump_point_locator26c_csl::jump_downnorth(uint32_t node_id,
        uint32_t goal_id, uint32_t& jumpnode_id, warthog::cost_t& jumpcost,
        uint32_t scan_lim, uint32_t* nodes_scanned)
{
	uint32_t num_steps = 0;

	// first 3 bits of first 3 bytes represent a 3x3 cell of tiles
	// from the grid. next_id at centre. Assume little endian format.
	uint32_t next_id = node_id;
	uint32_t mapwx = map_->width();
	uint32_t mapwy = map_->height();

	// early return if the first diagonal step is invalid
	// (validity of subsequent steps is checked by straight jump functions)
	boost::multiprecision::uint128_t neis = 0;
	map_->get_neighbours(next_id, (uint8_t*)&neis);
	if((neis & 8623489538) != 8623489538) { jumpnode_id = warthog::INF32; jumpcost=0; return; }

    // jump a single step at a time (no corner cutting)
	uint32_t rnext_id = map_id_to_rmap_id(next_id);
	uint32_t rgoal_id = map_id_to_rmap_id(goal_id);
	uint32_t rmapwx = rmap_->width();
	uint32_t rmapwy = rmap_->height();
	uint32_t znext_id = map_id_to_zmap_id(next_id);
	uint32_t zgoal_id = map_id_to_zmap_id(goal_id);
	uint32_t zmapw = zmap_->width();

	while(true)
	{
		num_steps++;
		next_id = next_id - mapwx*mapwy - mapwx;
		rnext_id = rnext_id - rmapwx*rmapwy + 1;
        znext_id = znext_id - zmapw - 1;

        // before searching further, we must identify if the current node 
        // has any forced neighbours -- if so, we do not need to recurse
        boost::multiprecision::uint128_t neis = 0;
        map_->get_neighbours(next_id, (uint8_t*)&neis);
        if((neis & warthog::lnum_US_forcedDNE) == 1024) {break;}
        if((neis & 288230376151712768) == 1024) {break;}
        if((neis & 263168) == 1024) {break;}
        if((neis & warthog::lnum_US_forcedDNW) == 256) {break;}
        if((neis & 72057594037928192) == 256) {break;}
        if((neis & 65792) == 256) {break;}

		// recurse straight before stepping again diagonally;
		// (ensures we do not miss any optimal turning points)
		uint32_t jp_id1, jp_id2;
		warthog::cost_t cost1, cost2;
        __jump_north(rnext_id, rgoal_id, jp_id1, cost1, rmap_, scan_lim, nodes_scanned);
		if(jp_id1 != warthog::INF32) { break; }
		__jump_down(znext_id, zgoal_id, jp_id2, cost2, zmap_, scan_lim, nodes_scanned);
		if(jp_id2 != warthog::INF32) { break; }

		// couldn't move in either straight dir; node_id is an obstacle
		if(!(cost1 && cost2)) { next_id = warthog::INF32; break; }
        
        // stop scanning and return current node at the defined limit
        if(num_steps > scan_lim) { break; }
	}
	(*nodes_scanned) += num_steps;
	jumpnode_id = next_id;
	jumpcost = num_steps * warthog::DBL_ROOT_TWO;
}

void
warthog::online_jump_point_locator26c_csl::jump_downsouth(uint32_t node_id,
        uint32_t goal_id, uint32_t& jumpnode_id, warthog::cost_t& jumpcost,
        uint32_t scan_lim, uint32_t* nodes_scanned)
{
	uint32_t num_steps = 0;

	// first 3 bits of first 3 bytes represent a 3x3 cell of tiles
	// from the grid. next_id at centre. Assume little endian format.
	uint32_t next_id = node_id;
	uint32_t mapwx = map_->width();
	uint32_t mapwy = map_->height();

	// early return if the first diagonal step is invalid
	// (validity of subsequent steps is checked by straight jump functions)
	boost::multiprecision::uint128_t neis = 0;
	map_->get_neighbours(next_id, (uint8_t*)&neis);
	if((neis & 2207613321728) != 2207613321728) { jumpnode_id = warthog::INF32; jumpcost=0; return; }

	// jump a single step at a time (no corner cutting)
	uint32_t rnext_id = map_id_to_rmap_id(next_id);
	uint32_t rgoal_id = map_id_to_rmap_id(goal_id);
	uint32_t rmapwx = rmap_->width();
	uint32_t rmapwy = rmap_->height();
	uint32_t znext_id = map_id_to_zmap_id(next_id);
	uint32_t zgoal_id = map_id_to_zmap_id(goal_id);
	uint32_t zmapw = zmap_->width();

	while(true)
	{
		num_steps++;
        next_id = next_id - mapwx*mapwy + mapwx;
		rnext_id = rnext_id - rmapwx*rmapwy - 1;
        znext_id = znext_id + zmapw - 1;

        // before searching further, we must identify if the current node 
        // has any forced neighbours -- if so, we do not need to recurse
        boost::multiprecision::uint128_t neis = 0;
        map_->get_neighbours(next_id, (uint8_t*)&neis);
        if((neis & 1125899906843648) == 1024) {break;}
        if((neis & 288230376151712768) == 1024) {break;}
        if((neis & 1028) == 1024) {break;}
        if((neis & 281474976710912) == 256) {break;}
        if((neis & 72057594037928192) == 256) {break;}
        if((neis & 257) == 256) {break;}

		// recurse straight before stepping again diagonally;
		// (ensures we do not miss any optimal turning points)
		uint32_t jp_id1, jp_id2;
		warthog::cost_t cost1, cost2;
        __jump_south(rnext_id, rgoal_id, jp_id1, cost1, rmap_, scan_lim, nodes_scanned);
		if(jp_id1 != warthog::INF32) { break; }
		__jump_down(znext_id, zgoal_id, jp_id2, cost2, zmap_, scan_lim, nodes_scanned);
		if(jp_id2 != warthog::INF32) { break; }

		// couldn't move in either straight dir; node_id is an obstacle
		if(!(cost1 && cost2)) { next_id = warthog::INF32; break; }
        
        // stop scanning and return current node at the defined limit
        if(num_steps > scan_lim) { break; }
	}
	(*nodes_scanned) += num_steps;
	jumpnode_id = next_id;
	jumpcost = num_steps * warthog::DBL_ROOT_TWO;
}

void
warthog::online_jump_point_locator26c_csl::jump_downeast(uint32_t node_id,
        uint32_t goal_id, uint32_t& jumpnode_id, warthog::cost_t& jumpcost,
        uint32_t scan_lim, uint32_t* nodes_scanned)
{
	uint32_t num_steps = 0;

	// first 3 bits of first 3 bytes represent a 3x3 cell of tiles
	// from the grid. next_id at centre. Assume little endian format.
	uint32_t next_id = node_id;
	uint32_t mapwx = map_->width();
	uint32_t mapwy = map_->height();

	// early return if the first diagonal step is invalid
	// (validity of subsequent steps is checked by straight jump functions)
	boost::multiprecision::uint128_t neis = 0;
	map_->get_neighbours(next_id, (uint8_t*)&neis);
	if((neis & 25769805312) != 25769805312) { jumpnode_id = warthog::INF32; jumpcost=0; return; }

	// jump a single step at a time (no corner cutting)
	uint32_t znext_id = map_id_to_zmap_id(next_id);
	uint32_t zgoal_id = map_id_to_zmap_id(goal_id);
	uint32_t zmapwx = zmap_->width();
	uint32_t zmapwy = zmap_->height();

	while(true)
	{
		num_steps++;
		next_id = next_id - mapwx*mapwy + 1;
        znext_id = znext_id + zmapwx*zmapwy - 1;

        // before searching further, we must identify if the current node 
        // has any forced neighbours -- if so, we do not need to recurse
        boost::multiprecision::uint128_t neis = 0;
        map_->get_neighbours(next_id, (uint8_t*)&neis);
        if((neis & 281474976710658) == 2) {break;}
        if((neis & 562949953421314) == 2) {break;}
        if((neis & 3) == 2) {break;}
        if((neis & warthog::lnum_US_forcedDE) == 131072) {break;}
        if((neis & warthog::lnum_US_forcedU) == 131072) {break;}
        if((neis & 196608) == 131072) {break;}

		// recurse straight before stepping again diagonally;
		// (ensures we do not miss any optimal turning points)
		uint32_t jp_id1, jp_id2;
		warthog::cost_t cost1, cost2;
        __jump_east(next_id, goal_id, jp_id1, cost1, map_, scan_lim, nodes_scanned);
		if(jp_id1 != warthog::INF32) { break; }
		__jump_down(znext_id, zgoal_id, jp_id2, cost2, zmap_, scan_lim, nodes_scanned);
		if(jp_id2 != warthog::INF32) { break; }

		// couldn't move in either straight dir; node_id is an obstacle
		if(!(cost1 && cost2)) { next_id = warthog::INF32; break; }
        
        // stop scanning and return current node at the defined limit
        if(num_steps > scan_lim) { break; }
	}
	(*nodes_scanned) += num_steps;
	jumpnode_id = next_id;
	jumpcost = num_steps * warthog::DBL_ROOT_TWO;
}

void
warthog::online_jump_point_locator26c_csl::jump_downwest(uint32_t node_id,
        uint32_t goal_id, uint32_t& jumpnode_id, warthog::cost_t& jumpcost,
        uint32_t scan_lim, uint32_t* nodes_scanned)
{
	uint32_t num_steps = 0;

	// first 3 bits of first 3 bytes represent a 3x3 cell of tiles
	// from the grid. next_id at centre. Assume little endian format.
	uint32_t next_id = node_id;
	uint32_t mapwx = map_->width();
	uint32_t mapwy = map_->height();

	// early return if the first diagonal step is invalid
	// (validity of subsequent steps is checked by straight jump functions)
	boost::multiprecision::uint128_t neis = 0;
	map_->get_neighbours(next_id, (uint8_t*)&neis);
	if((neis & 12884902656) != 12884902656) { jumpnode_id = warthog::INF32; jumpcost=0; return; }

	// jump a single step at a time (no corner cutting)
	uint32_t znext_id = map_id_to_zmap_id(next_id);
	uint32_t zgoal_id = map_id_to_zmap_id(goal_id);
	uint32_t zmapwx = zmap_->width();
	uint32_t zmapwy = zmap_->height();

	while(true)
	{
		num_steps++;
        next_id = next_id - mapwx*mapwy - 1;
        znext_id = znext_id - zmapwx*zmapwy - 1;

        // before searching further, we must identify if the current node 
        // has any forced neighbours -- if so, we do not need to recurse
        boost::multiprecision::uint128_t neis = 0;
        map_->get_neighbours(next_id, (uint8_t*)&neis);
        if((neis & 1125899906842626) == 2) {break;}
        if((neis & 562949953421314) == 2) {break;}
        if((neis & 6) == 2) {break;}
        if((neis & warthog::lnum_US_forcedDW) == 131072) {break;}
        if((neis & warthog::lnum_US_forcedU) == 131072) {break;}
        if((neis & 393216) == 131072) {break;}

		// recurse straight before stepping again diagonally;
		// (ensures we do not miss any optimal turning points)
		uint32_t jp_id1, jp_id2;
		warthog::cost_t cost1, cost2;
        __jump_west(next_id, goal_id, jp_id1, cost1, map_, scan_lim, nodes_scanned);
		if(jp_id1 != warthog::INF32) { break; }
		__jump_down(znext_id, zgoal_id, jp_id2, cost2, zmap_, scan_lim, nodes_scanned);
		if(jp_id2 != warthog::INF32) { break; }

		// couldn't move in either straight dir; node_id is an obstacle
		if(!(cost1 && cost2)) { next_id = warthog::INF32; break; }
        
        // stop scanning and return current node at the defined limit
        if(num_steps > scan_lim) { break; }
	}
	(*nodes_scanned) += num_steps;
	jumpnode_id = next_id;
	jumpcost = num_steps * warthog::DBL_ROOT_TWO;
}

void
warthog::online_jump_point_locator26c_csl::jump_upnorth(uint32_t node_id,
        uint32_t goal_id, uint32_t& jumpnode_id, warthog::cost_t& jumpcost,
        uint32_t scan_lim, uint32_t* nodes_scanned)
{
	uint32_t num_steps = 0;

	// first 3 bits of first 3 bytes represent a 3x3 cell of tiles
	// from the grid. next_id at centre. Assume little endian format.
	uint32_t next_id = node_id;
	uint32_t mapwx = map_->width();
	uint32_t mapwy = map_->height();

	// early return if the first diagonal step is invalid
	// (validity of subsequent steps is checked by straight jump functions)
	boost::multiprecision::uint128_t neis = 0;
	map_->get_neighbours(next_id, (uint8_t*)&neis);
	if((neis & 144678138029277698) != 144678138029277698) { jumpnode_id = warthog::INF32; jumpcost=0; return; }

	// jump a single step at a time (no corner cutting)
	uint32_t rnext_id = map_id_to_rmap_id(next_id);
	uint32_t rgoal_id = map_id_to_rmap_id(goal_id);
	uint32_t rmapwx = rmap_->width();
	uint32_t rmapwy = rmap_->height();
	uint32_t znext_id = map_id_to_zmap_id(next_id);
	uint32_t zgoal_id = map_id_to_zmap_id(goal_id);
	uint32_t zmapw = zmap_->width();

	while(true)
	{
		num_steps++;
		next_id = next_id + mapwx*mapwy - mapwx;
		rnext_id = rnext_id + rmapwx*rmapwy + 1;
        znext_id = znext_id - zmapw + 1;

        // before searching further, we must identify if the current node 
        // has any forced neighbours -- if so, we do not need to recurse
        boost::multiprecision::uint128_t neis = 0;
        map_->get_neighbours(next_id, (uint8_t*)&neis);
        if((neis & 4398046512128) == 1024) {break;}
        if((neis & 17179870208) == 1024) {break;}
        if((neis & 263168) == 1024) {break;}
        if((neis & 1099511628032) == 256) {break;}
        if((neis & 4294967552) == 256) {break;}
        if((neis & 65792) == 256) {break;}

		// recurse straight before stepping again diagonally;
		// (ensures we do not miss any optimal turning points)
		uint32_t jp_id1, jp_id2;
		warthog::cost_t cost1, cost2;
        __jump_north(rnext_id, rgoal_id, jp_id1, cost1, rmap_, scan_lim, nodes_scanned);
		if(jp_id1 != warthog::INF32) { break; }
		__jump_up(znext_id, zgoal_id, jp_id2, cost2, zmap_, scan_lim, nodes_scanned);
		if(jp_id2 != warthog::INF32) { break; }

		// couldn't move in either straight dir; node_id is an obstacle
		if(!(cost1 && cost2)) { next_id = warthog::INF32; break; }
        
        // stop scanning and return current node at the defined limit
        if(num_steps > scan_lim) { break; }
	}
	(*nodes_scanned) += num_steps;
	jumpnode_id = next_id;
	jumpcost = num_steps * warthog::DBL_ROOT_TWO;
}

void
warthog::online_jump_point_locator26c_csl::jump_upsouth(uint32_t node_id,
        uint32_t goal_id, uint32_t& jumpnode_id, warthog::cost_t& jumpcost,
        uint32_t scan_lim, uint32_t* nodes_scanned)
{
	uint32_t num_steps = 0;

	// first 3 bits of first 3 bytes represent a 3x3 cell of tiles
	// from the grid. next_id at centre. Assume little endian format.
	uint32_t next_id = node_id;
	uint32_t mapwx = map_->width();
	uint32_t mapwy = map_->height();

	// early return if the first diagonal step is invalid
	// (validity of subsequent steps is checked by straight jump functions)
	boost::multiprecision::uint128_t neis = 0;
	map_->get_neighbours(next_id, (uint8_t*)&neis);
	if((neis & warthog::lnum_US) != warthog::lnum_US) { jumpnode_id = warthog::INF32; jumpcost=0; return; }

	// jump a single step at a time (no corner cutting)
	uint32_t rnext_id = map_id_to_rmap_id(next_id);
	uint32_t rgoal_id = map_id_to_rmap_id(goal_id);
	uint32_t rmapwx = rmap_->width();
	uint32_t rmapwy = rmap_->height();
	uint32_t znext_id = map_id_to_zmap_id(next_id);
	uint32_t zgoal_id = map_id_to_zmap_id(goal_id);
	uint32_t zmapw = zmap_->width();

	while(true)
	{
		num_steps++;
		next_id = next_id + mapwx*mapwy + mapwx;
		rnext_id = rnext_id + rmapwx*rmapwy - 1;
        znext_id = znext_id + zmapw + 1;

        // before searching further, we must identify if the current node 
        // has any forced neighbours -- if so, we do not need to recurse
        boost::multiprecision::uint128_t neis = 0;
        map_->get_neighbours(next_id, (uint8_t*)&neis);
        if((neis & 67109888) == 1024) {break;}
        if((neis & 17179870208) == 1024) {break;}
        if((neis & 1028) == 1024) {break;}
        if((neis & 16777472) == 256) {break;}
        if((neis & 4294967552) == 256) {break;}
        if((neis & 257) == 256) {break;}

		// recurse straight before stepping again diagonally;
		// (ensures we do not miss any optimal turning points)
		uint32_t jp_id1, jp_id2;
		warthog::cost_t cost1, cost2;
        __jump_south(rnext_id, rgoal_id, jp_id1, cost1, rmap_, scan_lim, nodes_scanned);
		if(jp_id1 != warthog::INF32) { break; }
		__jump_up(znext_id, zgoal_id, jp_id2, cost2, zmap_, scan_lim, nodes_scanned);
		if(jp_id2 != warthog::INF32) { break; }

		// couldn't move in either straight dir; node_id is an obstacle
		if(!(cost1 && cost2)) { next_id = warthog::INF32; break; }
        
        // stop scanning and return current node at the defined limit
        if(num_steps > scan_lim) { break; }
	}
	(*nodes_scanned) += num_steps;
	jumpnode_id = next_id;
	jumpcost = num_steps * warthog::DBL_ROOT_TWO;
}

void
warthog::online_jump_point_locator26c_csl::jump_upeast(uint32_t node_id,
        uint32_t goal_id, uint32_t& jumpnode_id, warthog::cost_t& jumpcost,
        uint32_t scan_lim, uint32_t* nodes_scanned)
{
    uint32_t num_steps = 0;

	// first 3 bits of first 3 bytes represent a 3x3 cell of tiles
	// from the grid. next_id at centre. Assume little endian format.
	uint32_t next_id = node_id;
	uint32_t mapwx = map_->width();
	uint32_t mapwy = map_->height();

	// early return if the first diagonal step is invalid
	// (validity of subsequent steps is checked by straight jump functions)
	boost::multiprecision::uint128_t neis = 0;
	map_->get_neighbours(next_id, (uint8_t*)&neis);
	if((neis & 432345564227569152) != 432345564227569152) { jumpnode_id = warthog::INF32; jumpcost=0; return; }

	// jump a single step at a time (no corner cutting)
	uint32_t znext_id = map_id_to_zmap_id(next_id);
	uint32_t zgoal_id = map_id_to_zmap_id(goal_id);
	uint32_t zmapwx = zmap_->width();
	uint32_t zmapwy = zmap_->height();

	while(true)
	{
		num_steps++;
		next_id = next_id + mapwx*mapwy + 1;
        znext_id = znext_id + zmapwx*zmapwy + 1;

        // before searching further, we must identify if the current node 
        // has any forced neighbours -- if so, we do not need to recurse
        boost::multiprecision::uint128_t neis = 0;
        map_->get_neighbours(next_id, (uint8_t*)&neis);
        if((neis & 16777218) == 2) {break;}
        if((neis & 33554434) == 2) {break;}
        if((neis & 3) == 2) {break;}
        if((neis & 1099511758848) == 131072) {break;}
        if((neis & 2199023386624) == 131072) {break;}
        if((neis & 196608) == 131072) {break;}

		// recurse straight before stepping again diagonally;
		// (ensures we do not miss any optimal turning points)
		uint32_t jp_id1, jp_id2;
		warthog::cost_t cost1, cost2;
        __jump_east(next_id, goal_id, jp_id1, cost1, map_, scan_lim, nodes_scanned);
		if(jp_id1 != warthog::INF32) { break; }
		__jump_up(znext_id, zgoal_id, jp_id2, cost2, zmap_, scan_lim, nodes_scanned);
		if(jp_id2 != warthog::INF32) { break; }

		// couldn't move in either straight dir; node_id is an obstacle
		if(!(cost1 && cost2)) { next_id = warthog::INF32; break; }
        
        // stop scanning and return current node at the defined limit
        if(num_steps > scan_lim) { break; }
	}
	(*nodes_scanned) += num_steps;
	jumpnode_id = next_id;
	jumpcost = num_steps * warthog::DBL_ROOT_TWO;
}

void
warthog::online_jump_point_locator26c_csl::jump_upwest(uint32_t node_id,
        uint32_t goal_id, uint32_t& jumpnode_id, warthog::cost_t& jumpcost,
        uint32_t scan_lim, uint32_t* nodes_scanned)
{
    uint32_t num_steps = 0;

	// first 3 bits of first 3 bytes represent a 3x3 cell of tiles
	// from the grid. next_id at centre. Assume little endian format.
	uint32_t next_id = node_id;
	uint32_t mapwx = map_->width();
	uint32_t mapwy = map_->height();

	// early return if the first diagonal step is invalid
	// (validity of subsequent steps is checked by straight jump functions)
	boost::multiprecision::uint128_t neis = 0;
	map_->get_neighbours(next_id, (uint8_t*)&neis);
	if((neis & 216172782113784576) != 216172782113784576) { jumpnode_id = warthog::INF32; jumpcost=0; return; }

	// jump a single step at a time (no corner cutting)
	uint32_t znext_id = map_id_to_zmap_id(next_id);
	uint32_t zgoal_id = map_id_to_zmap_id(goal_id);
	uint32_t zmapwx = zmap_->width();
	uint32_t zmapwy = zmap_->height();

	while(true)
	{
		num_steps++;
		next_id = next_id + mapwx*mapwy - 1;
        znext_id = znext_id - zmapwx*zmapwy + 1;

        // before searching further, we must identify if the current node 
        // has any forced neighbours -- if so, we do not need to recurse
        boost::multiprecision::uint128_t neis = 0;
        map_->get_neighbours(next_id, (uint8_t*)&neis);
        if((neis & 67108866) == 2) {break;}
        if((neis & 33554434) == 2) {break;}
        if((neis & 6) == 2) {break;}
        if((neis & 4398046642176) == 131072) {break;}
        if((neis & 2199023386624) == 131072) {break;}
        if((neis & 393216) == 131072) {break;}

		// recurse straight before stepping again diagonally;
		// (ensures we do not miss any optimal turning points)
		uint32_t jp_id1, jp_id2;
		warthog::cost_t cost1, cost2;
        __jump_west(next_id, goal_id, jp_id1, cost1, map_, scan_lim, nodes_scanned);
		if(jp_id1 != warthog::INF32) { break; }
		__jump_up(znext_id, zgoal_id, jp_id2, cost2, zmap_, scan_lim, nodes_scanned);
		if(jp_id2 != warthog::INF32) { break; }

		// couldn't move in either straight dir; node_id is an obstacle
		if(!(cost1 && cost2)) { next_id = warthog::INF32; break; }
        
        // stop scanning and return current node at the defined limit
        if(num_steps > scan_lim) { break; }
	}
	(*nodes_scanned) += num_steps;
	jumpnode_id = next_id;
	jumpcost = num_steps * warthog::DBL_ROOT_TWO;
}

void
warthog::online_jump_point_locator26c_csl::jump_downnortheast(uint32_t node_id,
        uint32_t goal_id, uint32_t& jumpnode_id, warthog::cost_t& jumpcost,
        uint32_t scan_lim, uint32_t* nodes_scanned)
{
    uint32_t num_steps = 0;

	// first 3 bits of first 3 bytes represent a 3x3 cell of tiles
	// from the grid. next_id at centre. Assume little endian format.
	uint32_t next_id = node_id;
	uint32_t mapwx = map_->width();
	uint32_t mapwy = map_->height();

	// early return if the first diagonal step is invalid
	// (validity of subsequent steps is checked by straight jump functions)
	boost::multiprecision::uint128_t neis = 0;
	map_->get_neighbours(next_id, (uint8_t*)&neis);
	if((neis & 25870468614) != 25870468614) { jumpnode_id = warthog::INF32; jumpcost=0; return; }


	// jump a single step at a time (no corner cutting)
    uint32_t rnext_id = map_id_to_rmap_id(next_id);
	uint32_t rgoal_id = map_id_to_rmap_id(goal_id);
	uint32_t rmapwx = rmap_->width();
	uint32_t rmapwy = rmap_->height();
	uint32_t znext_id = map_id_to_zmap_id(next_id);
	uint32_t zgoal_id = map_id_to_zmap_id(goal_id);
	uint32_t zmapwx = zmap_->width();
	uint32_t zmapwy = zmap_->height();

	while(true)
	{
		num_steps++;
		next_id = next_id - mapwx*mapwy - mapwx + 1;
        rnext_id = rnext_id - rmapwx*rmapwy + rmapwx + 1;
        znext_id = znext_id + zmapwx*zmapwy - zmapwx - 1;

		// recurse straight before stepping again diagonally;
		// (ensures we do not miss any optimal turning points)
		uint32_t jp_id1, jp_id2, jp_id3, jp_id4, jp_id5, jp_id6;
		warthog::cost_t cost1, cost2, cost3, cost4, cost5, cost6;
		__jump_north(rnext_id, rgoal_id, jp_id1, cost1, rmap_, scan_lim, nodes_scanned);            
		if(jp_id1 != warthog::INF32) { break; }
		__jump_east(next_id, goal_id, jp_id2, cost2, map_, scan_lim, nodes_scanned);     
		if(jp_id2 != warthog::INF32) { break; }
        __jump_down(znext_id, zgoal_id, jp_id3, cost3, zmap_, scan_lim, nodes_scanned);     
		if(jp_id3 != warthog::INF32) { break; }
        jump_northeast(next_id, goal_id, jp_id4, cost4, scan_lim, nodes_scanned);     
		if(jp_id4 != warthog::INF32) { break; }
        jump_downnorth(next_id, goal_id, jp_id5, cost5, scan_lim, nodes_scanned);     
		if(jp_id5 != warthog::INF32) { break; }
        jump_downeast(next_id, goal_id, jp_id6, cost6, scan_lim, nodes_scanned);     
		if(jp_id6 != warthog::INF32) { break; }

		// couldn't move in any lessor straight or diagonal; node_id is an obstacle
		if(!(cost1 && cost2 && cost3 && cost4 && cost5 && cost6)) { next_id = warthog::INF32; break; }
        
        // stop scanning and return current node at the defined limit
        if(num_steps > scan_lim) { break; }
	}
	(*nodes_scanned) += num_steps;
	jumpnode_id = next_id;
	jumpcost = num_steps*warthog::DBL_ROOT_THREE;                     
}

void
warthog::online_jump_point_locator26c_csl::jump_downnorthwest(uint32_t node_id,
        uint32_t goal_id, uint32_t& jumpnode_id, warthog::cost_t& jumpcost,
        uint32_t scan_lim, uint32_t* nodes_scanned)
{
    uint32_t num_steps = 0;

	// first 3 bits of first 3 bytes represent a 3x3 cell of tiles
	// from the grid. next_id at centre. Assume little endian format.
	uint32_t next_id = node_id;
	uint32_t mapwx = map_->width();
	uint32_t mapwy = map_->height();

	// early return if the first diagonal step is invalid
	// (validity of subsequent steps is checked by straight jump functions)
	boost::multiprecision::uint128_t neis = 0;
	map_->get_neighbours(next_id, (uint8_t*)&neis);
	if((neis & 12935234307) != 12935234307) { jumpnode_id = warthog::INF32; jumpcost=0; return; }

	// jump a single step at a time (no corner cutting)
    uint32_t rnext_id = map_id_to_rmap_id(next_id);
	uint32_t rgoal_id = map_id_to_rmap_id(goal_id);
	uint32_t rmapwx = rmap_->width();
	uint32_t rmapwy = rmap_->height();
	uint32_t znext_id = map_id_to_zmap_id(next_id);
	uint32_t zgoal_id = map_id_to_zmap_id(goal_id);
	uint32_t zmapwx = zmap_->width();
	uint32_t zmapwy = zmap_->height();

	while(true)
	{
		num_steps++;
		next_id = next_id - mapwx*mapwy - mapwx - 1;
        rnext_id = rnext_id - rmapwx*rmapwy - rmapwx + 1;
        znext_id = znext_id - zmapwx*zmapwy - zmapwx - 1;

		// recurse straight before stepping again diagonally;
		// (ensures we do not miss any optimal turning points)
		uint32_t jp_id1, jp_id2, jp_id3, jp_id4, jp_id5, jp_id6;
		warthog::cost_t cost1, cost2, cost3, cost4, cost5, cost6;
		__jump_north(rnext_id, rgoal_id, jp_id1, cost1, rmap_, scan_lim, nodes_scanned);            
		if(jp_id1 != warthog::INF32) { break; }
		__jump_west(next_id, goal_id, jp_id2, cost2, map_, scan_lim, nodes_scanned);     
		if(jp_id2 != warthog::INF32) { break; }
        __jump_down(znext_id, zgoal_id, jp_id3, cost3, zmap_, scan_lim, nodes_scanned);     
		if(jp_id3 != warthog::INF32) { break; }
        jump_northwest(next_id, goal_id, jp_id4, cost4, scan_lim, nodes_scanned);     
		if(jp_id4 != warthog::INF32) { break; }
        jump_downnorth(next_id, goal_id, jp_id5, cost5, scan_lim, nodes_scanned);     
		if(jp_id5 != warthog::INF32) { break; }
        jump_downwest(next_id, goal_id, jp_id6, cost6, scan_lim, nodes_scanned);     
		if(jp_id6 != warthog::INF32) { break; }

		// couldn't move in any lessor straight or diagonal; node_id is an obstacle
		if(!(cost1 && cost2 && cost3 && cost4 && cost5 && cost6)) { next_id = warthog::INF32; break; }
        
        // stop scanning and return current node at the defined limit
        if(num_steps > scan_lim) { break; }
	}
	(*nodes_scanned) += num_steps;
	jumpnode_id = next_id;
	jumpcost = num_steps*warthog::DBL_ROOT_THREE;                     
}

void
warthog::online_jump_point_locator26c_csl::jump_downsoutheast(uint32_t node_id,
        uint32_t goal_id, uint32_t& jumpnode_id, warthog::cost_t& jumpcost,
        uint32_t scan_lim, uint32_t* nodes_scanned)
{
    uint32_t num_steps = 0;

	// first 3 bits of first 3 bytes represent a 3x3 cell of tiles
	// from the grid. next_id at centre. Assume little endian format.
	uint32_t next_id = node_id;
	uint32_t mapwx = map_->width();
	uint32_t mapwy = map_->height();

	// early return if the first diagonal step is invalid
	// (validity of subsequent steps is checked by straight jump functions)
	boost::multiprecision::uint128_t neis = 0;
	map_->get_neighbours(next_id, (uint8_t*)&neis);
	if((neis & 6622839965184) != 6622839965184) { jumpnode_id = warthog::INF32; jumpcost=0; return; }

	// jump a single step at a time (no corner cutting)
    uint32_t rnext_id = map_id_to_rmap_id(next_id);
	uint32_t rgoal_id = map_id_to_rmap_id(goal_id);
	uint32_t rmapwx = rmap_->width();
	uint32_t rmapwy = rmap_->height();
	uint32_t znext_id = map_id_to_zmap_id(next_id);
	uint32_t zgoal_id = map_id_to_zmap_id(goal_id);
	uint32_t zmapwx= zmap_->width();
	uint32_t zmapwy= zmap_->height();

	while(true)
	{
		num_steps++;
		next_id = next_id - mapwx*mapwy + mapwx + 1;
        rnext_id = rnext_id - rmapwx*rmapwy + rmapwx - 1;
        znext_id = znext_id + zmapwx*zmapwy + zmapwx - 1;

		// recurse straight before stepping again diagonally;
		// (ensures we do not miss any optimal turning points)
		uint32_t jp_id1, jp_id2, jp_id3, jp_id4, jp_id5, jp_id6;
		warthog::cost_t cost1, cost2, cost3, cost4, cost5, cost6;
		__jump_south(rnext_id, rgoal_id, jp_id1, cost1, rmap_, scan_lim, nodes_scanned);            
		if(jp_id1 != warthog::INF32) { break; }
		__jump_east(next_id, goal_id, jp_id2, cost2, map_, scan_lim, nodes_scanned);     
		if(jp_id2 != warthog::INF32) { break; }
        __jump_down(znext_id, zgoal_id, jp_id3, cost3, zmap_, scan_lim, nodes_scanned);     
		if(jp_id3 != warthog::INF32) { break; }
        jump_southeast(next_id, goal_id, jp_id4, cost4, scan_lim, nodes_scanned);     
		if(jp_id4 != warthog::INF32) { break; }
        jump_downsouth(next_id, goal_id, jp_id5, cost5, scan_lim, nodes_scanned);     
		if(jp_id5 != warthog::INF32) { break; }
        jump_downeast(next_id, goal_id, jp_id6, cost6, scan_lim, nodes_scanned);     
		if(jp_id6 != warthog::INF32) { break; }

		// couldn't move in any lessor straight or diagonal; node_id is an obstacle
		if(!(cost1 && cost2 && cost3 && cost4 && cost5 && cost6)) { next_id = warthog::INF32; break; }
        
        // stop scanning and return current node at the defined limit
        if(num_steps > scan_lim) { break; }
	}
	(*nodes_scanned) += num_steps;
	jumpnode_id = next_id;
	jumpcost = num_steps*warthog::DBL_ROOT_THREE;                     
}

void
warthog::online_jump_point_locator26c_csl::jump_downsouthwest(uint32_t node_id,
        uint32_t goal_id, uint32_t& jumpnode_id, warthog::cost_t& jumpcost,
        uint32_t scan_lim, uint32_t* nodes_scanned)
{
    uint32_t num_steps = 0;

	// first 3 bits of first 3 bytes represent a 3x3 cell of tiles
	// from the grid. next_id at centre. Assume little endian format.
	uint32_t next_id = node_id;
	uint32_t mapwx = map_->width();
	uint32_t mapwy = map_->height();

	// early return if the first diagonal step is invalid
	// (validity of subsequent steps is checked by straight jump functions)
	boost::multiprecision::uint128_t neis = 0;
	map_->get_neighbours(next_id, (uint8_t*)&neis);
	if((neis & 3311419982592) != 3311419982592) { jumpnode_id = warthog::INF32; jumpcost=0; return; }

	// jump a single step at a time (no corner cutting)
    uint32_t rnext_id = map_id_to_rmap_id(next_id);
	uint32_t rgoal_id = map_id_to_rmap_id(goal_id);
	uint32_t rmapwx = rmap_->width();
	uint32_t rmapwy = rmap_->height();
	uint32_t znext_id = map_id_to_zmap_id(next_id);
	uint32_t zgoal_id = map_id_to_zmap_id(goal_id);
	uint32_t zmapwx= zmap_->width();
	uint32_t zmapwy= zmap_->height();

	while(true)
	{
		num_steps++;
		next_id = next_id - mapwx*mapwy + mapwx - 1;
        rnext_id = rnext_id - rmapwx*rmapwy - rmapwx - 1;
        znext_id = znext_id - zmapwx*zmapwy + zmapwx - 1;

		// recurse straight before stepping again diagonally;
		// (ensures we do not miss any optimal turning points)
		uint32_t jp_id1, jp_id2, jp_id3, jp_id4, jp_id5, jp_id6;
		warthog::cost_t cost1, cost2, cost3, cost4, cost5, cost6;
		__jump_south(rnext_id, rgoal_id, jp_id1, cost1, rmap_, scan_lim, nodes_scanned);            
		if(jp_id1 != warthog::INF32) { break; }
		__jump_west(next_id, goal_id, jp_id2, cost2, map_, scan_lim, nodes_scanned);     
		if(jp_id2 != warthog::INF32) { break; }
        __jump_down(znext_id, zgoal_id, jp_id3, cost3, zmap_, scan_lim, nodes_scanned);     
		if(jp_id3 != warthog::INF32) { break; }
        jump_southwest(next_id, goal_id, jp_id4, cost4, scan_lim, nodes_scanned);     
		if(jp_id4 != warthog::INF32) { break; }
        jump_downsouth(next_id, goal_id, jp_id5, cost5, scan_lim, nodes_scanned);     
		if(jp_id5 != warthog::INF32) { break; }
        jump_downwest(next_id, goal_id, jp_id6, cost6, scan_lim, nodes_scanned);     
		if(jp_id6 != warthog::INF32) { break; }

		// couldn't move in any lessor straight or diagonal; node_id is an obstacle
		if(!(cost1 && cost2 && cost3 && cost4 && cost5 && cost6)) { next_id = warthog::INF32; break; }
        
        // stop scanning and return current node at the defined limit
        if(num_steps > scan_lim) { break; }
	}
	(*nodes_scanned) += num_steps;
	jumpnode_id = next_id;
	jumpcost = num_steps*warthog::DBL_ROOT_THREE;                     
}

void
warthog::online_jump_point_locator26c_csl::jump_upnortheast(uint32_t node_id,
        uint32_t goal_id, uint32_t& jumpnode_id, warthog::cost_t& jumpcost,
        uint32_t scan_lim, uint32_t* nodes_scanned)
{
    uint32_t num_steps = 0;

	// first 3 bits of first 3 bytes represent a 3x3 cell of tiles
	// from the grid. next_id at centre. Assume little endian format.
	uint32_t next_id = node_id;
	uint32_t mapwx = map_->width();
	uint32_t mapwy = map_->height();

	// early return if the first diagonal step is invalid
	// (validity of subsequent steps is checked by straight jump functions)
	boost::multiprecision::uint128_t neis = 0;
	map_->get_neighbours(next_id, (uint8_t*)&neis);
	if((neis & 434034414087833094) != 434034414087833094) { jumpnode_id = warthog::INF32; jumpcost=0; return; }

	// jump a single step at a time (no corner cutting)
    uint32_t rnext_id = map_id_to_rmap_id(next_id);
	uint32_t rgoal_id = map_id_to_rmap_id(goal_id);
	uint32_t rmapwx = rmap_->width();
	uint32_t rmapwy = rmap_->height();
	uint32_t znext_id = map_id_to_zmap_id(next_id);
	uint32_t zgoal_id = map_id_to_zmap_id(goal_id);
	uint32_t zmapwx= zmap_->width();
	uint32_t zmapwy= zmap_->height();

	while(true)
	{
		num_steps++;
		next_id = next_id + mapwx*mapwy - mapwx + 1;
        rnext_id = rnext_id + rmapwx*rmapwy + rmapwx + 1;
        znext_id = znext_id + zmapwx*zmapwy - zmapwx + 1;

		// recurse straight before stepping again diagonally;
		// (ensures we do not miss any optimal turning points)
		uint32_t jp_id1, jp_id2, jp_id3, jp_id4, jp_id5, jp_id6;
		warthog::cost_t cost1, cost2, cost3, cost4, cost5, cost6;
		__jump_north(rnext_id, rgoal_id, jp_id1, cost1, rmap_, scan_lim, nodes_scanned);            
		if(jp_id1 != warthog::INF32) { break; }
		__jump_east(next_id, goal_id, jp_id2, cost2, map_, scan_lim, nodes_scanned);     
		if(jp_id2 != warthog::INF32) { break; }
        __jump_up(znext_id, zgoal_id, jp_id3, cost3, zmap_, scan_lim, nodes_scanned);     
		if(jp_id3 != warthog::INF32) { break; }
        jump_northeast(next_id, goal_id, jp_id4, cost4, scan_lim, nodes_scanned);     
		if(jp_id4 != warthog::INF32) { break; }
        jump_upnorth(next_id, goal_id, jp_id5, cost5, scan_lim, nodes_scanned);     
		if(jp_id5 != warthog::INF32) { break; }
        jump_upeast(next_id, goal_id, jp_id6, cost6, scan_lim, nodes_scanned);     
		if(jp_id6 != warthog::INF32) { break; }

		// couldn't move in any lessor straight or diagonal; node_id is an obstacle
		if(!(cost1 && cost2 && cost3 && cost4 && cost5 && cost6)) { next_id = warthog::INF32; break; }
        
        // stop scanning and return current node at the defined limit
        if(num_steps > scan_lim) { break; }
	}
	(*nodes_scanned) += num_steps;
	jumpnode_id = next_id;
	jumpcost = num_steps*warthog::DBL_ROOT_THREE;                     
}

void
warthog::online_jump_point_locator26c_csl::jump_upnorthwest(uint32_t node_id,
        uint32_t goal_id, uint32_t& jumpnode_id, warthog::cost_t& jumpcost,
        uint32_t scan_lim, uint32_t* nodes_scanned)
{
    uint32_t num_steps = 0;

	// first 3 bits of first 3 bytes represent a 3x3 cell of tiles
	// from the grid. next_id at centre. Assume little endian format.
	uint32_t next_id = node_id;
	uint32_t mapwx = map_->width();
	uint32_t mapwy = map_->height();

	// early return if the first diagonal step is invalid
	// (validity of subsequent steps is checked by straight jump functions)
	boost::multiprecision::uint128_t neis = 0;
	map_->get_neighbours(next_id, (uint8_t*)&neis);
	if((neis & 217017207043916547) != 217017207043916547) { jumpnode_id = warthog::INF32; jumpcost=0; return; }

	// jump a single step at a time (no corner cutting)
    uint32_t rnext_id = map_id_to_rmap_id(next_id);
	uint32_t rgoal_id = map_id_to_rmap_id(goal_id);
	uint32_t rmapwx = rmap_->width();
	uint32_t rmapwy = rmap_->height();
	uint32_t znext_id = map_id_to_zmap_id(next_id);
	uint32_t zgoal_id = map_id_to_zmap_id(goal_id);
	uint32_t zmapwx= zmap_->width();
	uint32_t zmapwy= zmap_->height();

	while(true)
	{
		num_steps++;
		next_id = next_id + mapwx*mapwy - mapwx - 1;
        rnext_id = rnext_id + rmapwx*rmapwy - rmapwx + 1;
        znext_id = znext_id - zmapwx*zmapwy - zmapwx + 1;

		// recurse straight before stepping again diagonally;
		// (ensures we do not miss any optimal turning points)
		uint32_t jp_id1, jp_id2, jp_id3, jp_id4, jp_id5, jp_id6;
		warthog::cost_t cost1, cost2, cost3, cost4, cost5, cost6;
		__jump_north(rnext_id, rgoal_id, jp_id1, cost1, rmap_, scan_lim, nodes_scanned);            
		if(jp_id1 != warthog::INF32) { break; }
		__jump_west(next_id, goal_id, jp_id2, cost2, map_, scan_lim, nodes_scanned);     
		if(jp_id2 != warthog::INF32) { break; }
        __jump_up(znext_id, zgoal_id, jp_id3, cost3, zmap_, scan_lim, nodes_scanned);     
		if(jp_id3 != warthog::INF32) { break; }
        jump_northwest(next_id, goal_id, jp_id4, cost4, scan_lim, nodes_scanned);     
		if(jp_id4 != warthog::INF32) { break; }
        jump_upnorth(next_id, goal_id, jp_id5, cost5, scan_lim, nodes_scanned);     
		if(jp_id5 != warthog::INF32) { break; }
        jump_upwest(next_id, goal_id, jp_id6, cost6, scan_lim, nodes_scanned);     
		if(jp_id6 != warthog::INF32) { break; }

		// couldn't move in any lessor straight or diagonal; node_id is an obstacle
		if(!(cost1 && cost2 && cost3 && cost4 && cost5 && cost6)) { next_id = warthog::INF32; break; }
        
        // stop scanning and return current node at the defined limit
        if(num_steps > scan_lim) { break; }
	}
	(*nodes_scanned) += num_steps;
	jumpnode_id = next_id;
	jumpcost = num_steps*warthog::DBL_ROOT_THREE;                     
}

void
warthog::online_jump_point_locator26c_csl::jump_upsoutheast(uint32_t node_id,
        uint32_t goal_id, uint32_t& jumpnode_id, warthog::cost_t& jumpcost,
        uint32_t scan_lim, uint32_t* nodes_scanned)
{
    uint32_t num_steps = 0;

	// first 3 bits of first 3 bytes represent a 3x3 cell of tiles
	// from the grid. next_id at centre. Assume little endian format.
	uint32_t next_id = node_id;
	uint32_t mapwx = map_->width();
	uint32_t mapwy = map_->height();

	// early return if the first diagonal step is invalid
	// (validity of subsequent steps is checked by straight jump functions)
	boost::multiprecision::uint128_t neis = 0;
	map_->get_neighbours(next_id, (uint8_t*)&neis);
	if((neis & warthog::lnum_USE) != warthog::lnum_USE) { jumpnode_id = warthog::INF32; jumpcost=0; return; }

	// jump a single step at a time (no corner cutting)
    uint32_t rnext_id = map_id_to_rmap_id(next_id);
	uint32_t rgoal_id = map_id_to_rmap_id(goal_id);
	uint32_t rmapwx = rmap_->width();
	uint32_t rmapwy = rmap_->height();
	uint32_t znext_id = map_id_to_zmap_id(next_id);
	uint32_t zgoal_id = map_id_to_zmap_id(goal_id);
	uint32_t zmapwx= zmap_->width();
	uint32_t zmapwy= zmap_->height();

	while(true)
	{
		num_steps++;
		next_id = next_id + mapwx*mapwy + mapwx + 1;
        rnext_id = rnext_id + rmapwx*rmapwy + rmapwx - 1;
        znext_id = znext_id + zmapwx*zmapwy + zmapwx + 1;

		// recurse straight before stepping again diagonally;
		// (ensures we do not miss any optimal turning points)
		uint32_t jp_id1, jp_id2, jp_id3, jp_id4, jp_id5, jp_id6;
		warthog::cost_t cost1, cost2, cost3, cost4, cost5, cost6;
		__jump_south(rnext_id, rgoal_id, jp_id1, cost1, rmap_, scan_lim, nodes_scanned);            
		if(jp_id1 != warthog::INF32) { break; }
		__jump_east(next_id, goal_id, jp_id2, cost2, map_, scan_lim, nodes_scanned);     
		if(jp_id2 != warthog::INF32) { break; }
        __jump_up(znext_id, zgoal_id, jp_id3, cost3, zmap_, scan_lim, nodes_scanned);     
		if(jp_id3 != warthog::INF32) { break; }
        jump_southeast(next_id, goal_id, jp_id4, cost4, scan_lim, nodes_scanned);     
		if(jp_id4 != warthog::INF32) { break; }
        jump_upsouth(next_id, goal_id, jp_id5, cost5, scan_lim, nodes_scanned);     
		if(jp_id5 != warthog::INF32) { break; }
        jump_upeast(next_id, goal_id, jp_id6, cost6, scan_lim, nodes_scanned);     
		if(jp_id6 != warthog::INF32) { break; }
        
        // couldn't move in any lessor straight or diagonal; node_id is an obstacle
		if(!(cost1 && cost2 && cost3 && cost4 && cost5 && cost6)) { next_id = warthog::INF32; break; }
        
        // stop scanning and return current node at the defined limit
        if(num_steps > scan_lim) { break; }
	}
    (*nodes_scanned) += num_steps;
	jumpnode_id = next_id; 
	jumpcost = num_steps*warthog::DBL_ROOT_THREE;                       
}

void
warthog::online_jump_point_locator26c_csl::jump_upsouthwest(uint32_t node_id,
        uint32_t goal_id, uint32_t& jumpnode_id, warthog::cost_t& jumpcost,
        uint32_t scan_lim, uint32_t* nodes_scanned)
{
    uint32_t num_steps = 0;

	// first 3 bits of first 3 bytes represent a 3x3 cell of tiles
	// from the grid. next_id at centre. Assume little endian format.
	uint32_t next_id = node_id;
	uint32_t mapwx = map_->width();
	uint32_t mapwy = map_->height();

	// early return if the first diagonal step is invalid
	// (validity of subsequent steps is checked by straight jump functions)
	boost::multiprecision::uint128_t neis = 0;
	map_->get_neighbours(next_id, (uint8_t*)&neis);
	if((neis &warthog::lnum_USW) != warthog::lnum_USW) { jumpnode_id = warthog::INF32; jumpcost=0; return; }

	// jump a single step at a time (no corner cutting)
    uint32_t rnext_id = map_id_to_rmap_id(next_id);
	uint32_t rgoal_id = map_id_to_rmap_id(goal_id);
	uint32_t rmapwx = rmap_->width();
	uint32_t rmapwy = rmap_->height();
	uint32_t znext_id = map_id_to_zmap_id(next_id);
	uint32_t zgoal_id = map_id_to_zmap_id(goal_id);
	uint32_t zmapwx= zmap_->width();
	uint32_t zmapwy= zmap_->height();

	while(true)
	{
		num_steps++;
		next_id = next_id + mapwx*mapwy + mapwx - 1;
        rnext_id = rnext_id + rmapwx*rmapwy - rmapwx - 1;
        znext_id = znext_id - zmapwx*zmapwy + zmapwx + 1;

		// recurse straight before stepping again diagonally;
		// (ensures we do not miss any optimal turning points)
		uint32_t jp_id1, jp_id2, jp_id3, jp_id4, jp_id5, jp_id6;
		warthog::cost_t cost1, cost2, cost3, cost4, cost5, cost6;
		__jump_south(rnext_id, rgoal_id, jp_id1, cost1, rmap_, scan_lim, nodes_scanned);            
		if(jp_id1 != warthog::INF32) { break; }
		__jump_west(next_id, goal_id, jp_id2, cost2, map_, scan_lim, nodes_scanned);     
		if(jp_id2 != warthog::INF32) { break; }
        __jump_up(znext_id, zgoal_id, jp_id3, cost3, zmap_, scan_lim, nodes_scanned);     
		if(jp_id3 != warthog::INF32) { break; }
        jump_southwest(next_id, goal_id, jp_id4, cost4, scan_lim, nodes_scanned);     
		if(jp_id4 != warthog::INF32) { break; }
        jump_upsouth(next_id, goal_id, jp_id5, cost5, scan_lim, nodes_scanned);     
		if(jp_id5 != warthog::INF32) { break; }
        jump_upwest(next_id, goal_id, jp_id6, cost6, scan_lim, nodes_scanned);     
		if(jp_id6 != warthog::INF32) { break; }

		// couldn't move in any lessor straight or diagonal; node_id is an obstacle
		if(!(cost1 && cost2 && cost3 && cost4 && cost5 && cost6)) { next_id = warthog::INF32; break; }
        
        // stop scanning and return current node at the defined limit
        if(num_steps > scan_lim) { break; }
	}
    (*nodes_scanned) += num_steps;
	jumpnode_id = next_id; 
	jumpcost = num_steps*warthog::DBL_ROOT_THREE;                       
}
