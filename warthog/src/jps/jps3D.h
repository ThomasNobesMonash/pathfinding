#ifndef WARTHOG_JPS3D_H
#define WARTHOG_JPS3D_H

// jps3D.h
//
// This file contains the namespace for common definitions
// required by the various classes that use Jump Points.
// Note that the operations defined here assume corner
// cutting is not allowed.
// For details see:
// [D Harabor and A Grastien, The JPS+ Pathfinding System, SoCS, 2012]
// [Nobes T. et. al., 2022, JPS in 3D, SoCS] 
//
// @author: dharabor, tnobes
// @created: 10/04/2022
//

#include "forward.h"
#include "helpers.h"

#include "stdint.h"
#include <boost/multiprecision/cpp_int.hpp>

namespace warthog
{

namespace jps3D
{

typedef enum
{
	NONE = 0,                   // 00000000 00000000 00000000 00000000
	NORTH = 1,                  // 00000000 00000000 00000000 00000001
	SOUTH = 2,                  // 00000000 00000000 00000000 00000010
	EAST = 4,                   // 00000000 00000000 00000000 00000100
	WEST = 8,                   // 00000000 00000000 00000000 00001000
	NORTHEAST = 16,             // 00000000 00000000 00000000 00010000
	NORTHWEST = 32,             // 00000000 00000000 00000000 00100000
	SOUTHEAST = 64,             // 00000000 00000000 00000000 01000000
	SOUTHWEST = 128,            // 00000000 00000000 00000000 10000000

    DOWN = 256,                 // 00000000 00000000 00000001 00000000
    DOWNNORTH = 512,            // 00000000 00000000 00000010 00000000
    DOWNSOUTH = 1024,           // 00000000 00000000 00000100 00000000
    DOWNEAST = 2048,            // 00000000 00000000 00001000 00000000
    DOWNWEST = 4096,            // 00000000 00000000 00010000 00000000
    DOWNNORTHEAST = 8192,       // 00000000 00000000 00100000 00000000 
    DOWNNORTHWEST = 16384,      // 00000000 00000000 01000000 00000000
    DOWNSOUTHEAST = 32768,      // 00000000 00000000 10000000 00000000
    DOWNSOUTHWEST = 65536,      // 00000000 00000001 00000000 00000000

    UP = 131072,                // 00000000 00000010 00000000 00000000
    UPNORTH = 262144,           // 00000000 00000100 00000010 00000000
    UPSOUTH = 524288,           // 00000000 00001000 00000000 00000000
    UPEAST = 1048576,           // 00000000 00010000 00000000 00000000
    UPWEST = 2097152,           // 00000000 00100000 00000000 00000000
    UPNORTHEAST = 4194304,      // 00000000 01000000 00000000 00000000 
    UPNORTHWEST = 8388608,      // 00000000 10000000 00000000 00000000
    UPSOUTHEAST = 16777216,     // 00000001 00000000 00000000 00000000
    UPSOUTHWEST = 33554432,     // 00000010 00000000 00000000 00000000

    ALL = 67108864,             // 00000100 00000000 00000000 00000000
} direction;

// Computes the set of "forced" directions in which to search for jump points
// from a given location (x, y, z). 
// A neighbour is forced if it cannot be proven that there is at least one 
// alternative optimal path that does not pass through the node (x, y, z).
uint32_t
compute_forced(warthog::jps3D::direction d, boost::multiprecision::uint128_t tiles);

// Computes the set of "natural" neighbours for a given location
// (x, y, z).
uint32_t 
compute_natural(warthog::jps3D::direction d, boost::multiprecision::uint128_t tiles);

// Computes all successors (forced \union natural) of a node (x, y, z).
// This function is specialised for uniform cost grid maps.
//
// @param d: the direction of travel used to reach (x, y, z)
// @param tiles: the 3x3x3 square of cells having (x, y, z) at its centre.
//
// @return an integer representing the set of forced directions.
// Each of the first 26 bits of the returned value, when set, correspond to a
// direction, as defined in warthog::jps3D::direction
inline uint32_t
compute_successors(warthog::jps3D::direction d, boost::multiprecision::uint128_t tiles)
{
	return warthog::jps3D::compute_forced(d, tiles) |
	   	warthog::jps3D::compute_natural(d, tiles);
}

// These rules ALLOW diagonal transitions that cut corners.
uint32_t
compute_forced_cc(warthog::jps3D::direction d, boost::multiprecision::uint128_t tiles);

// These rules ALLOW diagonal transitions that cut corners.
uint32_t 
compute_natural_cc(warthog::jps3D::direction d, boost::multiprecision::uint128_t tiles);

// These rules ALLOW diagonal transitions that cut corners.
inline uint32_t
compute_successors_cc(warthog::jps3D::direction d, boost::multiprecision::uint128_t tiles)
{
	return warthog::jps3D::compute_forced_cc(d, tiles) |
	   	warthog::jps3D::compute_natural_cc(d, tiles);
}

}
}

#endif

