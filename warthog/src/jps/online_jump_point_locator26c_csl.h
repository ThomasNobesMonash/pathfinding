#ifndef WARTHOG_online_jump_point_locator26C_CSL_H
#define WARTHOG_online_jump_point_locator26C_CSL_H

// online_jump_point_locator26c_csl.h
//
// A class wrapper around some code that finds, online, jump point
// successors of an arbitrary nodes in a uniform-cost grid map.
//
// For theoretical details see:
// [Harabor D. and Grastien A, 2011, 
// Online Graph Pruning Pathfinding on Grid Maps, AAAI]
// [Nobes T. et. al., 2022, JPS in 3D, SoCS] 
//
// @author: dharabor, tnobes
// @created: 10/04/2022
//

#include "jps3D.h"
#include "voxelmap.h"
#include "voxel_heuristic.h"
#include <boost/multiprecision/cpp_int.hpp>
#include <unordered_map>
#include <limits>

namespace warthog
{

class online_jump_point_locator26c_csl
{
	public: 
		online_jump_point_locator26c_csl(warthog::voxelmap* map);
		~online_jump_point_locator26c_csl();

		void
		jump(warthog::jps3D::direction d, uint32_t node_id, uint32_t goalid,
				uint32_t& jumpnode_id, warthog::cost_t& jumpcost,
                uint32_t scan_lim, uint32_t* nodes_scanned);

		uint32_t 
		mem()
		{
			return sizeof(this) + 
                rmap_->mem() +
                zmap_->mem() +
                xmap_->mem();
		}

        inline uint32_t
        get_stop_steps(uint32_t num_steps, uint32_t scan_lim)
        {
            if(scan_lim == 0) { return 1; }
            return 31 - (num_steps - scan_lim) + 1;
        }

	private:

		void
		jump_northwest(uint32_t node_id, uint32_t goal_id,
                uint32_t& jumpnode_id, warthog::cost_t& jumpcost,
                uint32_t scan_lim, uint32_t* nodes_scanned);
		void
		jump_northeast(uint32_t node_id, uint32_t goal_id,
                uint32_t& jumpnode_id, warthog::cost_t& jumpcost,
                uint32_t scan_lim, uint32_t* nodes_scanned);
		void
		jump_southwest(uint32_t node_id, uint32_t goal_id,
                uint32_t& jumpnode_id, warthog::cost_t& jumpcost,
                uint32_t scan_lim, uint32_t* nodes_scanned);
		void
		jump_southeast(uint32_t node_id, uint32_t goal_id,
                uint32_t& jumpnode_id, warthog::cost_t& jumpcost,
                uint32_t scan_lim, uint32_t* nodes_scanned);
		void
		jump_north(uint32_t node_id, uint32_t goal_id,
                uint32_t& jumpnode_id, warthog::cost_t& jumpcost,
                uint32_t scan_lim, uint32_t* nodes_scanned);
		void
		jump_south(uint32_t node_id, uint32_t goal_id,
                uint32_t& jumpnode_id, warthog::cost_t& jumpcost,
                uint32_t scan_lim, uint32_t* nodes_scanned);
		void
		jump_east(uint32_t node_id, uint32_t goal_id,
                uint32_t& jumpnode_id, warthog::cost_t& jumpcost,
                uint32_t scan_lim, uint32_t* nodes_scanned);
		void
		jump_west(uint32_t node_id, uint32_t goal_id,
                uint32_t& jumpnode_id, warthog::cost_t& jumpcost,
                uint32_t scan_lim, uint32_t* nodes_scanned);
		void
		jump_down(uint32_t node_id, uint32_t goal_id,
                uint32_t& jumpnode_id, warthog::cost_t& jumpcost,
                uint32_t scan_lim, uint32_t* nodes_scanned);
		void
		jump_downnorth(uint32_t node_id, uint32_t goal_id,
                uint32_t& jumpnode_id, warthog::cost_t& jumpcost,
                uint32_t scan_lim, uint32_t* nodes_scanned);
		void
		jump_downsouth(uint32_t node_id, uint32_t goal_id,
                uint32_t& jumpnode_id, warthog::cost_t& jumpcost,
                uint32_t scan_lim, uint32_t* nodes_scanned);
		void
		jump_downeast(uint32_t node_id, uint32_t goal_id,
                uint32_t& jumpnode_id, warthog::cost_t& jumpcost,
                uint32_t scan_lim, uint32_t* nodes_scanned);
		void
		jump_downwest(uint32_t node_id, uint32_t goal_id,
                uint32_t& jumpnode_id, warthog::cost_t& jumpcost,
                uint32_t scan_lim, uint32_t* nodes_scanned);
		void
		jump_downnortheast(uint32_t node_id, uint32_t goal_id,
                uint32_t& jumpnode_id, warthog::cost_t& jumpcost,
                uint32_t scan_lim, uint32_t* nodes_scanned);
		void
		jump_downnorthwest(uint32_t node_id, uint32_t goal_id,
                uint32_t& jumpnode_id, warthog::cost_t& jumpcost,
                uint32_t scan_lim, uint32_t* nodes_scanned);
		void
		jump_downsoutheast(uint32_t node_id, uint32_t goal_id,
                uint32_t& jumpnode_id, warthog::cost_t& jumpcost,
                uint32_t scan_lim, uint32_t* nodes_scanned);
		void
		jump_downsouthwest(uint32_t node_id, uint32_t goal_id,
                uint32_t& jumpnode_id, warthog::cost_t& jumpcost,
                uint32_t scan_lim, uint32_t* nodes_scanned);
		void
		jump_up(uint32_t node_id, uint32_t goal_id,
                uint32_t& jumpnode_id, warthog::cost_t& jumpcost,
                uint32_t scan_lim, uint32_t* nodes_scanned);
		void
		jump_upnorth(uint32_t node_id, uint32_t goal_id,
                uint32_t& jumpnode_id, warthog::cost_t& jumpcost,
                uint32_t scan_lim, uint32_t* nodes_scanned);
		void
		jump_upsouth(uint32_t node_id, uint32_t goal_id,
                uint32_t& jumpnode_id, warthog::cost_t& jumpcost,
                uint32_t scan_lim, uint32_t* nodes_scanned);
		void
		jump_upeast(uint32_t node_id, uint32_t goal_id,
                uint32_t& jumpnode_id, warthog::cost_t& jumpcost,
                uint32_t scan_lim, uint32_t* nodes_scanned);
		void
		jump_upwest(uint32_t node_id, uint32_t goal_id,
                uint32_t& jumpnode_id, warthog::cost_t& jumpcost,
                uint32_t scan_lim, uint32_t* nodes_scanned);
		void
		jump_upnortheast(uint32_t node_id, uint32_t goal_id,
                uint32_t& jumpnode_id, warthog::cost_t& jumpcost,
                uint32_t scan_lim, uint32_t* nodes_scanned);
		void
		jump_upnorthwest(uint32_t node_id, uint32_t goal_id,
                uint32_t& jumpnode_id, warthog::cost_t& jumpcost,
                uint32_t scan_lim, uint32_t* nodes_scanned);
		void
		jump_upsoutheast(uint32_t node_id, uint32_t goal_id,
                uint32_t& jumpnode_id, warthog::cost_t& jumpcost,
                uint32_t scan_lim, uint32_t* nodes_scanned);
		void
		jump_upsouthwest(uint32_t node_id, uint32_t goal_id,
                uint32_t& jumpnode_id, warthog::cost_t& jumpcost,
                uint32_t scan_lim, uint32_t* nodes_scanned);
                
		// these versions can be passed a map parameter to
		// use when jumping. they allow switching between
		// map_ and rmap_ (a rotated counterpart).
		void
		__jump_east(uint32_t node_id, uint32_t goal_id, 
				uint32_t& jumpnode_id, warthog::cost_t& jumpcost,
                warthog::voxelmap* mymap, uint32_t scan_lim,
                uint32_t* nodes_scanned);
		void
		__jump_west(uint32_t node_id, uint32_t goal_id, 
				uint32_t& jumpnode_id, warthog::cost_t& jumpcost,
                warthog::voxelmap* mymap, uint32_t scan_lim,
                uint32_t* nodes_scanned);
		void
		__jump_north(uint32_t node_id, uint32_t goal_id, 
				uint32_t& jumpnode_id, warthog::cost_t& jumpcost,
                warthog::voxelmap* mymap, uint32_t scan_lim,
                uint32_t* nodes_scanned);
		void
		__jump_south(uint32_t node_id, uint32_t goal_id, 
				uint32_t& jumpnode_id, warthog::cost_t& jumpcost,
                warthog::voxelmap* mymap, uint32_t scan_lim,
                uint32_t* nodes_scanned);
        void
		__jump_down(uint32_t node_id, uint32_t goal_id, 
				uint32_t& jumpnode_id, warthog::cost_t& jumpcost,
                warthog::voxelmap* mymap, uint32_t scan_lim,
                uint32_t* nodes_scanned);
        void
		__jump_up(uint32_t node_id, uint32_t goal_id, 
				uint32_t& jumpnode_id, warthog::cost_t& jumpcost,
                warthog::voxelmap* mymap, uint32_t scan_lim,
                uint32_t* nodes_scanned);
        
		inline uint32_t
		map_id_to_rmap_id(uint32_t mapid)
		{
			if(mapid == warthog::INF32) { return mapid; }

			uint32_t x, y, z;
			uint32_t rx, ry, rz;
			map_->to_unpadded_xyz(mapid, x, y, z);
			rx = map_->header_height() - y - 1;
            ry = x;
            rz = z;
			return rmap_->to_padded_id(rx, ry, rz);
		}

		inline uint32_t
		rmap_id_to_map_id(uint32_t rmapid)
		{
			if(rmapid == warthog::INF32) { return rmapid; }

			uint32_t x, y, z;
			uint32_t rx, ry, rz;
			rmap_->to_unpadded_xyz(rmapid, rx, ry, rz);
			x = ry;
			y = rmap_->header_width() - rx - 1;
            z = rz;
			return map_->to_padded_id(x, y, z);
		}

		warthog::voxelmap*
		create_rmap();

        inline uint32_t
		map_id_to_zmap_id(uint32_t mapid)
		{
			if(mapid == warthog::INF32) { return mapid; }

			uint32_t x, y, z;
			uint32_t rx, ry, rz;
			map_->to_unpadded_xyz(mapid, x, y, z);
            rx = z;
			ry = y;
            rz = x;
			return zmap_->to_padded_id(rx, ry, rz);
		}

		inline uint32_t
		zmap_id_to_map_id(uint32_t zmapid)
		{
			if(zmapid == warthog::INF32) { return zmapid; }

			uint32_t x, y, z;
			uint32_t rx, ry, rz;
			zmap_->to_unpadded_xyz(zmapid, rx, ry, rz);
			x = rz;
			y = ry;
            z = rx;
			return map_->to_padded_id(x, y, z);
		}

        warthog::voxelmap*
        create_zmap();

        inline uint32_t
		map_id_to_xmap_id(uint32_t mapid)
		{
			if(mapid == warthog::INF32) { return mapid; }

			uint32_t x, y, z;
			uint32_t rx, ry, rz;
			map_->to_unpadded_xyz(mapid, x, y, z);
            rx = x;
			ry = z;
            rz = y;
			return xmap_->to_padded_id(rx, ry, rz);
		}

        inline uint32_t
		xmap_id_to_map_id(uint32_t xmapid)
		{
			if(xmapid == warthog::INF32) { return xmapid; }

			uint32_t x, y, z;
			uint32_t rx, ry, rz;
		    xmap_->to_unpadded_xyz(xmapid, rx, ry, rz);
			x = rx;
			y = rz;
            z = ry;
			return map_->to_padded_id(x, y, z);	
		}

        warthog::voxelmap*
        create_xmap();

		warthog::voxelmap* map_;
		warthog::voxelmap* rmap_;
        warthog::voxelmap* zmap_;
        warthog::voxelmap* xmap_;

};

}

#endif

