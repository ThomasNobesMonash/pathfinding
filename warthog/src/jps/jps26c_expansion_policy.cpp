#include "jps26c_expansion_policy.h"
#include "timer.h"

warthog::jps26c_expansion_policy::jps26c_expansion_policy(warthog::voxelmap* map)
    : expansion_policy(map->width()*map->height()*map->depth())
{
	map_ = map;
	jpl_ = new warthog::online_jump_point_locator26c(map);
	reset();
}

warthog::jps26c_expansion_policy::~jps26c_expansion_policy()
{
	delete jpl_;
}

void 
warthog::jps26c_expansion_policy::expand(
		warthog::search_node* current, warthog::problem_instance* problem)
{
    reset();

	// compute the direction of travel used to reach the current node.
	warthog::jps3D::direction dir_c =
	   	this->compute_direction((uint32_t)current->get_parent(), (uint32_t)current->get_id());

	// get the tiles around the current node c
	boost::multiprecision::uint128_t c_tiles = 0;
	uint32_t current_id = (uint32_t)current->get_id();
	map_->get_neighbours(current_id, (uint8_t*)&c_tiles);

    uint32_t nodes_scanned = 0;
    
	// look for jump points in the direction of each natural 
	// and forced neighbour
	uint32_t succ_dirs = warthog::jps3D::compute_successors(dir_c, c_tiles);
    uint32_t goal_id = (uint32_t)problem->target_id_;
	for(uint32_t i = 0; i < 26; i++)
	{
		warthog::jps3D::direction d = (warthog::jps3D::direction) (1 << i);
		if(succ_dirs & d)
		{
			warthog::cost_t jumpcost;
			uint32_t succ_id;

            warthog::timer jumptimer;
			jumptimer.start();
			jpl_->jump(d, current_id, goal_id, succ_id, jumpcost, &nodes_scanned);
            jumptimer.stop();
            jump_time_ += jumptimer.elapsed_time_micro();
            nodes_scanned_ += nodes_scanned;

			if(succ_id != warthog::INF32)
			{
                warthog::search_node* jp_succ = this->generate(succ_id);
                add_neighbour(jp_succ, jumpcost);
			}
		}
	}
}

void
warthog::jps26c_expansion_policy::get_xyz(
        warthog::sn_id_t node_id, int32_t& x, int32_t& y, int32_t& z)
{
    map_->to_unpadded_xyz((uint32_t)node_id, (uint32_t&)x, (uint32_t&)y, (uint32_t&)z);
}

warthog::search_node* 
warthog::jps26c_expansion_policy::generate_start_node(
        warthog::problem_instance* pi)
{ 
    uint32_t max_id = map_->header_width() * map_->header_height() * map_->header_depth();
    if((uint32_t)pi->start_id_ >= max_id) { return 0; }
    uint32_t padded_id = map_->to_padded_id((uint32_t)pi->start_id_);
    if(map_->get_label(padded_id) == 0) { return 0; }
    return generate(padded_id);
}

warthog::search_node*
warthog::jps26c_expansion_policy::generate_target_node(
        warthog::problem_instance* pi)
{
    uint32_t max_id = map_->header_width() * map_->header_height() * map_->header_depth();
    if((uint32_t)pi->target_id_ >= max_id) { return 0; }
    uint32_t padded_id = map_->to_padded_id((uint32_t)pi->target_id_);
    if(map_->get_label(padded_id) == 0) { return 0; }
    return generate(padded_id);
}

// computes the direction of travel; from a node n1
// to a node n2.
inline warthog::jps3D::direction
warthog::jps26c_expansion_policy::compute_direction(
        uint32_t n1_id, uint32_t n2_id)
{
    if(n1_id == warthog::GRID_ID_MAX) { return warthog::jps3D::NONE; }

    int32_t x, y, z, x2, y2, z2;
    warthog::helpers::index_to_xyz(n1_id, map_->width(), map_->height(), x, y, z);
    warthog::helpers::index_to_xyz(n2_id, map_->width(), map_->height(), x2, y2, z2);
    warthog::jps3D::direction dir = warthog::jps3D::NONE;

    if(z2 == z)
    {
        if(y2 == y)
        {
            if(x2 > x)
            {
                dir = warthog::jps3D::EAST;
            }
            else
            {
                dir = warthog::jps3D::WEST;
            }
        }
        else if(y2 < y)
        {
            if(x2 == x)
            {
                dir = warthog::jps3D::NORTH;
            }
            else if(x2 > x)
            {
                dir = warthog::jps3D::NORTHEAST;
            }
            else // x2 < x
            {
                dir = warthog::jps3D::NORTHWEST;
            }
        }
        else // y2 > y 
        {
            if(x2 == x)
            {
                dir = warthog::jps3D::SOUTH;
            }
            else if(x2 < x)
            {
                dir = warthog::jps3D::SOUTHWEST;
            }
            else // x2 > x
            {
                dir = warthog::jps3D::SOUTHEAST;
            }
        }
    }
    else if(z2 < z)
    {
        if(y2 == y)
        {
            if(x2 > x)
            {
                dir = warthog::jps3D::DOWNEAST;
            }
            else if(x2 < x)
            {
                dir = warthog::jps3D::DOWNWEST;
            }
            else
            {
                dir = warthog::jps3D::DOWN;
            }
        }
        else if(y2 < y)
        {
            if(x2 == x)
            {
                dir = warthog::jps3D::DOWNNORTH;
            }
            else if(x2 < x)
            {
                dir = warthog::jps3D::DOWNNORTHWEST;
            }
            else // x2 > x
            {
                dir = warthog::jps3D::DOWNNORTHEAST;
            }
        }
        else // y2 > y 
        {
            if(x2 == x)
            {
                dir = warthog::jps3D::DOWNSOUTH;
            }
            else if(x2 < x)
            {
                dir = warthog::jps3D::DOWNSOUTHWEST;
            }
            else // x2 > x
            {
                dir = warthog::jps3D::DOWNSOUTHEAST;
            }
        }
    }
    else // z2 > z
    {
        if(y2 == y)
        {
            if(x2 > x)
            {
                dir = warthog::jps3D::UPEAST;
            }
            else if(x2 < x)
            {
                dir = warthog::jps3D::UPWEST;
            }
            else
            {
                dir = warthog::jps3D::UP;
            }
        }
        else if(y2 < y)
        {
            if(x2 == x)
            {
                dir = warthog::jps3D::UPNORTH;
            }
            else if(x2 < x)
            {
                dir = warthog::jps3D::UPNORTHWEST;
            }
            else // x2 > x
            {
                dir = warthog::jps3D::UPNORTHEAST;
            }
        }
        else // y2 > y 
        {
            if(x2 == x)
            {
                dir = warthog::jps3D::UPSOUTH;
            }
            else if(x2 < x)
            {
                dir = warthog::jps3D::UPSOUTHWEST;
            }
            else // x2 > x
            {
                dir = warthog::jps3D::UPSOUTHEAST;
            }
        }
    }
    assert(dir != warthog::jps3D::NONE);
    return dir;
}