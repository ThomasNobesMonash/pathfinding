#ifndef WARTHOG_JPS26C_EXPANSION_POLICY_H
#define WARTHOG_JPS26C_EXPANSION_POLICY_H

// jps26c_expansion_policy.h
//
// This expansion policy reduces the branching factor
// of a node n during search by ignoring any neighbours which
// could be reached by an equivalent (or shorter) path that visits
// the parent of n but not n itself.
//
// An extension of this idea is to generate jump nodes located in the
// same direction as the remaining neighbours. 
//
// Theoretical details:
// [Harabor D. and Grastien A., 2011, Online Node Pruning for Pathfinding
// On Grid Maps, AAAI] 
// [Nobes T. et. al., 2022, JPS in 3D, SoCS] 
//
// @author: dharabor, tnobes
// @created: 10/04/2022

#include "expansion_policy.h"
#include "voxelmap.h"
#include "jps3D.h"
#include "online_jump_point_locator26c.h"
#include "problem_instance.h"
#include "search_node.h"

#include "stdint.h"

namespace warthog
{

class jps26c_expansion_policy : public expansion_policy
{
	public:
		jps26c_expansion_policy(warthog::voxelmap* map);
		virtual ~jps26c_expansion_policy();

        virtual void
        expand(warthog::search_node*, warthog::problem_instance*);

        virtual void
        get_xy(sn_id_t node_id, int32_t& x, int32_t& y) { };

        virtual void
        get_xyz(sn_id_t node_id, int32_t& x, int32_t& y, int32_t& z);

        virtual warthog::search_node*
        generate_start_node(warthog::problem_instance* pi);

        virtual warthog::search_node*
        generate_target_node(warthog::problem_instance* pi);

        virtual inline size_t
		mem()
		{
            return expansion_policy::mem() +
                sizeof(*this) + map_->mem() + jpl_->mem();
		}

    private:
		warthog::voxelmap* map_;
        warthog::online_jump_point_locator26c* jpl_;
        double jump_time_;
        uint32_t nodes_scanned_;

        // computes the direction of travel; from a node n1
		// to a node n2.
		inline warthog::jps3D::direction
		compute_direction(uint32_t n1_id, uint32_t n2_id);
};

}

#endif

