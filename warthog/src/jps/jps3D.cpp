#include "jps3D.h"

// computes the forced neighbours of a node.
// for a neighbour to be forced we must check that 
// (a) the alt path from the parent is blocked and
// (b) the neighbour is not an obstacle.
// if the test succeeds, we set a bit to indicate 
// the direction from the current node (tiles[4])
// to the forced neighbour.
//
// @return an integer value whose lower 8 bits indicate
// the directions of forced neighbours
//
// NB: the first 3 bits of the first 3 bytes of @param tiles represent
// a 3x3x3 block of nodes. the current node is at the centre of
// the block.
// its NW neighbour is bit 0
// its N neighbour is bit 1
// its NE neighbour is bit 2
// its W neighbour is bit 8
// ...
// its SE neighbour is bit 18 
// ...
// its DNW neighbour is bit 24
// its DSE neighbour is bit 42
// ...
// its UNW neighbour is bit 48
// There are optimisations below that use bitmasks in order
// to speed up forced neighbour computation. 
// We use the revised forced neighbour rules described in
// [Nobes T., et. al., JPS in 3D, SoCS, 2022]
// These rules do not allow diagonal transitions that cut corners.
uint32_t
warthog::jps3D::compute_forced(warthog::jps3D::direction d, boost::multiprecision::uint128_t tiles)
{
    // NB: to avoid branching statements, shift operations are
	// used below. The values of the shift constants correspond to 
	// direction values. 
	uint32_t ret = 0;
    switch(d)
	{
		case warthog::jps3D::NORTH:
            // Direct Obstacles
			if((tiles & 65792) == 256) // if West of p is blocked
			{
				ret |= (warthog::jps3D::WEST | warthog::jps3D::NORTHWEST |
                        warthog::jps3D::UPWEST | warthog::jps3D::DOWNWEST |
                        warthog::jps3D::UPNORTHWEST | warthog::jps3D::DOWNNORTHWEST);
			}
			if((tiles & 263168) == 1024) // if East of p is blocked
			{
				ret |= (warthog::jps3D::EAST | warthog::jps3D::NORTHEAST |
				        warthog::jps3D::UPEAST | warthog::jps3D::DOWNEAST |
                        warthog::jps3D::UPNORTHEAST | warthog::jps3D::DOWNNORTHEAST);
			}
            if((tiles & 2207613190144) == 8589934592) // if below (Down) oxf p is blocked
            {
                ret |= (warthog::jps3D::DOWN | warthog::jps3D::DOWNNORTH |
                        warthog::jps3D::DOWNEAST | warthog::jps3D::DOWNWEST |
                        warthog::jps3D::DOWNNORTHEAST | warthog::jps3D::DOWNNORTHWEST);
            }
            if((tiles & warthog::lnum_US_forcedN) == 144115188075855872) // if above (Up) of p is blocked
            {
                ret |= (warthog::jps3D::UP | warthog::jps3D::UPNORTH |
                        warthog::jps3D::UPEAST | warthog::jps3D::UPWEST |
                        warthog::jps3D::UPNORTHEAST | warthog::jps3D::UPNORTHWEST);
            }
            // Diagonal Obstacles
            if((tiles & warthog::lnum_NdiagUSE) == 288230376151711744) // if USE is blocked
            {
                ret |= (warthog::jps3D::UPEAST | warthog::jps3D::UPNORTHEAST);
            }
            if((tiles & warthog::lnum_NdiagUSW) == 72057594037927936) // if USW is blocked
            {
                ret |= (warthog::jps3D::UPWEST | warthog::jps3D::UPNORTHWEST);
            }
            if((tiles & 4415226380288) == 17179869184) // if DSE is blocked
            {
                ret |= (warthog::jps3D::DOWNEAST | warthog::jps3D::DOWNNORTHEAST);
            }
            if((tiles & 1103806595072) == 4294967296) // if DSW is blocked
            {
                ret |= (warthog::jps3D::DOWNWEST | warthog::jps3D::DOWNNORTHWEST);
            }
			break;
		case warthog::jps3D::SOUTH:
            // Direct obstacles
			if((tiles & 257) == 256) // if West of p is blocked
			{
				ret |= (warthog::jps3D::WEST | warthog::jps3D::SOUTHWEST |
                        warthog::jps3D::UPWEST | warthog::jps3D::DOWNWEST |
                        warthog::jps3D::UPSOUTHWEST | warthog::jps3D::DOWNSOUTHWEST);
			}
			if((tiles & 1028) == 1024) // if East of p is blocked
			{
				ret |= (warthog::jps3D::EAST | warthog::jps3D::SOUTHEAST |
                        warthog::jps3D::UPEAST | warthog::jps3D::DOWNEAST |
                        warthog::jps3D::UPSOUTHEAST | warthog::jps3D::DOWNSOUTHEAST);
			}
			if((tiles & 8623489024) == 8589934592) // if below (Down) of p is blocked
			{
				ret |= (warthog::jps3D::DOWN| warthog::jps3D::DOWNSOUTH |
                        warthog::jps3D::DOWNEAST | warthog::jps3D::DOWNWEST |
                        warthog::jps3D::DOWNSOUTHEAST | warthog::jps3D::DOWNSOUTHWEST);
			}
			if((tiles & 144678138029277184) == 144115188075855872) // if above (Up) of p is blocked
			{
				ret |= (warthog::jps3D::UP | warthog::jps3D::UPSOUTH |
                        warthog::jps3D::UPEAST | warthog::jps3D::UPWEST |
                        warthog::jps3D::UPSOUTHEAST | warthog::jps3D::UPSOUTHWEST);
			}
            // Diagonal obstacles
            if((tiles & 289356276058554368) == 288230376151711744) // if UNE is blocked
            {
                ret |= (warthog::jps3D::UPEAST | warthog::jps3D::UPSOUTHEAST);
            }
            if((tiles & 72339069014638592) == 72057594037927936) // if UNW is blocked
            {
                ret |= (warthog::jps3D::UPWEST | warthog::jps3D::UPSOUTHWEST);
            }
            if((tiles & 17246978048) == 17179869184) // if DNE is blocked
            {
                ret |= (warthog::jps3D::DOWNEAST | warthog::jps3D::DOWNSOUTHEAST);
            }
            if((tiles & 4311744512) == 4294967296) // if DNW is blocked
            {
                ret |= (warthog::jps3D::DOWNWEST | warthog::jps3D::DOWNSOUTHWEST);
            }
			break;
		case warthog::jps3D::EAST:
            // Direct obstacles
			if((tiles & 3) == 2) // if North of p is blocked
			{
				ret |= (warthog::jps3D::NORTH | warthog::jps3D::NORTHEAST |
                        warthog::jps3D::UPNORTH | warthog::jps3D::DOWNNORTH |
                        warthog::jps3D::UPNORTHEAST | warthog::jps3D::DOWNNORTHEAST);
			}
			if((tiles & 196608) == 131072) // if South of p is blocked
			{
				ret |= (warthog::jps3D::SOUTH | warthog::jps3D::SOUTHEAST |
                        warthog::jps3D::UPSOUTH | warthog::jps3D::DOWNSOUTH|
                        warthog::jps3D::UPSOUTHEAST | warthog::jps3D::DOWNSOUTHEAST);
			}
			if((tiles & 12884901888) == 8589934592) // if below (Down) of p is blocked
			{
				ret |= (warthog::jps3D::DOWN | warthog::jps3D::DOWNEAST |
                        warthog::jps3D::DOWNNORTH | warthog::jps3D::DOWNSOUTH |
                        warthog::jps3D::DOWNNORTHEAST | warthog::jps3D::DOWNSOUTHEAST);
			}
			if((tiles & 216172782113783808) == 144115188075855872) // if above (Up) of p is blocked
			{
				ret |= (warthog::jps3D::UP | warthog::jps3D::UPEAST |
                        warthog::jps3D::UPNORTH | warthog::jps3D::UPSOUTH |
                        warthog::jps3D::UPNORTHEAST | warthog::jps3D::UPSOUTHEAST);
			}
            // Diagonal obstacles
            if((tiles & 844424930131968) == 562949953421312) // if UNW is blocked
            {
                ret |= (warthog::jps3D::UPNORTH | warthog::jps3D::UPNORTHEAST);
            }
            if((tiles & warthog::lnum_EdiagUSW) == warthog::lnum_onlyUS) // if USW is blocked
            {
                ret |= (warthog::jps3D::UPSOUTH | warthog::jps3D::UPSOUTHEAST);
            }
            if((tiles & 50331648) == 33554432) // if DNW is blocked
            {
                ret |= (warthog::jps3D::DOWNNORTH | warthog::jps3D::DOWNNORTHEAST);
            }
            if((tiles & 3298534883328) == 2199023255552) // if DSW is blocked
            {
                ret |= (warthog::jps3D::DOWNSOUTH | warthog::jps3D::DOWNSOUTHEAST);
            }
			break;
		case warthog::jps3D::WEST:
            // Direct obstacles
			if((tiles & 6) == 2)
			{
				ret |= (warthog::jps3D::NORTH | warthog::jps3D::NORTHWEST |
                        warthog::jps3D::UPNORTH | warthog::jps3D::DOWNNORTH |
                        warthog::jps3D::UPNORTHWEST | warthog::jps3D::DOWNNORTHWEST);
			}
			if((tiles & 393216) == 131072)
			{
				ret |= (warthog::jps3D::SOUTH | warthog::jps3D::SOUTHWEST |
                        warthog::jps3D::UPSOUTH | warthog::jps3D::DOWNSOUTH|
                        warthog::jps3D::UPSOUTHWEST | warthog::jps3D::DOWNSOUTHWEST);
			}
			if((tiles & 25769803776) == 8589934592)
			{
				ret |= (warthog::jps3D::DOWN | warthog::jps3D::DOWNWEST |
                        warthog::jps3D::DOWNNORTH | warthog::jps3D::DOWNSOUTH |
                        warthog::jps3D::DOWNNORTHWEST | warthog::jps3D::DOWNSOUTHWEST);
			}
			if((tiles & 432345564227567616) == 144115188075855872)
			{
				ret |= (warthog::jps3D::UP | warthog::jps3D::UPWEST |
                        warthog::jps3D::UPNORTH | warthog::jps3D::UPSOUTH |
                        warthog::jps3D::UPNORTHWEST | warthog::jps3D::UPSOUTHWEST);
			}
            // Diagonal obstacles
            if((tiles & 1688849860263936) == 562949953421312) // if UNE is blocked
            {
                ret |= (warthog::jps3D::UPNORTH | warthog::jps3D::UPNORTHWEST);
            }
            if((tiles & warthog::lnum_WdiagUSE) == warthog::lnum_onlyUS) // if USE is blocked
            {
                ret |= (warthog::jps3D::UPSOUTH | warthog::jps3D::UPSOUTHWEST);
            }
            if((tiles & 100663296) == 33554432) // if DNE is blocked
            {
                ret |= (warthog::jps3D::DOWNNORTH | warthog::jps3D::DOWNNORTHWEST);
            }
            if((tiles & 6597069766656) == 2199023255552) // if DSE is blocked
            {
                ret |= (warthog::jps3D::DOWNSOUTH | warthog::jps3D::DOWNSOUTHWEST);
            }
			break;
        case warthog::jps3D::UP:
            // Direct obstacles
            if((tiles & 33554434) == 2) // if North of p is blocked
            {
                ret |= (warthog::jps3D::NORTH | warthog::jps3D::UPNORTH |
                        warthog::jps3D::NORTHEAST | warthog::jps3D::NORTHWEST |
                        warthog::jps3D::UPNORTHEAST | warthog::jps3D::UPNORTHWEST);
            }
            if((tiles & 17179870208) == 1024) // if East of p is blocked
            {
                ret |= (warthog::jps3D::EAST | warthog::jps3D::UPEAST |
                        warthog::jps3D::NORTHEAST | warthog::jps3D::SOUTHEAST |
                        warthog::jps3D::UPNORTHEAST | warthog::jps3D::UPSOUTHEAST);
            }
            if((tiles & 2199023386624) == 131072) // if South of p is blocked
            {
                ret |= (warthog::jps3D::SOUTH | warthog::jps3D::UPSOUTH |
                        warthog::jps3D::SOUTHEAST | warthog::jps3D::SOUTHWEST |
                        warthog::jps3D::UPSOUTHEAST | warthog::jps3D::UPSOUTHWEST);
            }
            if((tiles & 4294967552) == 256) // if West of p is blocked
            {
                ret |= (warthog::jps3D::WEST | warthog::jps3D::UPWEST |
                        warthog::jps3D::NORTHWEST | warthog::jps3D::SOUTHWEST |
                        warthog::jps3D::UPNORTHWEST | warthog::jps3D::UPSOUTHWEST);
            }
            // Diagonal obstacles
            if((tiles & 67108868) == 4) // if DNE is blocked
            {
                ret |= (warthog::jps3D::NORTHEAST | warthog::jps3D::UPNORTHEAST);
            }
            if((tiles & 16777217) == 1) // if DNW is blocked
            {
                ret |= (warthog::jps3D::NORTHWEST | warthog::jps3D::UPNORTHWEST);
            }
            if((tiles & 4398046773248) == 262144) // if DSE is blocked
            {
                ret |= (warthog::jps3D::SOUTHEAST | warthog::jps3D::UPSOUTHEAST);
            }
            if((tiles & 1099511693312) == 65536) // if DSW is blocked
            {
                ret |= (warthog::jps3D::SOUTHWEST | warthog::jps3D::UPSOUTHWEST);
            }
            break;
        case warthog::jps3D::DOWN:
            if((tiles & 562949953421314) == 2) // if North of p is blocked
            {
                ret |= (warthog::jps3D::NORTH | warthog::jps3D::DOWNNORTH |
                        warthog::jps3D::NORTHEAST | warthog::jps3D::NORTHWEST |
                        warthog::jps3D::DOWNNORTHEAST | warthog::jps3D::DOWNNORTHWEST);
            }
            if((tiles & 288230376151712768) == 1024) // if East of p is blocked
            {
                ret |= (warthog::jps3D::EAST | warthog::jps3D::DOWNEAST |
                        warthog::jps3D::NORTHEAST | warthog::jps3D::SOUTHEAST |
                        warthog::jps3D::DOWNNORTHEAST | warthog::jps3D::DOWNSOUTHEAST);
            }
            if((tiles & warthog::lnum_US_forcedU) == 131072) // if South of p is blocked
            {
                ret |= (warthog::jps3D::SOUTH | warthog::jps3D::DOWNSOUTH |
                        warthog::jps3D::SOUTHEAST | warthog::jps3D::SOUTHWEST |
                        warthog::jps3D::DOWNSOUTHEAST | warthog::jps3D::DOWNSOUTHWEST);
            }
            if((tiles & 72057594037928192) == 256) // if West of p is blocked
            {
                ret |= (warthog::jps3D::WEST | warthog::jps3D::DOWNWEST |
                        warthog::jps3D::NORTHWEST | warthog::jps3D::SOUTHWEST |
                        warthog::jps3D::DOWNNORTHWEST | warthog::jps3D::DOWNSOUTHWEST);
            }
            // Diagonal obstacles
            if((tiles & 1125899906842628) == 4) // if UNE is blocked
            {
                ret |= (warthog::jps3D::NORTHEAST | warthog::jps3D::DOWNNORTHEAST);
            }
            if((tiles & 281474976710657) == 1) // if UNW is blocked
            {
                ret |= (warthog::jps3D::NORTHWEST | warthog::jps3D::DOWNNORTHWEST);
            }
            if((tiles & warthog::lnum_DdiagUSE) == 262144) // if USE is blocked
            {
                ret |= (warthog::jps3D::SOUTHEAST | warthog::jps3D::DOWNSOUTHEAST);
            }
            if((tiles & warthog::lnum_SWandUSW) == 65536) // if USW is blocked
            {
                ret |= (warthog::jps3D::SOUTHWEST | warthog::jps3D::DOWNSOUTHWEST);
            }
            break;
        case warthog::jps3D::NORTHEAST:
            if((tiles & warthog::lnum_US_forcedNE) == 144115188075855872) // USW is blocked
            {
                ret |= (warthog::jps3D::UP | warthog::jps3D::UPNORTHEAST | warthog::jps3D::UPEAST | warthog::jps3D::UPNORTH);
            }
            else if((tiles & warthog::lnum_US_forcedN) == 144115188075855872) // US is blocked
            {
                ret |= (warthog::jps3D::UP | warthog::jps3D::UPNORTHEAST | warthog::jps3D::UPEAST | warthog::jps3D::UPNORTH);
            }
            else if((tiles & 216172782113783808) == 144115188075855872) // UW is blocked
            {
                ret |= (warthog::jps3D::UP | warthog::jps3D::UPNORTHEAST | warthog::jps3D::UPNORTH | warthog::jps3D::UPEAST);
            }
            if((tiles & 1108101562368) == 8589934592) // DSW is blocked
            {           
                ret |= (warthog::jps3D::DOWN | warthog::jps3D::DOWNNORTHEAST | warthog::jps3D::DOWNEAST | warthog::jps3D::DOWNNORTH);
            }
            else if((tiles & 2207613190144) == 8589934592) // DS is blocked
            { 
                ret |= (warthog::jps3D::DOWN | warthog::jps3D::DOWNNORTHEAST | warthog::jps3D::DOWNEAST | warthog::jps3D::DOWNNORTH);
            }
            else if((tiles & 12884901888) == 8589934592) // DW is blocked
            {
                ret |= (warthog::jps3D::DOWN | warthog::jps3D::DOWNNORTHEAST | warthog::jps3D::DOWNNORTH | warthog::jps3D::DOWNEAST);
            }
            break;
        case warthog::jps3D::NORTHWEST:
            if((tiles & warthog::lnum_US_forcedNW) == 144115188075855872) // USE is blocked
            {
                ret |= (warthog::jps3D::UP | warthog::jps3D::UPNORTHWEST | warthog::jps3D::UPWEST | warthog::jps3D::UPNORTH);
            }
            else if((tiles & warthog::lnum_US_forcedN) == 144115188075855872) // US is blocked
            {
                ret |= (warthog::jps3D::UP | warthog::jps3D::UPNORTHWEST | warthog::jps3D::UPWEST | warthog::jps3D::UPNORTH);
            }
            else if((tiles & 432345564227567616) == 144115188075855872) // UE is blocked
            {
                ret |= (warthog::jps3D::UP | warthog::jps3D::UPNORTHWEST | warthog::jps3D::UPNORTH | warthog::jps3D::UPWEST);
            }
            if((tiles & 4406636445696) == 8589934592) // DSE is blocked
            {
                ret |= (warthog::jps3D::DOWN | warthog::jps3D::DOWNNORTHWEST | warthog::jps3D::DOWNWEST | warthog::jps3D::DOWNNORTH);
            }
            else if((tiles & 2207613190144) == 8589934592) // DS is blocked
            {
                ret |= (warthog::jps3D::DOWN | warthog::jps3D::DOWNNORTHWEST | warthog::jps3D::DOWNWEST | warthog::jps3D::DOWNNORTH);
            }
            else if((tiles & 25769803776) == 8589934592) // DE is blocked
            {
                ret |= (warthog::jps3D::DOWN | warthog::jps3D::DOWNNORTHWEST | warthog::jps3D::DOWNNORTH | warthog::jps3D::DOWNWEST);
            }
            break;
        case warthog::jps3D::SOUTHEAST:
            if((tiles & 144396663052566528) == 144115188075855872) // UNW is blocked
            {
                ret |= (warthog::jps3D::UP | warthog::jps3D::UPSOUTHEAST | warthog::jps3D::UPEAST | warthog::jps3D::UPSOUTH);
            }
            else if((tiles & 144678138029277184) == 144115188075855872) // UN is blocked
            {
                ret |= (warthog::jps3D::UP | warthog::jps3D::UPSOUTHEAST | warthog::jps3D::UPEAST | warthog::jps3D::UPSOUTH);
            }
            else if((tiles & 216172782113783808) == 144115188075855872) // UW is blocked
            {
                ret |= (warthog::jps3D::UP | warthog::jps3D::UPSOUTHEAST | warthog::jps3D::UPSOUTH | warthog::jps3D::UPEAST);
            }
            if((tiles & 8606711808) == 8589934592) // DNW is blocked
            {
                ret |= (warthog::jps3D::DOWN | warthog::jps3D::DOWNSOUTHEAST | warthog::jps3D::DOWNEAST | warthog::jps3D::DOWNSOUTH);
            }
            else if((tiles & 8623489024) == 8589934592) // DN is blocked
            {
                ret |= (warthog::jps3D::DOWN | warthog::jps3D::DOWNSOUTHEAST | warthog::jps3D::DOWNEAST | warthog::jps3D::DOWNSOUTH);
            }
            else if((tiles & 12884901888) == 8589934592) // DW is blocked
            {
                ret |= (warthog::jps3D::DOWN | warthog::jps3D::DOWNSOUTHEAST | warthog::jps3D::DOWNSOUTH | warthog::jps3D::DOWNEAST);
            }
            break;
        case warthog::jps3D::SOUTHWEST:
            if((tiles & 145241087982698496) == 144115188075855872) // UNE is blocked
            {
                ret |= (warthog::jps3D::UP | warthog::jps3D::UPSOUTHWEST | warthog::jps3D::UPWEST | warthog::jps3D::UPSOUTH);
            }
            else if((tiles & 144678138029277184) == 144115188075855872) // UN is blocked
            {
                ret |= (warthog::jps3D::UP | warthog::jps3D::UPSOUTHWEST | warthog::jps3D::UPWEST | warthog::jps3D::UPSOUTH);
            }
            else if((tiles & 432345564227567616) == 144115188075855872) // UE is blocked
            {
                ret |= (warthog::jps3D::UP | warthog::jps3D::UPSOUTHWEST | warthog::jps3D::UPSOUTH | warthog::jps3D::UPWEST);
            }
            if((tiles & 8657043456) == 8589934592) // DNE is blocked
            {
                ret |= (warthog::jps3D::DOWN | warthog::jps3D::DOWNSOUTHWEST | warthog::jps3D::DOWNWEST | warthog::jps3D::DOWNSOUTH);
            }
            else if((tiles & 8623489024) == 8589934592) // DN is blocked
            {
                ret |= (warthog::jps3D::DOWN | warthog::jps3D::DOWNSOUTHWEST | warthog::jps3D::DOWNWEST | warthog::jps3D::DOWNSOUTH);
            }
            else if((tiles & 25769803776) == 8589934592) // DE is blocked
            {
                ret |= (warthog::jps3D::DOWN | warthog::jps3D::DOWNSOUTHWEST | warthog::jps3D::DOWNSOUTH | warthog::jps3D::DOWNWEST);
            }
            break;
        case warthog::jps3D::UPNORTH:
            if((tiles & 4398046512128) == 1024) // DSE is blocked
            {
                ret |= (warthog::jps3D::EAST | warthog::jps3D::UPNORTHEAST | warthog::jps3D::NORTHEAST | warthog::jps3D::UPEAST);
            }
            else if((tiles & 17179870208) == 1024) // DE is blocked
            {
                ret |= (warthog::jps3D::EAST | warthog::jps3D::UPNORTHEAST | warthog::jps3D::NORTHEAST | warthog::jps3D::UPEAST);
            }
            else if((tiles & 263168) == 1024) // SE is blocked
            {
                ret |= (warthog::jps3D::EAST | warthog::jps3D::UPNORTHEAST | warthog::jps3D::UPEAST | warthog::jps3D::NORTHEAST);
            }
            if((tiles & 1099511628032) == 256) // DSW is blocked
            {
                ret |= (warthog::jps3D::WEST | warthog::jps3D::UPNORTHWEST | warthog::jps3D::NORTHWEST | warthog::jps3D::UPWEST);
            }
            else if((tiles & 4294967552) == 256) // DW is blocked
            {
                ret |= (warthog::jps3D::WEST | warthog::jps3D::UPNORTHWEST | warthog::jps3D::NORTHWEST | warthog::jps3D::UPWEST);
            }
            else if((tiles & 65792) == 256) // SW is blocked
            {
                ret |= (warthog::jps3D::WEST | warthog::jps3D::UPNORTHWEST | warthog::jps3D::UPWEST | warthog::jps3D::NORTHWEST);
            }
            break;
        case warthog::jps3D::UPSOUTH:
            if((tiles & 67109888) == 1024) // DNE is blocked
            {
                ret |= (warthog::jps3D::EAST | warthog::jps3D::UPSOUTHEAST | warthog::jps3D::SOUTHEAST | warthog::jps3D::UPEAST);
            }
            else if((tiles & 17179870208) == 1024) // DE is blocked
            {
                ret |= (warthog::jps3D::EAST | warthog::jps3D::UPSOUTHEAST | warthog::jps3D::SOUTHEAST | warthog::jps3D::UPEAST);
            }
            else if((tiles & 1028) == 1024) // NE is blocked
            {
                ret |= (warthog::jps3D::EAST | warthog::jps3D::UPSOUTHEAST | warthog::jps3D::UPEAST | warthog::jps3D::SOUTHEAST);
            }
            if((tiles & 16777472) == 256) // DNW is blocked
            {
                ret |= (warthog::jps3D::WEST | warthog::jps3D::UPSOUTHWEST | warthog::jps3D::SOUTHWEST | warthog::jps3D::UPWEST);
            }
            else if((tiles & 4294967552) == 256) // DW is blocked
            {
                ret |= (warthog::jps3D::WEST | warthog::jps3D::UPSOUTHWEST | warthog::jps3D::SOUTHWEST | warthog::jps3D::UPWEST);
            }
            else if((tiles & 257) == 256) // NW is blocked
            {
                ret |= (warthog::jps3D::WEST | warthog::jps3D::UPSOUTHWEST | warthog::jps3D::UPWEST | warthog::jps3D::SOUTHWEST);
            }
            break;
        case warthog::jps3D::UPEAST:
            if((tiles & 16777218) == 2) // DNW is blocked
            {
                ret |= (warthog::jps3D::NORTH | warthog::jps3D::UPNORTHEAST | warthog::jps3D::NORTHEAST | warthog::jps3D::UPNORTH);
            }
            else if((tiles & 33554434) == 2) // DN is blocked
            {
                ret |= (warthog::jps3D::NORTH | warthog::jps3D::UPNORTHEAST | warthog::jps3D::NORTHEAST | warthog::jps3D::UPNORTH);
            }
            else if((tiles & 3) == 2) // NW is blocked
            {
                ret |= (warthog::jps3D::NORTH | warthog::jps3D::UPNORTHEAST | warthog::jps3D::UPNORTH | warthog::jps3D::NORTHEAST);
            }
            if((tiles & 1099511758848) == 131072) // DSW is blocked
            {
                ret |= (warthog::jps3D::SOUTH | warthog::jps3D::UPSOUTHEAST | warthog::jps3D::SOUTHEAST | warthog::jps3D::UPSOUTH);
            }
            else if((tiles & 2199023386624) == 131072) // DS is blocked
            {
                ret |= (warthog::jps3D::SOUTH | warthog::jps3D::UPSOUTHEAST | warthog::jps3D::SOUTHEAST | warthog::jps3D::UPSOUTH);
            }
            else if((tiles & 196608) == 131072) // SW is blocked
            {
                ret |= (warthog::jps3D::SOUTH | warthog::jps3D::UPSOUTHEAST | warthog::jps3D::UPSOUTH | warthog::jps3D::SOUTHEAST);
            }
            break;
        case warthog::jps3D::UPWEST:
            if((tiles & 67108866) == 2) // DNE is blocked
            {
                ret |= (warthog::jps3D::NORTH | warthog::jps3D::UPNORTHWEST | warthog::jps3D::NORTHWEST | warthog::jps3D::UPNORTH);
            }
            else if((tiles & 33554434) == 2) // DN is blocked
            {
                ret |= (warthog::jps3D::NORTH | warthog::jps3D::UPNORTHWEST | warthog::jps3D::NORTHWEST | warthog::jps3D::UPNORTH);
            }
            else if((tiles & 6) == 2) // NE is blocked
            {
                ret |= (warthog::jps3D::NORTH | warthog::jps3D::UPNORTHWEST | warthog::jps3D::UPNORTH | warthog::jps3D::NORTHWEST);
            }
            if((tiles & 4398046642176) == 131072) // DSE is blocked
            {
                ret |= (warthog::jps3D::SOUTH | warthog::jps3D::UPSOUTHWEST | warthog::jps3D::SOUTHWEST | warthog::jps3D::UPSOUTH);
            }
            else if((tiles & 2199023386624) == 131072) // DS is blocked
            {
                ret |= (warthog::jps3D::SOUTH | warthog::jps3D::UPSOUTHWEST | warthog::jps3D::SOUTHWEST | warthog::jps3D::UPSOUTH);
            }
            else if((tiles & 393216) == 131072) // SE is blocked
            {
                ret |= (warthog::jps3D::SOUTH | warthog::jps3D::UPSOUTHWEST | warthog::jps3D::UPSOUTH | warthog::jps3D::SOUTHWEST);
            }
            break;
        case warthog::jps3D::DOWNNORTH:
            if((tiles & warthog::lnum_US_forcedDNE) == 1024) // USE is blocked
            {
                ret |= (warthog::jps3D::EAST | warthog::jps3D::DOWNNORTHEAST | warthog::jps3D::NORTHEAST | warthog::jps3D::DOWNEAST);
            }
            else if((tiles & 288230376151712768) == 1024) // UE is blocked
            {
                ret |= (warthog::jps3D::EAST | warthog::jps3D::DOWNNORTHEAST | warthog::jps3D::NORTHEAST | warthog::jps3D::DOWNEAST);
            }
            else if((tiles & 263168) == 1024) // SE is blocked
            {
                ret |= (warthog::jps3D::EAST | warthog::jps3D::DOWNNORTHEAST | warthog::jps3D::DOWNEAST | warthog::jps3D::NORTHEAST);
            }
            if((tiles & warthog::lnum_US_forcedDNW) == 256) // USW is blocked
            {
                ret |= (warthog::jps3D::WEST | warthog::jps3D::DOWNNORTHWEST | warthog::jps3D::NORTHWEST | warthog::jps3D::DOWNWEST);
            }
            else if((tiles & 72057594037928192) == 256) // UW is blocked
            {
                ret |= (warthog::jps3D::WEST | warthog::jps3D::DOWNNORTHWEST | warthog::jps3D::NORTHWEST | warthog::jps3D::DOWNWEST);
            }
            else if((tiles & 65792) == 256) // SW is blocked
            {
                ret |= (warthog::jps3D::WEST | warthog::jps3D::DOWNNORTHWEST | warthog::jps3D::DOWNWEST | warthog::jps3D::NORTHWEST);
            }
            break;
        case warthog::jps3D::DOWNSOUTH:
            if((tiles & 1125899906843648) == 1024) // UNE is blocked
            {
                ret |= (warthog::jps3D::EAST | warthog::jps3D::DOWNSOUTHEAST | warthog::jps3D::SOUTHEAST | warthog::jps3D::DOWNEAST);
            }
            else if((tiles & 288230376151712768) == 1024) // UE is blocked
            {
                ret |= (warthog::jps3D::EAST | warthog::jps3D::DOWNSOUTHEAST | warthog::jps3D::SOUTHEAST | warthog::jps3D::DOWNEAST);
            }
            else if((tiles & 1028) == 1024) // NE is blocked
            {
                ret |= (warthog::jps3D::EAST | warthog::jps3D::DOWNSOUTHEAST | warthog::jps3D::DOWNEAST | warthog::jps3D::SOUTHEAST);
            }
            if((tiles & 281474976710912) == 256) // UNW is blocked
            {
                ret |= (warthog::jps3D::WEST | warthog::jps3D::DOWNSOUTHWEST | warthog::jps3D::SOUTHWEST | warthog::jps3D::DOWNWEST);
            }
            else if((tiles & 72057594037928192) == 256) // UW is blocked
            {
                ret |= (warthog::jps3D::WEST | warthog::jps3D::DOWNSOUTHWEST | warthog::jps3D::SOUTHWEST | warthog::jps3D::DOWNWEST);
            }
            else if((tiles & 257) == 256) // NW is blocked
            {
                ret |= (warthog::jps3D::WEST | warthog::jps3D::DOWNSOUTHWEST | warthog::jps3D::DOWNWEST | warthog::jps3D::SOUTHWEST);
            }
            break;
        case warthog::jps3D::DOWNEAST:
            if((tiles & 281474976710658) == 2) // UNW is blocked
            {
                ret |= (warthog::jps3D::NORTH | warthog::jps3D::DOWNNORTHEAST | warthog::jps3D::NORTHEAST | warthog::jps3D::DOWNNORTH);
            }
            else if((tiles & 562949953421314) == 2) // UN is blocked
            {
                ret |= (warthog::jps3D::NORTH | warthog::jps3D::DOWNNORTHEAST | warthog::jps3D::NORTHEAST | warthog::jps3D::DOWNNORTH);
            }
            else if((tiles & 3) == 2) // NW is blocked
            {
                ret |= (warthog::jps3D::NORTH | warthog::jps3D::DOWNNORTHEAST | warthog::jps3D::DOWNNORTH | warthog::jps3D::NORTHEAST);
            }
            if((tiles & warthog::lnum_US_forcedDE) == 131072) // USW is blocked
            {
                ret |= (warthog::jps3D::SOUTH | warthog::jps3D::DOWNSOUTHEAST | warthog::jps3D::SOUTHEAST | warthog::jps3D::DOWNSOUTH);
            }
            else if((tiles & warthog::lnum_US_forcedU) == 131072) // US is blocked
            {
                ret |= (warthog::jps3D::SOUTH | warthog::jps3D::DOWNSOUTHEAST | warthog::jps3D::SOUTHEAST | warthog::jps3D::DOWNSOUTH);
            }
            else if((tiles & 196608) == 131072) // SW is blocked
            {
                ret |= (warthog::jps3D::SOUTH | warthog::jps3D::DOWNSOUTHEAST | warthog::jps3D::DOWNSOUTH | warthog::jps3D::SOUTHEAST);
            }
            break;
        case warthog::jps3D::DOWNWEST:
            if((tiles & 1125899906842626) == 2) // UNE is blocked
            {
                ret |= (warthog::jps3D::NORTH | warthog::jps3D::DOWNNORTHWEST | warthog::jps3D::NORTHWEST | warthog::jps3D::DOWNNORTH);
            }
            else if((tiles & 562949953421314) == 2) // UN is blocked
            {
                ret |= (warthog::jps3D::NORTH | warthog::jps3D::DOWNNORTHWEST | warthog::jps3D::NORTHWEST | warthog::jps3D::DOWNNORTH);
            }
            else if((tiles & 6) == 2) // NE is blocked
            {
                ret |= (warthog::jps3D::NORTH | warthog::jps3D::DOWNNORTHWEST | warthog::jps3D::DOWNNORTH | warthog::jps3D::NORTHWEST);
            }
            if((tiles & warthog::lnum_US_forcedDW) == 131072) // USE is blocked
            {
                ret |= (warthog::jps3D::SOUTH | warthog::jps3D::DOWNSOUTHWEST | warthog::jps3D::SOUTHWEST | warthog::jps3D::DOWNSOUTH);
            }
            else if((tiles & warthog::lnum_US_forcedU) == 131072) // US is blocked
            {
                ret |= (warthog::jps3D::SOUTH | warthog::jps3D::DOWNSOUTHWEST | warthog::jps3D::SOUTHWEST | warthog::jps3D::DOWNSOUTH);
            }
            else if((tiles & 393216) == 131072) // SE is blocked
            {
                ret |= (warthog::jps3D::SOUTH | warthog::jps3D::DOWNSOUTHWEST | warthog::jps3D::DOWNSOUTH | warthog::jps3D::SOUTHWEST);
            }
            break;
		default:
			break;
	}
	return ret;
}

// Computes the natural neighbours of a node. 
//
// NB: the first 3 bits of the first 3 bytes of @param tiles represent
// a 3x3 block of nodes. the current node is at the centre of
// the block.
// its NW neighbour is bit 0
// its N neighbour is bit 1
// its NE neighbour is bit 2
// its W neighbour is bit 8
// ...
// its SE neighbour is bit 18 
// There are optimisations below that use bitmasks in order
// to speed up forced neighbour computation. 
uint32_t 
warthog::jps3D::compute_natural(warthog::jps3D::direction d, boost::multiprecision::uint128_t tiles)
{
	// In the shift operations below the constant values
	// correspond to bit offsets for warthog::jps::direction
	uint32_t ret = 0;
	switch(d)
	{
		case warthog::jps3D::NORTH:
			ret |= ((tiles & 2) == 2) << 0;
			break;
		case warthog::jps3D::SOUTH:
			ret |= ((tiles & 131072) == 131072) << 1;
			break;
		case warthog::jps3D::EAST: 
			ret |= ((tiles & 1024) == 1024) << 2;
			break;
		case warthog::jps3D::WEST:
			ret |= ((tiles & 256) == 256) << 3;
			break;
        case warthog::jps3D::DOWN:
			ret |= ((tiles & 8589934592) == 8589934592) << 8;
			break;
        case warthog::jps3D::UP:
			ret |= ((tiles & 144115188075855872) == 144115188075855872) << 17;
			break;
		case warthog::jps3D::NORTHWEST:
			ret |= ((tiles & 2) == 2) << 0; // N
			ret |= ((tiles & 256) == 256) << 3; // W
			ret |= ((tiles & 259) == 259) << 5; // NW
			break;
		case warthog::jps3D::NORTHEAST:
			ret |= ((tiles & 2) == 2) << 0;
			ret |= ((tiles & 1024) == 1024) << 2;
			ret |= ((tiles & 1030) == 1030) << 4;
			break;
		case warthog::jps3D::SOUTHWEST:
			ret |= ((tiles & 131072) == 131072) << 1;
			ret |= ((tiles & 256) == 256) << 3;
			ret |= ((tiles & 196864) == 196864) << 7;
			break;
		case warthog::jps3D::SOUTHEAST:
			ret |= ((tiles & 131072) == 131072) << 1;
			ret |= ((tiles & 1024) == 1024) << 2;
			ret |= ((tiles & 394240) == 394240) << 6;
			break;
        case warthog::jps3D::DOWNNORTH:
			ret |= ((tiles & 8589934592) == 8589934592) << 8;
            ret |= ((tiles & 2) == 2) << 0; 
			ret |= ((tiles & 8623489026) == 8623489026) << 9; 
			break;
        case warthog::jps3D::DOWNSOUTH:
			ret |= ((tiles & 8589934592) == 8589934592) << 8;
            ret |= ((tiles & 131072) == 131072) << 1; 
			ret |= ((tiles & 2207613321216) == 2207613321216) << 10;
			break;
        case warthog::jps3D::DOWNEAST:
			ret |= ((tiles & 8589934592) == 8589934592) << 8; 
            ret |= ((tiles & 1024) == 1024) << 2; 
			ret |= ((tiles & 25769804800) == 25769804800) << 11; 
			break;
        case warthog::jps3D::DOWNWEST:
			ret |= ((tiles & 8589934592) == 8589934592) << 8; 
            ret |= ((tiles & 256) == 256) << 3; 
			ret |= ((tiles & 12884902144) == 12884902144) << 12;
			break;
        case warthog::jps3D::UPNORTH:
			ret |= ((tiles & 144115188075855872) == 144115188075855872) << 17; 
            ret |= ((tiles & 2) == 2) << 0; 
			ret |= ((tiles & 144678138029277186) == 144678138029277186) << 18;
			break;
        case warthog::jps3D::UPSOUTH:
			ret |= ((tiles & 144115188075855872) == 144115188075855872) << 17; 
            ret |= ((tiles & 131072) == 131072) << 1; 
			ret |= ((tiles & warthog::lnum_US) == warthog::lnum_US) << 19;  
			break;
        case warthog::jps3D::UPEAST:
			ret |= ((tiles & 144115188075855872) == 144115188075855872) << 17; 
            ret |= ((tiles & 1024) == 1024) << 2; 
			ret |= ((tiles & 432345564227568640) == 432345564227568640) << 20; 
			break;
        case warthog::jps3D::UPWEST:
			ret |= ((tiles & 144115188075855872) == 144115188075855872) << 17; 
            ret |= ((tiles & 256) == 256) << 3; 
			ret |= ((tiles & 216172782113784064) == 216172782113784064) << 21; 
			break;
        case warthog::jps3D::DOWNNORTHEAST:
			ret |= ((tiles & 8589934592) == 8589934592) << 8; // D
            ret |= ((tiles & 2) == 2) << 0; // N
			ret |= ((tiles & 8623489026) == 8623489026) << 9; // DN
            ret |= ((tiles & 1024) == 1024) << 2; // E 
            ret |= ((tiles & 25769804800) == 25769804800) << 11; // DE
            ret |= ((tiles & 1030) == 1030) << 4; // NE
            ret |= ((tiles & 25870468102) == 25870468102) << 13; // DNE
			break;
        case warthog::jps3D::DOWNNORTHWEST:
			ret |= ((tiles & 8589934592) == 8589934592) << 8; // D
            ret |= ((tiles & 2) == 2) << 0; // N
			ret |= ((tiles & 8623489026) == 8623489026) << 9; // DN
            ret |= ((tiles & 256) == 256) << 3; // W 
            ret |= ((tiles & 12884902144) == 12884902144) << 12; // DW
            ret |= ((tiles & 259) == 259) << 5; // NW
            ret |= ((tiles & 12935233795) == 12935233795) << 14; // DNW
			break;
        case warthog::jps3D::DOWNSOUTHEAST:
			ret |= ((tiles & 8589934592) == 8589934592) << 8; // D
            ret |= ((tiles & 131072) == 131072) << 1; // S
			ret |= ((tiles & 2207613321216) == 2207613321216) << 10; // DS
            ret |= ((tiles & 1024) == 1024) << 2; // E 
            ret |= ((tiles & 25769804800) == 25769804800) << 11; // DE
            ret |= ((tiles & 394240) == 394240) << 6; // SE
            ret |= ((tiles & 6622839964672) == 6622839964672) << 15; // DSE
			break;
        case warthog::jps3D::DOWNSOUTHWEST:
			ret |= ((tiles & 8589934592) == 8589934592) << 8; // D
            ret |= ((tiles & 131072) == 131072) << 1; // S
			ret |= ((tiles & 2207613321216) == 2207613321216) << 10; // DS
            ret |= ((tiles & 256) == 256) << 3; // W 
            ret |= ((tiles & 12884902144) == 12884902144) << 12; // DW
            ret |= ((tiles & 196864) == 196864) << 7; // SW
            ret |= ((tiles & 3311419982080) == 3311419982080) << 16; // DSW
			break;
        case warthog::jps3D::UPNORTHEAST:
			ret |= ((tiles & 144115188075855872) == 144115188075855872) << 17; // U
            ret |= ((tiles & 2) == 2) << 0; // N
			ret |= ((tiles & 144678138029277186) == 144678138029277186) << 18; // UN
            ret |= ((tiles & 1024) == 1024) << 2; // E 
            ret |= ((tiles & 432345564227568640) == 432345564227568640) << 20; // UE
            ret |= ((tiles & 1030) == 1030) << 4; // NE
            ret |= ((tiles & 434034414087832582) == 434034414087832582) << 22; // UNE
			break;
        case warthog::jps3D::UPNORTHWEST:
			ret |= ((tiles & 144115188075855872) == 144115188075855872) << 17; // U
			ret |= ((tiles & 2) == 2) << 0; // N
			ret |= ((tiles & 144678138029277186) == 144678138029277186) << 18; // UN
			ret |= ((tiles & 256) == 256) << 3; // W 
			ret |= ((tiles & 216172782113784064) == 216172782113784064) << 21; // UW
			ret |= ((tiles & 259) == 259) << 5; // NW
			ret |= ((tiles & 217017207043916035) == 217017207043916035) << 23; // UNW
			break;
        case warthog::jps3D::UPSOUTHEAST:
			ret |= ((tiles & 144115188075855872) == 144115188075855872) << 17; // U
            ret |= ((tiles & 131072) == 131072) << 1; // S
			ret |= ((tiles & warthog::lnum_US) == warthog::lnum_US) << 19;  // US
            ret |= ((tiles & 1024) == 1024) << 2; // E 
            ret |= ((tiles & 432345564227568640) == 432345564227568640) << 20; // UE
            ret |= ((tiles & 394240) == 394240) << 6; // SE
            ret |= ((tiles & warthog::lnum_USE) == warthog::lnum_USE) << 24; // USE
			break;
        case warthog::jps3D::UPSOUTHWEST:
			ret |= ((tiles & 144115188075855872) == 144115188075855872) << 17; // U
            ret |= ((tiles & 131072) == 131072) << 1; // S
			ret |= ((tiles & warthog::lnum_US) == warthog::lnum_US) << 19;  // US
            ret |= ((tiles & 256) == 256) << 3; // W 
            ret |= ((tiles & 216172782113784064) == 216172782113784064) << 21; // UW
            ret |= ((tiles & 196864) == 196864) << 7; // SW
            ret |= ((tiles & warthog::lnum_USW) == warthog::lnum_USW) << 25; // USW
			break;
		default:
            ret |= ((tiles & 259) == 259) << 5; // NW
			ret |= ((tiles & 2) == 2) << 0; // N
            ret |= ((tiles & 1030) == 1030) << 4; // NE
            ret |= ((tiles & 256) == 256) << 3; // W
			ret |= ((tiles & 1024) == 1024) << 2; // E
            ret |= ((tiles & 196864) == 196864) << 7; // SW
            ret |= ((tiles & 131072) == 131072) << 1; // S
            ret |= ((tiles & 394240) == 394240) << 6; // SE
            ret |= ((tiles & 12935233795) == 12935233795) << 14; // DNW
            ret |= ((tiles & 8623489026) == 8623489026) << 9; // DN
            ret |= ((tiles & 25870468102) == 25870468102) << 13; // DNE
            ret |= ((tiles & 12884902144) == 12884902144) << 12; // DW
            ret |= ((tiles & 8589934592) == 8589934592) << 8; // D
            ret |= ((tiles & 25769804800) == 25769804800) << 11; // DE
            ret |= ((tiles & 3311419982080) == 3311419982080) << 16; // DSW
            ret |= ((tiles & 2207613321216) == 2207613321216) << 10; // DS
            ret |= ((tiles & 6622839964672) == 6622839964672) << 15; // DSE
            ret |= ((tiles & 217017207043916035) == 217017207043916035) << 23; // UNW
            ret |= ((tiles & 144678138029277186) == 144678138029277186) << 18; // UN
            ret |= ((tiles & 434034414087832582) == 434034414087832582) << 22; // UNE
            ret |= ((tiles & 216172782113784064) == 216172782113784064) << 21; // UW
            ret |= ((tiles & 144115188075855872) == 144115188075855872) << 17; // U
            ret |= ((tiles & 432345564227568640) == 432345564227568640) << 20; // UE
            ret |= ((tiles & warthog::lnum_USW) == warthog::lnum_USW) << 25; // USW -- 55556405003242635520
            ret |= ((tiles & warthog::lnum_US) == warthog::lnum_US) << 19; // US -- 37037603335495090688
            ret |= ((tiles & warthog::lnum_USE) == warthog::lnum_USE) << 24; // USE -- 111112810006485271552 
			break;
	}
	return ret;
}

// These rules ALLOW diagonal transitions that cut corners.
uint32_t
warthog::jps3D::compute_forced_cc(warthog::jps3D::direction d, boost::multiprecision::uint128_t tiles)
{
    // NB: to avoid branching statements, shift operations are
	// used below. The values of the shift constants correspond to 
	// direction values. 
	uint32_t ret = 0;
	switch(d)
	{
		case warthog::jps3D::NORTH:
            if((tiles & 1028) == 1024) // if East of x is blocked
			{
				ret |= (warthog::jps3D::NORTHEAST);
			}
			if((tiles & 257) == 256) // if West of x is blocked
			{
				ret |= (warthog::jps3D::NORTHWEST);
			}
            if((tiles & 289356276058554368) == 1125899906842624) // if UpEast of x is blocked
            {
                ret |= (warthog::jps3D::UPNORTHEAST);
            }
            if((tiles & 72339069014638592) == 281474976710656) // if UpWest of x is blocked
            {
                ret |= (warthog::jps3D::UPNORTHWEST);
            }
            if((tiles & 17246978048) == 67108864) // if DownEast of x is blocked
            {
                ret |= (warthog::jps3D::DOWNNORTHEAST);
            }
            if((tiles & 4311744512) == 16777216) // if DownWest of x is blocked
            {
                ret |= (warthog::jps3D::DOWNNORTHWEST);
            }
            if((tiles & 144678138029277184) == 562949953421312) // if Up of x is blocked
            {
                ret |= (warthog::jps3D::UPNORTH);
            }
            if((tiles & 8623489024) == 33554432) // if Down of x is blocked
            {
                ret |= (warthog::jps3D::DOWNNORTH);
            }
			break;
		case warthog::jps3D::SOUTH:
            if((tiles & 263168) == 262144) // if E of x is blocked
			{
				ret |= (warthog::jps3D::SOUTHEAST);
			}
			if((tiles & 65792) == 65536) // if W of x is blocked
			{
				ret |= (warthog::jps3D::SOUTHWEST);
			}
            if((tiles & warthog::lnum_NdiagUSE) == warthog::lnum_onlyUSE) // if UE of x is blocked
            {
                ret |= (warthog::jps3D::UPSOUTHEAST);
            }
            if((tiles & warthog::lnum_NdiagUSW) == warthog::lnum_onlyUSW) // if UW of x is blocked
            {
                ret |= (warthog::jps3D::UPSOUTHWEST);
            }
            if((tiles & 4415226380288) == 4398046511104) // if DE of x is blocked
            {
                ret |= (warthog::jps3D::DOWNSOUTHEAST);
            }
            if((tiles & 1103806595072) == 1099511627776) // if DW of x is blocked
            {
                ret |= (warthog::jps3D::DOWNSOUTHWEST);
            }
            if((tiles & warthog::lnum_US_forcedN) == warthog::lnum_onlyUS) // if U of x is blocked
            {
                ret |= (warthog::jps3D::UPSOUTH);
            }
            if((tiles & 2207613190144) == 2199023255552) // if D of x is blocked
            {
                ret |= (warthog::jps3D::DOWNSOUTH);
            }
			break;
		case warthog::jps3D::EAST:
            if((tiles & 6) == 4) // if N of x is blocked
			{
				ret |= (warthog::jps3D::NORTHEAST);
			}
			if((tiles & 393216) == 262144) // if S of x is blocked
			{
				ret |= (warthog::jps3D::SOUTHEAST);
			}
            if((tiles & 1688849860263936) == 562949953421312) // if UN of x is blocked
            {
                ret |= (warthog::jps3D::UPNORTHEAST);
            }
            if((tiles & warthog::lnum_WdiagUSE) == warthog::lnum_onlyUSE) // if US of x is blocked
            {
                ret |= (warthog::jps3D::UPSOUTHEAST);
            }
            if((tiles & 100663296) == 67108864) // if DN of x is blocked
            {
                ret |= (warthog::jps3D::DOWNNORTHEAST);
            }
            if((tiles & 6597069766656) == 4398046511104) // if DS of x is blocked
            {
                ret |= (warthog::jps3D::DOWNNORTHWEST);
            }
            if((tiles & 432345564227567616) == 288230376151711744) // if U of x is blocked
            {
                ret |= (warthog::jps3D::UPEAST);
            }
            if((tiles & 25769803776) == 17179869184) // if D of x is blocked
            {
                ret |= (warthog::jps3D::DOWNEAST);
            }
			break;
		case warthog::jps3D::WEST:
            if((tiles & 3) == 1) // if N of x is blocked
			{
				ret |= (warthog::jps3D::NORTHWEST);
			}
			if((tiles & 196608) == 65536) // if S of x is blocked
			{
				ret |= (warthog::jps3D::SOUTHWEST);
			}
            if((tiles & 844424930131968) == 281474976710656) // if UN of x is blocked
            {
                ret |= (warthog::jps3D::UPNORTHWEST);
            }
            if((tiles & warthog::lnum_EdiagUSW) == warthog::lnum_onlyUSE) // if US of x is blocked
            {
                ret |= (warthog::jps3D::UPSOUTHWEST);
            }
            if((tiles & 50331648) == 16777216) // if DN of x is blocked
            {
                ret |= (warthog::jps3D::DOWNNORTHWEST);
            }
            if((tiles & 3298534883328) == 1099511627776) // if DS of x is blocked
            {
                ret |= (warthog::jps3D::DOWNSOUTHWEST);
            }
            if((tiles & 216172782113783808) == 72057594037927936) // if U of x is blocked
            {
                ret |= (warthog::jps3D::UPWEST);
            }
            if((tiles & 12884901888) == 4294967296) // if D of x is blocked
            {
                ret |= (warthog::jps3D::DOWNWEST);
            }
			break;
        case warthog::jps3D::UP:
            if((tiles & 562949953421314) == 562949953421312) // if N of x is blocked
			{
				ret |= (warthog::jps3D::UPNORTH);
			}
			if((tiles & warthog::lnum_US_forcedU) == warthog::lnum_onlyUS) // if S of x is blocked
			{
				ret |= (warthog::jps3D::UPSOUTH);
			}
            if((tiles & 288230376151712768) == 288230376151711744) // if E of x is blocked
            {
                ret |= (warthog::jps3D::UPEAST);
            }
            if((tiles & 72057594037928192) == 72057594037927936) // if W of x is blocked
            {
                ret |= (warthog::jps3D::UPWEST);
            }
            if((tiles & 1125899906842628) == 1125899906842624) // if NE of x is blocked
            {
                ret |= (warthog::jps3D::UPNORTHEAST);
            }
            if((tiles & 281474976710657) == 281474976710656) // if NW of x is blocked
            {
                ret |= (warthog::jps3D::UPNORTHWEST);
            }
            if((tiles & warthog::lnum_DdiagUSE) == warthog::lnum_onlyUSE) // if SE of x is blocked
            {
                ret |= (warthog::jps3D::UPSOUTHEAST);
            }
            if((tiles & warthog::lnum_SWandUSW) == warthog::lnum_onlyUSW) // if SW of x is blocked
            {
                ret |= (warthog::jps3D::UPSOUTHWEST);
            }
			break;
        case warthog::jps3D::DOWN:
            if((tiles & 33554434) == 33554432) // if N of x is blocked
			{
				ret |= (warthog::jps3D::DOWNNORTH);
			}
			if((tiles & 2199023386624) == 2199023255552) // if S of x is blocked
			{
				ret |= (warthog::jps3D::DOWNSOUTH);
			}
            if((tiles & 17179870208) == 17179869184) // if E of x is blocked
            {
                ret |= (warthog::jps3D::DOWNEAST);
            }
            if((tiles & 4294967552) == 4294967296) // if W of x is blocked
            {
                ret |= (warthog::jps3D::DOWNWEST);
            }
            if((tiles & 67108868) == 67108864) // if NE of x is blocked
            {
                ret |= (warthog::jps3D::DOWNNORTHEAST);
            }
            if((tiles & 16777217) == 16777216) // if NW of x is blocked
            {
                ret |= (warthog::jps3D::DOWNNORTHWEST);
            }
            if((tiles & 4398046773248) == 4398046511104) // if SE of x is blocked
            {
                ret |= (warthog::jps3D::DOWNSOUTHEAST);
            }
            if((tiles & 1099511693312) == 1099511627776) // if SW of x is blocked
            {
                ret |= (warthog::jps3D::DOWNSOUTHWEST);
            }
			break;
        case warthog::jps3D::NORTHEAST:
            if((tiles & 257) == 1) // W is blocked
            {
                ret |= (warthog::jps3D::NORTHWEST);
            }
            if((tiles & 393216) == 262144) // S is blocked
            {
                ret |= (warthog::jps3D::SOUTHEAST);
            }
            if((tiles & warthog::lnum_WdiagUSE) == warthog::lnum_onlyUSE) // US is blocked
            {
                ret |= (warthog::jps3D::UPSOUTHEAST);
            }
            if((tiles & 72339069014638592) == 281474976710656) // UW is blocked
            {
                ret |= (warthog::jps3D::UPNORTHWEST);
            }
            if((tiles & 144678138029277184) == 562949953421312) // U is blocked
            {
                ret |= (warthog::jps3D::UPNORTH);
            }
            if((tiles & 145241087982698496) == 1125899906842624) // U is blocked
            {
                ret |= (warthog::jps3D::UPNORTHEAST);
            }
            if((tiles & 432345564227567616) == 288230376151711744) // U is blocked
            {
                ret |= (warthog::jps3D::UPEAST);
            }
            if((tiles & 6597069766656) == 4398046511104) // DS is blocked
            {
                ret |= (warthog::jps3D::DOWNSOUTHEAST);
            }
            if((tiles & 4311744512) == 16777216) // DW is blocked
            {
                ret |= (warthog::jps3D::DOWNNORTHWEST);
            }
            if((tiles & 8623489024) == 33554432) // D is blocked
            {
                ret |= (warthog::jps3D::DOWNNORTH);
            }
            if((tiles & 8657043456) == 67108864) // D is blocked
            {
                ret |= (warthog::jps3D::DOWNNORTHEAST);
            }
            if((tiles & 25769803776) == 17179869184) // D is blocked
            {
                ret |= (warthog::jps3D::DOWNEAST);
            }
            break;
        case warthog::jps3D::NORTHWEST:
            if((tiles & 1028) == 4) // E is blocked
            {
                ret |= (warthog::jps3D::NORTHEAST);
            }
            if((tiles & 393216) == 262144) // S is blocked
            {
                ret |= (warthog::jps3D::SOUTHWEST);
            }
            if((tiles & warthog::lnum_EdiagUSW) == warthog::lnum_onlyUSW) // US is blocked
            {
                ret |= (warthog::jps3D::UPSOUTHWEST);
            }
            if((tiles & 289356276058554368) == 1125899906842624) // UE is blocked
            {
                ret |= (warthog::jps3D::UPNORTHEAST);
            }
            if((tiles & 144678138029277184) == 562949953421312) // U is blocked
            {
                ret |= (warthog::jps3D::UPNORTH);
            }
            if((tiles & 144396663052566528) == 281474976710656) // U is blocked
            {
                ret |= (warthog::jps3D::UPNORTHWEST);
            }
            if((tiles & 216172782113783808) == 72057594037927936) // U is blocked
            {
                ret |= (warthog::jps3D::UPWEST);
            }
            if((tiles & 3298534883328) == 1099511627776) // DS is blocked
            {
                ret |= (warthog::jps3D::DOWNSOUTHWEST);
            }
            if((tiles & 17246978048) == 67108864) // DE is blocked
            {
                ret |= (warthog::jps3D::DOWNNORTHEAST);
            }
            if((tiles & 8623489024) == 33554432) // D is blocked
            {
                ret |= (warthog::jps3D::DOWNNORTH);
            }
            if((tiles & 8606711808) == 16777216) // D is blocked
            {
                ret |= (warthog::jps3D::DOWNNORTHWEST);
            }
            if((tiles & 12884901888) == 4294967296) // D is blocked
            {
                ret |= (warthog::jps3D::DOWNWEST);
            }
            break;
        case warthog::jps3D::SOUTHEAST:
            if((tiles & 6) == 4) // N is blocked
            {
                ret |= (warthog::jps3D::NORTHEAST);
            }
            if((tiles & 65792) == 65536) // W is blocked
            {
                ret |= (warthog::jps3D::SOUTHWEST);
            }
            if((tiles & 1688849860263936) == 1125899906842624) // UN is blocked
            {
                ret |= (warthog::jps3D::UPNORTHEAST);
            }
            if((tiles & warthog::lnum_NdiagUSW) == warthog::lnum_onlyUSW) // UW is blocked
            {
                ret |= (warthog::jps3D::UPSOUTHWEST);
            }
            if((tiles & 432345564227567616) == 288230376151711744) // U is blocked
            {
                ret |= (warthog::jps3D::UPEAST);
            }
            if((tiles & warthog::lnum_US_forcedNW) == warthog::lnum_onlyUSE) // U is blocked
            {
                ret |= (warthog::jps3D::UPSOUTHEAST);
            }
            if((tiles & warthog::lnum_US_forcedN) == warthog::lnum_onlyUS) // U is blocked
            {
                ret |= (warthog::jps3D::UPSOUTH);
            }
            if((tiles & 100663296) == 67108864) // DN is blocked
            {
                ret |= (warthog::jps3D::DOWNNORTHEAST);
            }
            if((tiles & 1103806595072) == 1099511627776) // DW is blocked
            {
                ret |= (warthog::jps3D::DOWNSOUTHWEST);
            }
            if((tiles & 25769803776) == 17179869184) // D is blocked
            {
                ret |= (warthog::jps3D::DOWNEAST);
            }
            if((tiles & 4406636445696) == 4398046511104) // D is blocked
            {
                ret |= (warthog::jps3D::DOWNSOUTHEAST);
            }
            if((tiles & 2207613190144) == 2199023255552) // D is blocked
            {
                ret |= (warthog::jps3D::DOWNSOUTH);
            }
            break;
        case warthog::jps3D::SOUTHWEST:
            if((tiles & 3) == 1) // N is blocked
            {
                ret |= (warthog::jps3D::NORTHWEST);
            }
            if((tiles & 263168) == 262144) // E is blocked
            {
                ret |= (warthog::jps3D::SOUTHEAST);
            }
            if((tiles & 262144) == 262144) // UN is blocked
            {
                ret |= (warthog::jps3D::UPNORTHWEST);
            }
            if((tiles & warthog::lnum_NdiagUSE) == warthog::lnum_onlyUSE) // UE is blocked
            {
                ret |= (warthog::jps3D::UPSOUTHEAST);
            }
            if((tiles & 216172782113783808) == 72057594037927936) // U is blocked
            {
                ret |= (warthog::jps3D::UPWEST);
            }
            if((tiles & warthog::lnum_US_forcedNE) == warthog::lnum_onlyUSW) // U is blocked
            {
                ret |= (warthog::jps3D::UPSOUTHWEST);
            }
            if((tiles & warthog::lnum_US_forcedN) == warthog::lnum_onlyUS) // U is blocked
            {
                ret |= (warthog::jps3D::UPSOUTH);
            }
            if((tiles & 50331648) == 16777216) // DN is blocked
            {
                ret |= (warthog::jps3D::DOWNNORTHWEST);
            }
            if((tiles & 4415226380288) == 4398046511104) // DE is blocked
            {
                ret |= (warthog::jps3D::DOWNSOUTHEAST);
            }
            if((tiles & 12884901888) == 4294967296) // D is blocked
            {
                ret |= (warthog::jps3D::DOWNWEST);
            }
            if((tiles & 1108101562368) == 1099511627776) // D is blocked
            {
                ret |= (warthog::jps3D::DOWNSOUTHWEST);
            }
            if((tiles & 2207613190144) == 2199023255552) // D is blocked
            {
                ret |= (warthog::jps3D::DOWNSOUTH);
            }
            break;
        case warthog::jps3D::UPNORTH:
            if((tiles & 288230376151712768) == 288230376151711744) // E is blocked
            {
                ret |= (warthog::jps3D::UPEAST);
            }
            if((tiles & 1125899906843648) == 1125899906842624) // E is blocked
            {
                ret |= (warthog::jps3D::UPNORTHEAST);
            }
            if((tiles & 1028) == 4) // E is blocked
            {
                ret |= (warthog::jps3D::NORTHEAST);
            }
            if((tiles & 72057594037928192) == 72057594037927936) // W is blocked
            {
                ret |= (warthog::jps3D::UPWEST);
            }
            if((tiles & 281474976710912) == 281474976710656) // W is blocked
            {
                ret |= (warthog::jps3D::UPNORTHWEST);
            }
            if((tiles & 257) == 1) // W is blocked
            {
                ret |= (warthog::jps3D::NORTHWEST);
            }
            if((tiles & warthog::lnum_DdiagUSE) == warthog::lnum_onlyUSE) // SE is blocked
            {
                ret |= (warthog::jps3D::UPSOUTHEAST);
            }
            if((tiles & warthog::lnum_US_forcedU) == warthog::lnum_onlyUS) // S is blocked
            {
                ret |= (warthog::jps3D::UPSOUTH);
            }
            if((tiles & warthog::lnum_SWandUSW) == warthog::lnum_onlyUSW) // SW is blocked
            {
                ret |= (warthog::jps3D::UPSOUTHWEST);
            }
            if((tiles & 4415226380288) == 4398046511104) // DE is blocked
            {
                ret |= (warthog::jps3D::DOWNSOUTHEAST);
            }
            if((tiles & 2207613190144) == 2199023255552) // D is blocked
            {
                ret |= (warthog::jps3D::DOWNSOUTH);
            }
            if((tiles & 1103806595072) == 1099511627776) // DW is blocked
            {
                ret |= (warthog::jps3D::DOWNSOUTHWEST);
            }
            break;
        case warthog::jps3D::UPSOUTH:
            if((tiles & 263168) == 262144) // E is blocked
            {
                ret |= (warthog::jps3D::SOUTHEAST);
            }
            if((tiles & warthog::lnum_US_forcedDNE) == warthog::lnum_onlyUSW) // E is blocked
            {
                ret |= (warthog::jps3D::UPSOUTHEAST);
            }
            if((tiles & 288230376151712768) == 288230376151711744) // E is blocked
            {
                ret |= (warthog::jps3D::UPEAST);
            }
            if((tiles & 65792) == 65536) // W is blocked
            {
                ret |= (warthog::jps3D::SOUTHWEST);
            }
            if((tiles & lnum_US_forcedDNW) == warthog::lnum_onlyUSW) // W is blocked
            {
                ret |= (warthog::jps3D::UPSOUTHWEST);
            }
            if((tiles & 72057594037928192) == 72057594037927936) // W is blocked
            {
                ret |= (warthog::jps3D::UPWEST);
            }
            if((tiles & 1125899906842628) == 1125899906842624) // NE is blocked
            {
                ret |= (warthog::jps3D::UPNORTHEAST);
            }
            if((tiles & 562949953421314) == 562949953421312) // N is blocked
            {
                ret |= (warthog::jps3D::UPNORTH);
            }
            if((tiles & 281474976710657) == 281474976710656) // NW is blocked
            {
                ret |= (warthog::jps3D::UPNORTHWEST);
            }
            if((tiles & 4415226380288) == 4398046511104) // DE is blocked
            {
                ret |= (warthog::jps3D::DOWNSOUTHEAST);
            }
            if((tiles & 2207613190144) == 2199023255552) // D is blocked
            {
                ret |= (warthog::jps3D::DOWNSOUTH);
            }
            if((tiles & 1103806595072) == 1099511627776) // DW is blocked
            {
                ret |= (warthog::jps3D::DOWNSOUTHWEST);
            }
            break;
        case warthog::jps3D::UPEAST:
            if((tiles & 6) == 4) // N is blocked
            {
                ret |= (warthog::jps3D::NORTHEAST);
            }
            if((tiles & 1125899906842626) == 1125899906842624) // N is blocked
            {
                ret |= (warthog::jps3D::UPNORTHEAST);
            }
            if((tiles & 562949953421314) == 562949953421312) // N is blocked
            {
                ret |= (warthog::jps3D::UPNORTH);
            }
            if((tiles & 393216) == 262144) // S is blocked
            {
                ret |= (warthog::jps3D::SOUTHEAST);
            }
            if((tiles & warthog::lnum_US_forcedDW) == warthog::lnum_onlyUSE) // S is blocked
            {
                ret |= (warthog::jps3D::UPSOUTHEAST);
            }
            if((tiles & warthog::lnum_US_forcedU) == warthog::lnum_onlyUS) // S is blocked
            {
                ret |= (warthog::jps3D::UPSOUTH);
            }
            if((tiles & 281474976710657) == 281474976710656) // NW is blocked
            {
                ret |= (warthog::jps3D::UPNORTHWEST);
            }
            if((tiles & 72057594037928192) == 72057594037927936) // W is blocked
            {
                ret |= (warthog::jps3D::UPWEST);
            }
            if((tiles & warthog::lnum_SWandUSW) == warthog::lnum_onlyUSW) // SW is blocked
            {
                ret |= (warthog::jps3D::UPSOUTHWEST);
            }
            if((tiles & 100663296) == 67108864) // DN is blocked
            {
                ret |= (warthog::jps3D::DOWNNORTHEAST);
            }
            if((tiles & 25769803776) == 17179869184) // D is blocked
            {
                ret |= (warthog::jps3D::DOWNEAST);
            }
            if((tiles & 6597069766656) == 4398046511104) // DS is blocked
            {
                ret |= (warthog::jps3D::DOWNSOUTHEAST);
            }
            break;
        case warthog::jps3D::UPWEST:
            if((tiles & 3) == 1) // N is blocked
            {
                ret |= (warthog::jps3D::NORTHWEST);
            }
            if((tiles & 281474976710658) == 281474976710656) // N is blocked
            {
                ret |= (warthog::jps3D::UPNORTHWEST);
            }
            if((tiles & 562949953421314) == 562949953421312) // N is blocked
            {
                ret |= (warthog::jps3D::UPNORTH);
            }
            if((tiles & 196608) == 65536) // S is blocked
            {
                ret |= (warthog::jps3D::SOUTHWEST);
            }
            if((tiles & warthog::lnum_US_forcedDE) == warthog::lnum_onlyUSW) // S is blocked
            {
                ret |= (warthog::jps3D::UPSOUTHWEST);
            }
            if((tiles & warthog::lnum_US_forcedU) == warthog::lnum_onlyUS) // S is blocked
            {
                ret |= (warthog::jps3D::UPSOUTH);
            }
            if((tiles & 1125899906842628) == 1125899906842624) // NE is blocked
            {
                ret |= (warthog::jps3D::UPNORTHEAST);
            }
            if((tiles & 288230376151712768) == 288230376151711744) // E is blocked
            {
                ret |= (warthog::jps3D::UPEAST);
            }
            if((tiles & warthog::lnum_DdiagUSE) == warthog::lnum_onlyUSE) // SE is blocked
            {
                ret |= (warthog::jps3D::UPSOUTHEAST);
            }
            if((tiles & 50331648) == 16777216) // DN is blocked
            {
                ret |= (warthog::jps3D::DOWNNORTHWEST);
            }
            if((tiles & 12884901888) == 4294967296) // D is blocked
            {
                ret |= (warthog::jps3D::DOWNWEST);
            }
            if((tiles & 3298534883328) == 1099511627776) // DS is blocked
            {
                ret |= (warthog::jps3D::DOWNSOUTHWEST);
            }
            break;
        case warthog::jps3D::DOWNNORTH:
            if((tiles & 17179870208) == 17179869184) // E is blocked
            {
                ret |= (warthog::jps3D::DOWNEAST);
            }
            if((tiles & 67109888) == 67108864) // E is blocked
            {
                ret |= (warthog::jps3D::DOWNNORTHEAST);
            }
            if((tiles & 1028) == 4) // E is blocked
            {
                ret |= (warthog::jps3D::NORTHEAST);
            }
            if((tiles & 4294967552) == 4294967296) // W is blocked
            {
                ret |= (warthog::jps3D::DOWNWEST);
            }
            if((tiles & 16777472) == 16777216) // W is blocked
            {
                ret |= (warthog::jps3D::DOWNNORTHWEST);
            }
            if((tiles & 257) == 1) // W is blocked
            {
                ret |= (warthog::jps3D::NORTHWEST);
            }
            if((tiles & 4398046773248) == 4398046511104) // SE is blocked
            {
                ret |= (warthog::jps3D::DOWNSOUTHEAST);
            }
            if((tiles & 2199023386624) == 2199023255552) // S is blocked
            {
                ret |= (warthog::jps3D::DOWNSOUTH);
            }
            if((tiles & 1099511693312) == 1099511627776) // SW is blocked
            {
                ret |= (warthog::jps3D::DOWNSOUTHWEST);
            }
            if((tiles & warthog::lnum_NdiagUSE) == warthog::lnum_onlyUSE) // UE is blocked
            {
                ret |= (warthog::jps3D::UPSOUTHEAST);
            }
            if((tiles & warthog::lnum_US_forcedN) == warthog::lnum_onlyUS) // U is blocked
            {
                ret |= (warthog::jps3D::UPSOUTH);
            }
            if((tiles & warthog::lnum_NdiagUSW) == warthog::lnum_onlyUSW) // UW is blocked
            {
                ret |= (warthog::jps3D::UPSOUTHWEST);
            }
            break;
        case warthog::jps3D::DOWNSOUTH:
            if((tiles & 263168) == 262144) // E is blocked
            {
                ret |= (warthog::jps3D::SOUTHEAST);
            }
            if((tiles & 4398046512128) == 4398046511104) // E is blocked
            {
                ret |= (warthog::jps3D::DOWNSOUTHEAST);
            }
            if((tiles & 17179870208) == 17179869184) // E is blocked
            {
                ret |= (warthog::jps3D::DOWNEAST);
            }
            if((tiles & 65792) == 65536) // W is blocked
            {
                ret |= (warthog::jps3D::SOUTHWEST);
            }
            if((tiles & 1099511628032) == 1099511627776) // W is blocked
            {
                ret |= (warthog::jps3D::DOWNSOUTHWEST);
            }
            if((tiles & 4294967552) == 4294967296) // W is blocked
            {
                ret |= (warthog::jps3D::DOWNWEST);
            }
            if((tiles & 67108868) == 67108864) // NE is blocked
            {
                ret |= (warthog::jps3D::DOWNNORTHEAST);
            }
            if((tiles & 33554434) == 33554432) // N is blocked
            {
                ret |= (warthog::jps3D::DOWNNORTH);
            }
            if((tiles & 16777217) == 16777216) // NW is blocked
            {
                ret |= (warthog::jps3D::DOWNNORTHWEST);
            }
            if((tiles & warthog::lnum_NdiagUSE) == warthog::lnum_onlyUSE) // UE is blocked
            {
                ret |= (warthog::jps3D::UPSOUTHEAST);
            }
            if((tiles & warthog::lnum_US_forcedN) == warthog::lnum_onlyUS) // U is blocked
            {
                ret |= (warthog::jps3D::UPSOUTH);
            }
            if((tiles & warthog::lnum_NdiagUSW) == warthog::lnum_onlyUSW) // UW is blocked
            {
                ret |= (warthog::jps3D::UPSOUTHWEST);
            }
            break;
        case warthog::jps3D::DOWNEAST:
            if((tiles & 6) == 4) // N is blocked
            {
                ret |= (warthog::jps3D::NORTHEAST);
            }
            if((tiles & 67108866) == 67108864) // N is blocked
            {
                ret |= (warthog::jps3D::DOWNNORTHEAST);
            }
            if((tiles & 33554434) == 33554432) // N is blocked
            {
                ret |= (warthog::jps3D::DOWNNORTH);
            }
            if((tiles & 393216) == 262144) // S is blocked
            {
                ret |= (warthog::jps3D::SOUTHEAST);
            }
            if((tiles & 4398046642176) == 4398046511104) // S is blocked
            {
                ret |= (warthog::jps3D::DOWNSOUTHEAST);
            }
            if((tiles & 2199023386624) == 2199023255552) // S is blocked
            {
                ret |= (warthog::jps3D::DOWNSOUTH);
            }
            if((tiles & 16777217) == 16777216) // NW is blocked
            {
                ret |= (warthog::jps3D::DOWNNORTHWEST);
            }
            if((tiles & 4294967552) == 4294967296) // W is blocked
            {
                ret |= (warthog::jps3D::DOWNWEST);
            }
            if((tiles & 1099511693312) == 1099511627776) // SW is blocked
            {
                ret |= (warthog::jps3D::DOWNSOUTHWEST);
            }
            if((tiles & 1688849860263936) == 1125899906842624) // UN is blocked
            {
                ret |= (warthog::jps3D::UPNORTHEAST);
            }
            if((tiles & 432345564227567616) == 288230376151711744) // U is blocked
            {
                ret |= (warthog::jps3D::UPEAST);
            }
            if((tiles & warthog::lnum_WdiagUSE) == warthog::lnum_onlyUSE) // US is blocked
            {
                ret |= (warthog::jps3D::UPSOUTHEAST);
            }
            break;
        case warthog::jps3D::DOWNWEST:
            if((tiles & 3) == 1) // N is blocked
            {
                ret |= (warthog::jps3D::NORTHWEST);
            }
            if((tiles & 16777218) == 16777216) // N is blocked
            {
                ret |= (warthog::jps3D::DOWNNORTHWEST);
            }
            if((tiles & 33554434) == 33554432) // N is blocked
            {
                ret |= (warthog::jps3D::DOWNNORTH);
            }
            if((tiles & 196608) == 65536) // S is blocked
            {
                ret |= (warthog::jps3D::SOUTHWEST);
            }
            if((tiles & 1099511758848) == 1099511627776) // S is blocked
            {
                ret |= (warthog::jps3D::DOWNSOUTHWEST);
            }
            if((tiles & 2199023386624) == 2199023255552) // S is blocked
            {
                ret |= (warthog::jps3D::DOWNSOUTH);
            }
            if((tiles & 67108868) == 67108864) // NE is blocked
            {
                ret |= (warthog::jps3D::DOWNNORTHEAST);
            }
            if((tiles & 17179870208) == 17179869184) // E is blocked
            {
                ret |= (warthog::jps3D::DOWNEAST);
            }
            if((tiles & 4398046773248) == 4398046511104) // SE is blocked
            {
                ret |= (warthog::jps3D::DOWNSOUTHEAST);
            }
            if((tiles & 844424930131968) == 281474976710656) // UN is blocked
            {
                ret |= (warthog::jps3D::UPNORTHWEST);
            }
            if((tiles & 216172782113783808) == 72057594037927936) // U is blocked
            {
                ret |= (warthog::jps3D::UPWEST);
            }
            if((tiles & warthog::lnum_EdiagUSW) == warthog::lnum_onlyUSW) // US is blocked
            {
                ret |= (warthog::jps3D::UPSOUTHWEST);
            }
            break;
        case warthog::jps3D::UPNORTHEAST:
            if((tiles & 4311744512) == 16777216) // DW is blocked
            {
                ret |= (warthog::jps3D::DOWNNORTHWEST);
            }
            if((tiles & 6597069766656) == 4398046511104) // DS is blocked
            {
                ret |= (warthog::jps3D::DOWNSOUTHEAST);
            }
            if((tiles & 8623489024) == 33554432) // D is blocked
            {
                ret |= (warthog::jps3D::DOWNNORTH);
            }
            if((tiles & 8657043456) == 67108864) // D is blocked
            {
                ret |= (warthog::jps3D::DOWNNORTHEAST);
            }
            if((tiles & 25769803776) == 17179869184) // D is blocked
            {
                ret |= (warthog::jps3D::DOWNEAST);
            }
        case warthog::jps3D::UPNORTHWEST:
            if((tiles & 17246978048) == 67108864) // DE is blocked
            {
                ret |= (warthog::jps3D::DOWNNORTHEAST);
            }
            if((tiles & 3298534883328) == 1099511627776) // DS is blocked
            {
                ret |= (warthog::jps3D::DOWNSOUTHWEST);
            }
            if((tiles & 8623489024) == 33554432) // D is blocked
            {
                ret |= (warthog::jps3D::DOWNNORTH);
            }
            if((tiles & 8606711808) == 16777216) // D is blocked
            {
                ret |= (warthog::jps3D::DOWNNORTHWEST);
            }
            if((tiles & 12884901888) == 4294967296) // D is blocked
            {
                ret |= (warthog::jps3D::DOWNWEST);
            }
        case warthog::jps3D::UPSOUTHEAST:
            if((tiles & 67108866) == 67108864) // N is blocked
            {
                ret |= (warthog::jps3D::DOWNNORTHEAST);
            }
            if((tiles & 1103806595072) == 1099511627776) // DW is blocked
            {
                ret |= (warthog::jps3D::DOWNSOUTHWEST);
            }
            if((tiles & 25769803776) == 17179869184) // D is blocked
            {
                ret |= (warthog::jps3D::DOWNEAST);
            }
            if((tiles & 4406636445696) == 4398046511104) // D is blocked
            {
                ret |= (warthog::jps3D::DOWNSOUTHEAST);
            }
            if((tiles & 2207613190144) == 2199023255552) // D is blocked
            {
                ret |= (warthog::jps3D::DOWNSOUTH);
            }
        case warthog::jps3D::UPSOUTHWEST:
            if((tiles & 50331648) == 16777216) // DN is blocked
            {
                ret |= (warthog::jps3D::DOWNNORTHWEST);
            }
            if((tiles & 4415226380288) == 4398046511104) // DE is blocked
            {
                ret |= (warthog::jps3D::DOWNSOUTHEAST);
            }
            if((tiles & 12884901888) == 4294967296) // D is blocked
            {
                ret |= (warthog::jps3D::DOWNWEST);
            }
            if((tiles & 1108101562368) == 1099511627776) // D is blocked
            {
                ret |= (warthog::jps3D::DOWNSOUTHWEST);
            }
            if((tiles & 2207613190144) == 2199023255552) // D is blocked
            {
                ret |= (warthog::jps3D::DOWNSOUTH);
            }
        case warthog::jps3D::DOWNNORTHEAST:
            if((tiles & 72339069014638592) == 281474976710656) // UW is blocked
            {
                ret |= (warthog::jps3D::UPNORTHWEST);
            }
            if((tiles & warthog::lnum_WdiagUSE) == warthog::lnum_onlyUSE) // US is blocked
            {
                ret |= (warthog::jps3D::UPSOUTHEAST);
            }
            if((tiles & 144678138029277184) == 562949953421312) // U is blocked
            {
                ret |= (warthog::jps3D::UPNORTH);
            }
            if((tiles & 145241087982698496) == 1125899906842624) // U is blocked
            {
                ret |= (warthog::jps3D::UPNORTHEAST);
            }
            if((tiles & 432345564227567616) == 288230376151711744) // U is blocked
            {
                ret |= (warthog::jps3D::UPEAST);
            }
        case warthog::jps3D::DOWNNORTHWEST:
            if((tiles & 289356276058554368) == 288230376151711744) // UE is blocked
            {
                ret |= (warthog::jps3D::UPNORTHEAST);
            }
            if((tiles & warthog::lnum_EdiagUSW) == warthog::lnum_onlyUSW) // US is blocked
            {
                ret |= (warthog::jps3D::UPSOUTHWEST);
            }
            if((tiles & 144678138029277184) == 562949953421312) // U is blocked
            {
                ret |= (warthog::jps3D::UPNORTH);
            }
            if((tiles & 144396663052566528) == 281474976710656) // U is blocked
            {
                ret |= (warthog::jps3D::UPNORTHWEST);
            }
            if((tiles & 216172782113783808) == 72057594037927936) // U is blocked
            {
                ret |= (warthog::jps3D::UPWEST);
            }
        case warthog::jps3D::DOWNSOUTHEAST:
            if((tiles & 1688849860263936) == 1125899906842624) // UN is blocked
            {
                ret |= (warthog::jps3D::UPNORTHEAST);
            }
            if((tiles & warthog::lnum_NdiagUSW) == warthog::lnum_onlyUSW) // UW is blocked
            {
                ret |= (warthog::jps3D::UPSOUTHWEST);
            }
            if((tiles & 432345564227567616) == 288230376151711744) // U is blocked
            {
                ret |= (warthog::jps3D::UPEAST);
            }
            if((tiles & warthog::lnum_US_forcedNW) == warthog::lnum_onlyUSE) // U is blocked
            {
                ret |= (warthog::jps3D::UPSOUTHEAST);
            }
            if((tiles & warthog::lnum_US_forcedN) == warthog::lnum_onlyUS) // U is blocked
            {
                ret |= (warthog::jps3D::UPSOUTH);
            }
        case warthog::jps3D::DOWNSOUTHWEST:
            if((tiles & 844424930131968) == 281474976710656) // UN is blocked
            {
                ret |= (warthog::jps3D::UPNORTHWEST);
            }
            if((tiles & warthog::lnum_NdiagUSE) == warthog::lnum_onlyUSE) // UE is blocked
            {
                ret |= (warthog::jps3D::UPSOUTHEAST);
            }
            if((tiles & 216172782113783808) == 72057594037927936) // U is blocked
            {
                ret |= (warthog::jps3D::UPWEST);
            }
            if((tiles & warthog::lnum_US_forcedNE) == warthog::lnum_onlyUSW) // U is blocked
            {
                ret |= (warthog::jps3D::UPSOUTHWEST);
            }
            if((tiles & warthog::lnum_US_forcedN) == warthog::lnum_onlyUS) // U is blocked
            {
                ret |= (warthog::jps3D::UPSOUTH);
            }
		default:
			break;
	}
    // std::cout << "FORCED:    " << std::bitset<32>(ret) << "\n"; 
	return ret;
}

// These rules ALLOW diagonal transitions that cut corners.
uint32_t
warthog::jps3D::compute_natural_cc(warthog::jps3D::direction d, boost::multiprecision::uint128_t tiles)
{
	// In the shift operations below the constant values
	// correspond to bit offsets for warthog::jps::direction
	uint32_t ret = 0;
	switch(d)
	{
		case warthog::jps3D::NORTH:
			ret |= ((tiles & 2) == 2) << 0;
			break;
		case warthog::jps3D::SOUTH:
			ret |= ((tiles & 131072) == 131072) << 1;
			break;
		case warthog::jps3D::EAST: 
			ret |= ((tiles & 1024) == 1024) << 2;
			break;
		case warthog::jps3D::WEST:
			ret |= ((tiles & 256) == 256) << 3;
			break;
        case warthog::jps3D::DOWN:
			ret |= ((tiles & 8589934592) == 8589934592) << 8;
			break;
        case warthog::jps3D::UP:
			ret |= ((tiles & 144115188075855872) == 144115188075855872) << 17;
			break;
		case warthog::jps3D::NORTHWEST:
			ret |= ((tiles & 2) == 2) << 0; // N
			ret |= ((tiles & 256) == 256) << 3; // W
			ret |= ((tiles & 1) == 1) << 5; // NW
			break;
		case warthog::jps3D::NORTHEAST:
			ret |= ((tiles & 2) == 2) << 0;
			ret |= ((tiles & 1024) == 1024) << 2;
			ret |= ((tiles & 4) == 4) << 4;
			break;
		case warthog::jps3D::SOUTHWEST:
			ret |= ((tiles & 131072) == 131072) << 1;
			ret |= ((tiles & 256) == 256) << 3;
			ret |= ((tiles & 65536) == 65536) << 7;
			break;
		case warthog::jps3D::SOUTHEAST:
			ret |= ((tiles & 131072) == 131072) << 1;
			ret |= ((tiles & 1024) == 1024) << 2;
			ret |= ((tiles & 262144) == 262144) << 6;
			break;
        case warthog::jps3D::DOWNNORTH:
			ret |= ((tiles & 8589934592) == 8589934592) << 8;
            ret |= ((tiles & 2) == 2) << 0; 
			ret |= ((tiles & 33554432) == 33554432) << 9; 
			break;
        case warthog::jps3D::DOWNSOUTH:
			ret |= ((tiles & 8589934592) == 8589934592) << 8;
            ret |= ((tiles & 131072) == 131072) << 1; 
			ret |= ((tiles & 2199023255552) == 2199023255552) << 10;
			break;
        case warthog::jps3D::DOWNEAST:
			ret |= ((tiles & 8589934592) == 8589934592) << 8; 
            ret |= ((tiles & 1024) == 1024) << 2; 
			ret |= ((tiles & 17179869184) == 17179869184) << 11; 
			break;
        case warthog::jps3D::DOWNWEST:
			ret |= ((tiles & 8589934592) == 8589934592) << 8; 
            ret |= ((tiles & 256) == 256) << 3; 
			ret |= ((tiles & 4294967296) == 4294967296) << 12;
			break;
        case warthog::jps3D::UPNORTH:
			ret |= ((tiles & 144115188075855872) == 144115188075855872) << 17; 
            ret |= ((tiles & 2) == 2) << 0; 
			ret |= ((tiles & 562949953421312) == 562949953421312) << 18;
			break;
        case warthog::jps3D::UPSOUTH:
			ret |= ((tiles & 144115188075855872) == 144115188075855872) << 17; 
            ret |= ((tiles & 131072) == 131072) << 1; 
			ret |= ((tiles & warthog::lnum_onlyUS) == warthog::lnum_onlyUS) << 19;  
			break;
        case warthog::jps3D::UPEAST:
			ret |= ((tiles & 144115188075855872) == 144115188075855872) << 17; 
            ret |= ((tiles & 1024) == 1024) << 2; 
			ret |= ((tiles & 288230376151711744) == 288230376151711744) << 20; 
			break;
        case warthog::jps3D::UPWEST:
			ret |= ((tiles & 144115188075855872) == 144115188075855872) << 17; 
            ret |= ((tiles & 256) == 256) << 3; 
			ret |= ((tiles & 72057594037927936) == 72057594037927936) << 21; 
			break;
        case warthog::jps3D::DOWNNORTHEAST:
			ret |= ((tiles & 8589934592) == 8589934592) << 8; // D
            ret |= ((tiles & 2) == 2) << 0; // N
			ret |= ((tiles & 33554432) == 33554432) << 9; // DN
            ret |= ((tiles & 1024) == 1024) << 2; // E 
            ret |= ((tiles & 17179869184) == 17179869184) << 11; // DE
            ret |= ((tiles & 4) == 4) << 4; // NE
            ret |= ((tiles & 67108864) == 67108864) << 13; // DNE
			break;
        case warthog::jps3D::DOWNNORTHWEST:
			ret |= ((tiles & 8589934592) == 8589934592) << 8; // D
            ret |= ((tiles & 2) == 2) << 0; // N
			ret |= ((tiles & 33554432) == 33554432) << 9; // DN
            ret |= ((tiles & 256) == 256) << 3; // W 
            ret |= ((tiles & 4294967296) == 4294967296) << 12; // DW
            ret |= ((tiles & 1) == 1) << 5; // NW
            ret |= ((tiles & 16777216) == 16777216) << 14; // DNW
			break;
        case warthog::jps3D::DOWNSOUTHEAST:
			ret |= ((tiles & 8589934592) == 8589934592) << 8; // D
            ret |= ((tiles & 131072) == 131072) << 1; // S
			ret |= ((tiles & 2199023255552) == 2199023255552) << 10; // DS
            ret |= ((tiles & 1024) == 1024) << 2; // E 
            ret |= ((tiles & 17179869184) == 17179869184) << 11; // DE
            ret |= ((tiles & 262144) == 262144) << 6; // SE
            ret |= ((tiles & 4398046511104) == 4398046511104) << 15; // DSE
			break;
        case warthog::jps3D::DOWNSOUTHWEST:
			ret |= ((tiles & 8589934592) == 8589934592) << 8; // D
            ret |= ((tiles & 131072) == 131072) << 1; // S
			ret |= ((tiles & 2199023255552) == 2199023255552) << 10; // DS
            ret |= ((tiles & 256) == 256) << 3; // W 
            ret |= ((tiles & 4294967296) == 4294967296) << 12; // DW
            ret |= ((tiles & 65536) == 65536) << 7; // SW
            ret |= ((tiles & 1099511627776) == 1099511627776) << 16; // DSW
			break;
        case warthog::jps3D::UPNORTHEAST:
			ret |= ((tiles & 144115188075855872) == 144115188075855872) << 17; // U
            ret |= ((tiles & 2) == 2) << 0; // N
			ret |= ((tiles & 562949953421312) == 562949953421312) << 18; // UN
            ret |= ((tiles & 1024) == 1024) << 2; // E 
            ret |= ((tiles & 288230376151711744) == 288230376151711744) << 20; // UE
            ret |= ((tiles & 4) == 4) << 4; // NE
            ret |= ((tiles & 1125899906842624) == 1125899906842624) << 22; // UNE
			break;
        case warthog::jps3D::UPNORTHWEST:
			ret |= ((tiles & 144115188075855872) == 144115188075855872) << 17; // U
			ret |= ((tiles & 2) == 2) << 0; // N
			ret |= ((tiles & 562949953421312) == 562949953421312) << 18; // UN
			ret |= ((tiles & 256) == 256) << 3; // W 
			ret |= ((tiles & 72057594037927936) == 72057594037927936) << 21; // UW
			ret |= ((tiles & 1) == 1) << 5; // NW
			ret |= ((tiles & 281474976710656) == 281474976710656) << 23; // UNW
			break;
        case warthog::jps3D::UPSOUTHEAST:
			ret |= ((tiles & 144115188075855872) == 144115188075855872) << 17; // U
            ret |= ((tiles & 131072) == 131072) << 1; // S
			ret |= ((tiles & warthog::lnum_onlyUS) == warthog::lnum_onlyUS) << 19;  // US
            ret |= ((tiles & 1024) == 1024) << 2; // E 
            ret |= ((tiles & 288230376151711744) == 288230376151711744) << 20; // UE
            ret |= ((tiles & 262144) == 262144) << 6; // SE
            ret |= ((tiles & warthog::lnum_onlyUSE) == warthog::lnum_onlyUSE) << 24; // USE
			break;
        case warthog::jps3D::UPSOUTHWEST:
			ret |= ((tiles & 144115188075855872) == 144115188075855872) << 17; // U
            ret |= ((tiles & 131072) == 131072) << 1; // S
			ret |= ((tiles & warthog::lnum_onlyUS) == warthog::lnum_onlyUS) << 19;  // US
            ret |= ((tiles & 256) == 256) << 3; // W 
            ret |= ((tiles & 72057594037927936) == 72057594037927936) << 21; // UW
            ret |= ((tiles & 65536) == 65536) << 7; // SW
            ret |= ((tiles & warthog::lnum_onlyUSW) == warthog::lnum_onlyUSW) << 25; // USW
			break;
		default:
            ret |= ((tiles & 1) == 1) << 5; // NW
			ret |= ((tiles & 2) == 2) << 0; // N
            ret |= ((tiles & 4) == 4) << 4; // NE
            ret |= ((tiles & 256) == 256) << 3; // W
			ret |= ((tiles & 1024) == 1024) << 2; // E
            ret |= ((tiles & 65536) == 65536) << 7; // SW
            ret |= ((tiles & 131072) == 131072) << 1; // S
            ret |= ((tiles & 262144) == 262144) << 6; // SE
            ret |= ((tiles & 16777216) == 16777216) << 14; // DNW
            ret |= ((tiles & 33554432) == 33554432) << 9; // DN
            ret |= ((tiles & 67108864) == 67108864) << 13; // DNE
            ret |= ((tiles & 4294967296) == 4294967296) << 12; // DW
            ret |= ((tiles & 8589934592) == 8589934592) << 8; // D
            ret |= ((tiles & 17179869184) == 17179869184) << 11; // DE
            ret |= ((tiles & 1099511627776) == 1099511627776) << 16; // DSW
            ret |= ((tiles & 2199023255552) == 2199023255552) << 10; // DS
            ret |= ((tiles & 4398046511104) == 4398046511104) << 15; // DSE
            ret |= ((tiles & 281474976710656) == 281474976710656) << 23; // UNW
            ret |= ((tiles & 562949953421312) == 562949953421312) << 18; // UN
            ret |= ((tiles & 1125899906842624) == 1125899906842624) << 22; // UNE
            ret |= ((tiles & 72057594037927936) == 72057594037927936) << 21; // UW
            ret |= ((tiles & 144115188075855872) == 144115188075855872) << 17; // U
            ret |= ((tiles & 288230376151711744) == 288230376151711744) << 20; // UE
            ret |= ((tiles & warthog::lnum_onlyUSW) == warthog::lnum_onlyUSW) << 25; // USW -- 18446744073709551616
            ret |= ((tiles & warthog::lnum_onlyUS) == warthog::lnum_onlyUS) << 19; // US -- 36893488147419103232
            ret |= ((tiles & warthog::lnum_onlyUSE) == warthog::lnum_onlyUSE) << 24; // USE -- 73786976294838206464 
			break;
	}
    // std::cout << "NATURAL:   " << std::bitset<32>(ret) << "\n";
	return ret;
}