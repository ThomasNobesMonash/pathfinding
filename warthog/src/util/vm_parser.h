#ifndef WARTHOG_VOXELMAP_PARSER_H
#define WARTHOG_VOXELMAP_PARSER_H

// voxel_parser.h
//
// A parser for voxelmap files written in Nathan Sturtevant's MOVING-AI format.
//
// @author: dharabor, tnobes
// @created: 10/04/2022
//

#include <cstdlib>
#include <fstream>
#include <iostream>
#include <vector>

namespace warthog 
{
    class vm_header 
    {
        public:
            vm_header(int width, int height, int depth, const char* type)
                : width_(width), height_(height), depth_(depth), type_(type)
            {
            }

            vm_header() : width_(0), height_(0), depth_(0), type_("") 
			{ 
			}

            vm_header(const warthog::vm_header& other)
			{
				(*this) = other;
			}

            virtual ~vm_header()
            {
            }

            vm_header& operator=(const warthog::vm_header& other)
			{
				this->width_ = other.width_;
				this->height_ = other.height_;
                this->depth_ = other.depth_;
				this->type_ = other.type_;
				return *this;
			}

            int width_;
            int height_;
            int depth_; 
            std::string type_;
    }; 

    class vm_parser
    {
        public:
            vm_parser(const char* filename);
            ~vm_parser();

            inline warthog::vm_header
            get_header()
            {
                return this-> header_;
            }

            inline uint32_t
			get_num_tiles() 
			{ 
				return this->map_.size(); 
			}

            inline char
			get_tile_at(unsigned int index) 
			{ 
				if(index >= this->map_.size())
				{
					std::cerr << "err; vm_parser::get_tile_at "
						"invalid index " << index << std::endl;
					exit(1);
				}
				return this->map_.at(index); 
			}
            
            std::vector<int> cvt_id_to_xyz(int id);
            int cvt_xyz_to_id(int x, int y, int z);
        
        private:
            vm_parser(const vm_parser&) {}
            vm_parser& operator=(const vm_parser&) { return *this; }

            bool parse_header(std::fstream&);
            void parse_map(std::fstream&, bool rev);

            std::vector<char> map_;
            vm_header header_;
    };
}

#endif