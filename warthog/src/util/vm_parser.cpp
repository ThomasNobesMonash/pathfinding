#include <vector>
#include <fstream>
#include <unordered_map>
#include "vm_parser.h"

warthog::vm_parser::vm_parser(const char* filename) 
{
    std::fstream mapfs(filename, std::fstream::in);
	if(!mapfs.is_open())
	{
		std::cerr << "err; vm_parser::vm_parser "
			"cannot open map file: "<<filename << std::endl;
		exit(1);
	}

	bool rev = this->parse_header(mapfs);
	this->parse_map(mapfs, rev);
	mapfs.close();
}

warthog::vm_parser::~vm_parser()
{
}

bool
warthog::vm_parser::parse_header(std::fstream& mapfs)
{
    // read header fields
    std::string htype, hwidth, hheight, hdepth;
    mapfs >> htype;
    mapfs >> hwidth;
    mapfs >> hheight;
    mapfs >> hdepth;
    
    this->header_.type_ = htype;
    if (this->header_.type_.compare("voxel") != 0 && this->header_.type_.compare("rev_voxel") != 0)
    {
        std::cerr << "err; map type " << this->header_.type_ <<
            "is unkown. known types: voxel / rev_voxel" << std::endl;
        exit(1);
    }
    this->header_.width_ = std::stoi(hwidth);
    if (this->header_.width_ == 0)
	{
		std::cerr << "err; map file specifies invalid width. " << std::endl;
		exit(1);
	}
    this->header_.height_ = std::stoi(hheight);
    if (this->header_.height_ == 0)
	{
		std::cerr << "err; map file specifies invalid height. " << std::endl;
		exit(1);
	}
    this->header_.depth_ = std::stoi(hdepth);
    if (this->header_.depth_ == 0)
	{
		std::cerr << "err; map file specifies invalid depth. " << std::endl;
		exit(1);
	}

    if(this->header_.type_.compare("rev_voxel") == 0) { std::cout << "reversing voxels in map\n";return true; }
    else { return false; }
}

void
warthog::vm_parser::parse_map(std::fstream& mapfs, bool rev)
{
    int db_size = this->header_.width_ * this->header_.height_ * this->header_.depth_;
    if(rev)
    {
        for(int i=0; i < db_size; i++)
        {
            this->map_.push_back('O');
        }
    }
    else
    {
        for(int i=0; i < db_size; i++)
        {
            this->map_.push_back('.');
        }
    }
    
    // read map data
    int index = 0;
    while (true)
    {
        /// read line
        std::string x, y, z;
        mapfs >> x; mapfs >> y; mapfs >> z;
        if (x == "" || y == "" || z == "")
        {
            break;
        }

        int obs_index = this->cvt_xyz_to_id(std::stoi(x), std::stoi(y), std::stoi(z));
        if(rev) { this->map_[obs_index] = '.'; }
        else { this->map_[obs_index] = 'O'; }
        index++;
    }
    // std::cerr << "map dimensions = (" 
    //     << this->header_.width_ << ", "
    //     << this->header_.height_ << ", "
    //     << this->header_.depth_ << ")";
    // std::cerr << " | map size = " << this->get_num_tiles() << "\n";
}


std::vector<int> 
warthog::vm_parser::cvt_id_to_xyz(int id) 
{
    int z = id / (this->header_.width_ * this->header_.height_);
    int a = id - (this->header_.width_ * this->header_.height_ * z);
    int y = a / this->header_.width_;
    int x = a % this->header_.width_;
    std::vector<int> res;
    res.insert(res.end(),{x, y, z});
    return res;
}  

int
warthog::vm_parser::cvt_xyz_to_id(int x, int y, int z) 
{
    return y * this->header_.width_ + x + z * this->header_.width_ * this->header_.height_;
}  
