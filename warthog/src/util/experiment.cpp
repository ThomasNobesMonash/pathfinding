#include "experiment.h"

#include <iomanip>

void
warthog::experiment::print(std::ostream& out)
{
    if(this->mapdepth() <= 1)
    {
        out << this->map() <<"\t";
        out << this->mapwidth() << "\t";
        out << this->mapheight() << "\t";
        out << this->startx() <<"\t";
        out << this->starty()<<"\t";
        out << this->goalx()<<"\t";
        out << this->goaly()<<"\t";
        out << std::fixed << std::setprecision((int)this->precision());
        out << this->distance();
    }
    else
    {
        out << this->map() <<"\t";
        out << this->mapwidth() << "\t";
        out << this->mapheight() << "\t";
        out << this->mapdepth() << "\t";
        out << this->startx() <<"\t";
        out << this->starty()<<"\t";
        out << this->startz()<<"\t";
        out << this->goalx()<<"\t";
        out << this->goaly()<<"\t";
        out << this->goalz()<<"\t";
        out << std::fixed << std::setprecision((int)this->precision());
        out << this->distance();
    }
	
}
