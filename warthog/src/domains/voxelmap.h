#ifndef WARTHOG_VOXELMAP_H
#define WARTHOG_VOXELMAP_H

// voxelmap.h
//
// A uniform cost voxelmap implementation.  The map is stored in
// a compact matrix form. Nodes are represented as single bit quantities.
//
// NB: To allow efficient lookups this implementation uses a padding scheme
// that adds extra elements to the end of each row and also introduces 
// empty planes immediately before and immediately after the start and end of 
// the gridmap data.
// Padding allows us to refer to tiles and their neighbours by their indexes
// in a one dimensional array and also to avoid range checks when trying to 
// identify invalid neighbours of tiles on the edge of the map.
//
// @author: dharabor, tnobes
// @created: 10/04/2022
// 

#include "constants.h"
#include "vm_parser.h"

#include <climits>
#include "stdint.h"

namespace warthog
{

// const uint32_t GRID_ID_MAX = (uint32_t)warthog::SN_ID_MAX;
class voxelmap
{
	public:
		voxelmap(uint32_t width, uint32_t height, uint32_t depth);
		voxelmap(const char* filename);
		~voxelmap();

		// here we convert from the coordinate space of 
		// the original grid to the coordinate space of db_. 
		inline uint32_t
        to_padded_id(uint32_t node_id)
		{
            uint32_t z = node_id / (header_.width_ * header_.height_);
            uint32_t flat_id = node_id - (header_.width_ * header_.height_ * z);
            return node_id + 
                // ------ padding on the current plane before this one -------
                padded_rows_before_first_row_ * padded_width_ +    // padded rows before the actual map data starts
                flat_id / header_.width_ * padding_per_row_  +     // padding from each row of data before this one
                // ------ padding from all previous planes -------
                z *                                                 // multiply by number of previous rows
                (padded_rows_before_first_row_ * padded_width_ +   // padded rows before the actual map data starts
                padded_rows_after_last_row_ * padded_width_ +      // padded rows after the actual map ends
                header_.height_ * padding_per_row_) +               // padding from each row of data in the plane
                // ------ padding from padded plane before first plane -------
                padded_planes_before_first_plane_ * padded_width_ * padded_height_;    // extra padded plane below 
		}

		// here we convert from the coordinate space of 
		// the original grid to the coordinate space of db_. 
		inline uint32_t
		to_padded_id(uint32_t x, uint32_t y, uint32_t z)
		{
			return to_padded_id(y * this->header_width() + x + z * header_width() * header_height());
		}

        inline uint32_t
        to_unpadded_id(uint32_t x, uint32_t y, uint32_t z)
		{
            return y * this->header_width() + x + z * header_width() * header_height();
		}

		inline void
		to_unpadded_xyz(uint32_t padded_id, uint32_t& x, uint32_t& y, uint32_t& z)
		{
            z = padded_id / (padded_width_ * padded_height_);
            int a = padded_id - (padded_width_ * padded_height_ * z);
            z -= padded_planes_before_first_plane_;
            // remove padding from padded index 'a' on first layer
            a -= padded_rows_before_first_row_* padded_width_;
            y = a / padded_width_;
			x = a % padded_width_;
		}

		inline void
		to_padded_xyz(uint32_t padded_id, uint32_t& x, uint32_t& y, uint32_t& z)
		{
            z = padded_id / (padded_width_ * padded_height_);
            int a = padded_id - (padded_width_ * padded_height_ * z);
            y = a / padded_width_;
			x = a % padded_width_;
		}

        // fetches a contiguous set of tiles from three adjacent rows. each row is
		// 32 tiles long. the middle row begins with tile padded_id. the other tiles
		// are from the row immediately above and immediately below padded_id.
		inline void
		get_neighbours_32bit(uint32_t padded_id, uint32_t tiles[9])
		{
			// 1. calculate the dbword offset for the node at index padded_id
			// 2. convert padded_id into a dbword index.
			uint32_t bit_offset = (padded_id & warthog::DBWORD_BITS_MASK);
			uint32_t dbindex = padded_id >> warthog::LOG2_DBWORD_BITS;

			// compute dbword indexes for tiles
            uint32_t pos1 = dbindex - dbwidth_;                                // horiz row above (NW, N, NE)
			uint32_t pos2 = dbindex;                                            // horiz same row (W, C, E)
			uint32_t pos3 = dbindex + dbwidth_;                                // horiz row below (SW, S, SE)
            uint32_t pos4 = dbindex - (dbwidth_ * dbheight_) - dbwidth_;      // plane below, horiz row above (DNW, DN, DNE)
            uint32_t pos5 = dbindex - (dbwidth_ * dbheight_);                  // plane below, horiz same row (DW, D, DE)
            uint32_t pos6 = dbindex - (dbwidth_ * dbheight_) + dbwidth_;      // plane below, horiz row below (DSW, DS, DSE)
            uint32_t pos7 = dbindex + (dbwidth_ * dbheight_) - dbwidth_;      // plane above, horiz row above (UNW, UN, UNE)
            uint32_t pos8 = dbindex + (dbwidth_ * dbheight_);                  // plane above, horiz same row (UW, U, UE)  
            uint32_t pos9 = dbindex + (dbwidth_ * dbheight_) + dbwidth_;      // plane above, horiz row below (USW, US, USE)

			// read 32bits of memory; padded_id is in the 
			// lowest bit position of tiles[1]
			tiles[0] = (uint32_t)(*((uint64_t*)(db_+pos1)) >> (bit_offset));
			tiles[1] = (uint32_t)(*((uint64_t*)(db_+pos2)) >> (bit_offset));
			tiles[2] = (uint32_t)(*((uint64_t*)(db_+pos3)) >> (bit_offset));
			tiles[3] = (uint32_t)(*((uint64_t*)(db_+pos4)) >> (bit_offset));
			tiles[4] = (uint32_t)(*((uint64_t*)(db_+pos5)) >> (bit_offset));
			tiles[5] = (uint32_t)(*((uint64_t*)(db_+pos6)) >> (bit_offset));
			tiles[6] = (uint32_t)(*((uint64_t*)(db_+pos7)) >> (bit_offset));
			tiles[7] = (uint32_t)(*((uint64_t*)(db_+pos8)) >> (bit_offset));
			tiles[8] = (uint32_t)(*((uint64_t*)(db_+pos9)) >> (bit_offset));
		}

		// similar to get_neighbours_32bit but padded_id is placed into the
		// upper bit of the return value. this variant is useful when jumping
		// toward smaller memory addresses (i.e. west instead of east).
		inline void
		get_neighbours_upper_32bit(uint32_t padded_id, uint32_t tiles[9])
		{
			// 1. calculate the dbword offset for the node at index padded_id
			// 2. convert padded_id into a dbword index.
			uint32_t bit_offset = (padded_id & warthog::DBWORD_BITS_MASK);
			uint32_t dbindex = padded_id >> warthog::LOG2_DBWORD_BITS;
			
			// start reading from a prior index. this way everything
			// up to padded_id is cached.
			// dbindex -= 10;
			dbindex -= 4;

			// compute dbword indexes for tiles..
			uint32_t pos1 = dbindex - dbwidth_;                                // horiz row above (NW, N, NE)
			uint32_t pos2 = dbindex;                                            // horiz same row (W, C, E)
			uint32_t pos3 = dbindex + dbwidth_;                                // horiz row below (SW, S, SE)
            uint32_t pos4 = dbindex - (dbwidth_ * dbheight_) - dbwidth_;      // plane below, horiz row above (DNW, DN, DNE)
            uint32_t pos5 = dbindex - (dbwidth_ * dbheight_);                  // plane below, horiz same row (DW, D, DE)
            uint32_t pos6 = dbindex - (dbwidth_ * dbheight_) + dbwidth_;      // plane below, horiz row below (DSW, DS, DSE)
            uint32_t pos7 = dbindex + (dbwidth_ * dbheight_) - dbwidth_;      // plane above, horiz row above (UNW, UN, UNE)
            uint32_t pos8 = dbindex + (dbwidth_ * dbheight_);                  // plane above, horiz same row (UW, U, UE)  
            uint32_t pos9 = dbindex + (dbwidth_ * dbheight_) + dbwidth_;      // plane above, horiz row below (USW, US, USE)

			// read 32bits of memory; padded_id is in the 
			// highest bit position of tiles[1]
			tiles[0] = (uint32_t)(*((uint64_t*)(db_+pos1)) >> (bit_offset+1));
			tiles[1] = (uint32_t)(*((uint64_t*)(db_+pos2)) >> (bit_offset+1));
			tiles[2] = (uint32_t)(*((uint64_t*)(db_+pos3)) >> (bit_offset+1));
			tiles[3] = (uint32_t)(*((uint64_t*)(db_+pos4)) >> (bit_offset+1));
			tiles[4] = (uint32_t)(*((uint64_t*)(db_+pos5)) >> (bit_offset+1));
			tiles[5] = (uint32_t)(*((uint64_t*)(db_+pos6)) >> (bit_offset+1));
			tiles[6] = (uint32_t)(*((uint64_t*)(db_+pos7)) >> (bit_offset+1));
			tiles[7] = (uint32_t)(*((uint64_t*)(db_+pos8)) >> (bit_offset+1));
			tiles[8] = (uint32_t)(*((uint64_t*)(db_+pos9)) >> (bit_offset+1));
		}


		// get the immediately adjacent neighbours of @param node_id
		// neighbours from the row above node_id are stored in 
		// @param tiles[0], neighbours from the same row in tiles[1]
		// and neighbours from the row below in tiles[2].
		// In each case the bits for each neighbour are in the three 
		// lowest positions of the byte.
		// position :0 is the nei in direction NW, :1 is N and :2 is NE 
		inline void
		get_neighbours_rectilinear(uint32_t padded_id, uint8_t tiles[5])
        // neighbours w/ recti-linear movement
		{
			// 1. calculate the offset for the node at index padded_id
			// 2. convert padded_id into a dbword index.
			uint32_t bit_offset = (padded_id & warthog::DBWORD_BITS_MASK);
			uint32_t dbindex = padded_id >> warthog::LOG2_DBWORD_BITS; // shift down id by number of required bits to read a dbword
            
            // compute dbword indexes for tiles..
			uint32_t pos1 = dbindex - dbwidth_;                    // immediately above horiz (N)
			uint32_t pos2 = dbindex;                                // in the same row (W, E)
			uint32_t pos3 = dbindex + dbwidth_;                    // immediately below horiz (S)
            uint32_t pos4 = dbindex - (dbwidth_ * dbheight_);      // immediately above vert (Down)
            uint32_t pos5 = dbindex + (dbwidth_ * dbheight_);      // immediately above vert (Up)  

			// read from the byte just before node_id and shift down until the
			// nei adjacent to node_id is in the lowest position
			tiles[0] = (uint8_t)(*((uint32_t*)(db_+(pos1-1))) >> (bit_offset+7));
			tiles[1] = (uint8_t)(*((uint32_t*)(db_+(pos2-1))) >> (bit_offset+7));
			tiles[2] = (uint8_t)(*((uint32_t*)(db_+(pos3-1))) >> (bit_offset+7));
            tiles[3] = (uint8_t)(*((uint32_t*)(db_+(pos4-1))) >> (bit_offset+7));
            tiles[4] = (uint8_t)(*((uint32_t*)(db_+(pos5-1))) >> (bit_offset+7));
		}

		inline void
		get_neighbours(uint32_t padded_id, uint8_t tiles[9])
        // used to expand all neighbours in the 27 voxel box around current node
		{
			// 1. calculate the offset for the node at index padded_id
			// 2. convert padded_id into a dbword index.
			uint32_t bit_offset = (padded_id & warthog::DBWORD_BITS_MASK);
			uint32_t dbindex = padded_id >> warthog::LOG2_DBWORD_BITS; // shift down id by number of required bits to read a dbword

            // compute dbword indexes for tiles..
			uint32_t pos1 = dbindex - dbwidth_;                                // horiz row above (NW, N, NE)
			uint32_t pos2 = dbindex;                                            // horiz same row (W, C, E)
			uint32_t pos3 = dbindex + dbwidth_;                                // horiz row below (SW, S, SE)
            uint32_t pos4 = dbindex - (dbwidth_ * dbheight_) - dbwidth_;      // plane below, horiz row above (DNW, DN, DNE)
            uint32_t pos5 = dbindex - (dbwidth_ * dbheight_);                  // plane below, horiz same row (DW, D, DE)
            uint32_t pos6 = dbindex - (dbwidth_ * dbheight_) + dbwidth_;      // plane below, horiz row below (DSW, DS, DSE)
            uint32_t pos7 = dbindex + (dbwidth_ * dbheight_) - dbwidth_;      // plane above, horiz row above (UNW, UN, UNE)
            uint32_t pos8 = dbindex + (dbwidth_ * dbheight_);                  // plane above, horiz same row (UW, U, UE)  
            uint32_t pos9 = dbindex + (dbwidth_ * dbheight_) + dbwidth_;      // plane above, horiz row below (USW, US, USE)

			// read from the byte just before node_id and shift down until the
			// nei adjacent to node_id is in the lowest position
			tiles[0] = (uint8_t)(*((uint32_t*)(db_+(pos1-1))) >> (bit_offset+7));
			tiles[1] = (uint8_t)(*((uint32_t*)(db_+(pos2-1))) >> (bit_offset+7));
			tiles[2] = (uint8_t)(*((uint32_t*)(db_+(pos3-1))) >> (bit_offset+7));
            tiles[3] = (uint8_t)(*((uint32_t*)(db_+(pos4-1))) >> (bit_offset+7));
            tiles[4] = (uint8_t)(*((uint32_t*)(db_+(pos5-1))) >> (bit_offset+7));
            tiles[5] = (uint8_t)(*((uint32_t*)(db_+(pos6-1))) >> (bit_offset+7));
            tiles[6] = (uint8_t)(*((uint32_t*)(db_+(pos7-1))) >> (bit_offset+7));
            tiles[7] = (uint8_t)(*((uint32_t*)(db_+(pos8-1))) >> (bit_offset+7));
            tiles[8] = (uint8_t)(*((uint32_t*)(db_+(pos9-1))) >> (bit_offset+7));
		}

		// get the label associated with the padded coordinate pair (x, y, z)
		inline bool
		get_label(uint32_t x, uint32_t y, uint32_t z)
		{
			return this->get_label(y*padded_width_+x+z*padded_width_*padded_height_);
		}

		inline warthog::dbword 
		get_label(uint32_t padded_id)
		{
			// now we can fetch the label
			warthog::dbword bitmask = 1;
			bitmask <<= (padded_id & warthog::DBWORD_BITS_MASK);
			uint32_t dbindex = padded_id >> warthog::LOG2_DBWORD_BITS;
			if(dbindex > max_id_) { return 0; }
			return (db_[dbindex] & bitmask) != 0;
		}

		// set the label associated with the padded coordinate pair (x, y)
		inline void
	    set_label(uint32_t x, uint32_t y, uint32_t z, bool label)
		{
			this->set_label(y*padded_width_+x+z*padded_width_*padded_height_, label);
		}

		inline void 
		set_label(uint32_t padded_id, bool label)
		{
			warthog::dbword bitmask =
				(1 << (padded_id & warthog::DBWORD_BITS_MASK));
			uint32_t dbindex = padded_id >> warthog::LOG2_DBWORD_BITS;
			if(dbindex > max_id_) { return; }

			if(label)
			{
				db_[dbindex] |= bitmask;
			}
			else
			{
				db_[dbindex] &= ~bitmask;
			}
		}

		inline uint32_t
		padded_mapsize()
		{
			return padded_width_ * padded_height_ * padded_depth_;
		}

		inline uint32_t 
		width() const 
		{ 
			return this->padded_width_;
		}

        inline uint32_t 
		height() const 
		{ 
			return this->padded_height_;
		}

        inline uint32_t 
		depth() const
		{ 
			return this->padded_depth_;
		} 

		inline uint32_t 
		header_width()
		{
			return this->header_.width_;
		}

        inline uint32_t 
		header_height()
		{
			return this->header_.height_;
		}

        inline uint32_t 
		header_depth()
		{
			return this->header_.depth_;
		}

        inline uint32_t
        get_num_obstacles()
        {
            return num_obstacles_;
        }

        inline void
        increment_obstacles()
        {
            num_obstacles_++;
        }

		inline const char*
		filename()
		{
			return this->filename_;
		}

		void 
		print(std::ostream&);

		void
		printdb(std::ostream& out);

		uint32_t 
		mem()
		{
			return sizeof(*this) +
			sizeof(warthog::dbword) * db_size_;
		}


	private:
		warthog::vm_header header_;
		warthog::dbword* db_;
		char filename_[256];

		uint32_t dbwidth_;
        uint32_t dbheight_;
		uint32_t dbdepth_;
		uint32_t db_size_;
		uint32_t padded_width_;
        uint32_t padded_height_;
		uint32_t padded_depth_;
		uint32_t padding_per_row_;
		uint32_t padding_column_above_;
		uint32_t padded_rows_before_first_row_;
		uint32_t padded_rows_after_last_row_;
        uint32_t padded_planes_before_first_plane_;
        uint32_t padded_planes_after_last_plane_;
		uint32_t max_id_;
        uint32_t num_obstacles_ = 0;

		voxelmap(const warthog::voxelmap& other) {}
		voxelmap& operator=(const warthog::voxelmap& other) { return *this; }
		void init_db();
};

}

#endif

