#include "voxelmap_expansion_policy.h"
#include "helpers.h"
#include "problem_instance.h"
#include <bitset>
#include <boost/multiprecision/cpp_int.hpp>

warthog::voxelmap_expansion_policy::voxelmap_expansion_policy(
		warthog::voxelmap* map, bool rectilinear)
: expansion_policy(map->width()*map->height()*map->depth()), map_(map), rectilinear_(rectilinear)
{
}

void 
warthog::voxelmap_expansion_policy::expand(warthog::search_node* current,
		warthog::problem_instance* problem)
{
	reset();

	// get terrain type of each tile in the 3x3x3 cube around (x, y, z)
	boost::multiprecision::uint128_t tiles = 0;
	uint32_t nodeid = (uint32_t)current->get_id();
	map_->get_neighbours(nodeid, (uint8_t*)&tiles);

	// NB: no corner cutting or squeezing between obstacles!
	uint32_t nid_m_w = nodeid - map_->width();                                         // north
	uint32_t nid_p_w = nodeid + map_->width();                                         // south
    uint32_t nid_up_m_w = nodeid + map_->width() * map_->height() - map_->width();    // up north
    uint32_t nid_up = nodeid + map_->width() * map_->height();                         // up 
    uint32_t nid_up_p_w = nodeid + map_->width() * map_->height() + map_->width();    // up south
    uint32_t nid_down_m_w = nodeid - map_->width() * map_->height() - map_->width();  // down north
    uint32_t nid_down = nodeid - map_->width() * map_->height();                       // down
    uint32_t nid_down_p_w = nodeid - map_->width() * map_->height() + map_->width();  // down south

    // generate cardinal moves
    if((tiles & 514) == 514) // N
	{  
		add_neighbour(this->generate(nid_m_w), 1);
	} 
    if((tiles & 1536) == 1536) // E
	{
		add_neighbour(this->generate(nodeid + 1), 1);
	}
    if((tiles & 131584) == 131584) // S
	{		
        add_neighbour(this->generate(nid_p_w), 1);
	}
    if((tiles & 768) == 768) // W
	{
		add_neighbour(this->generate(nodeid - 1), 1);
	}
    if((tiles & 144115188075856384) == 144115188075856384) // U
	{
		add_neighbour(this->generate(nid_up), 1);
    }
    if((tiles & 8589935104) == 8589935104) // D
	{
		add_neighbour(this->generate(nid_down), 1);
	}
    if(rectilinear_) { return; }

    // generate 2D diagonal moves
    if((tiles & 1542) == 1542) // NE
	{
		add_neighbour(this->generate(nid_m_w + 1), warthog::DBL_ROOT_TWO);
	}
    if((tiles & 394752) == 394752) // SE
	{
		add_neighbour(this->generate(nid_p_w + 1), warthog::DBL_ROOT_TWO);
	}
    if((tiles & 197376) == 197376) // SW
	{
		add_neighbour(this->generate(nid_p_w - 1), warthog::DBL_ROOT_TWO);
	}
    if((tiles & 771) == 771) // NW
	{		
        add_neighbour(this->generate(nid_m_w - 1), warthog::DBL_ROOT_TWO);
	}
    if((tiles & 8623489538) == 8623489538) // DN
	{
		add_neighbour(this->generate(nid_down_m_w), warthog::DBL_ROOT_TWO);
	}
    if((tiles & 25769805312) == 25769805312) // DE
	{
		add_neighbour(this->generate(nid_down + 1), warthog::DBL_ROOT_TWO);
	}
    if((tiles & 2207613321728) == 2207613321728) // DS
	{
		add_neighbour(this->generate(nid_down_p_w), warthog::DBL_ROOT_TWO);
	}
    if((tiles & 12884902656) == 12884902656) // DW
	{
		add_neighbour(this->generate(nid_down - 1), warthog::DBL_ROOT_TWO);
	}
    if((tiles & 144678138029277698) == 144678138029277698) // UN
	{
		add_neighbour(this->generate(nid_up_m_w), warthog::DBL_ROOT_TWO);
    }
    if((tiles & 432345564227569152) == 432345564227569152) // UE
	{
		add_neighbour(this->generate(nid_up + 1), warthog::DBL_ROOT_TWO);
    }
    // note that 37037603335495090688 is 66 bit and thus cannot be stored
    if((tiles & warthog::lnum_US_wc) == warthog::lnum_US_wc) // US
	{
		add_neighbour(this->generate(nid_up_p_w), warthog::DBL_ROOT_TWO);
    }
    if((tiles & 216172782113784576) == 216172782113784576) // UW
	{
		add_neighbour(this->generate(nid_up - 1), warthog::DBL_ROOT_TWO);
    }
    
    // generate 3D diagonal moves
    if((tiles & 25870468614) == 25870468614) // DNE
	{
		add_neighbour(this->generate(nid_down_m_w + 1), warthog::DBL_ROOT_THREE);
	}
    if((tiles & 6622839965184) == 6622839965184) // DSE
	{
		add_neighbour(this->generate(nid_down_p_w + 1), warthog::DBL_ROOT_THREE);
	}
    if((tiles & 3311419982592) == 3311419982592) // DSW
	{
		add_neighbour(this->generate(nid_down_p_w - 1), warthog::DBL_ROOT_THREE);
	}
    if((tiles & 12935234307) == 12935234307) // DNW
	{
		add_neighbour(this->generate(nid_down_m_w - 1), warthog::DBL_ROOT_THREE);
	}
    if((tiles & 434034414087833094) == 434034414087833094) // UNE
	{
		add_neighbour(this->generate(nid_up_m_w + 1), warthog::DBL_ROOT_THREE);
    }
    // note that 111112810006485272064 is 67 bit and thus cannot be stored
    if((tiles & warthog::lnum_USE_wc) == warthog::lnum_USE_wc) // USE
	{
		add_neighbour(this->generate(nid_up_p_w + 1), warthog::DBL_ROOT_THREE);
    }
    // note that 55556405003242636032 is 66 bit and thus cannot be stored 
    if((tiles & warthog::lnum_USW_wc) == warthog::lnum_USW_wc) // USW
	{
		add_neighbour(this->generate(nid_up_p_w - 1), warthog::DBL_ROOT_THREE);
    }
    if((tiles & 217017207043916547) == 217017207043916547) // UNW
	{
		add_neighbour(this->generate(nid_up_m_w - 1), warthog::DBL_ROOT_THREE);
    }

}

void
warthog::voxelmap_expansion_policy::get_xy(warthog::sn_id_t nid, int32_t& x, int32_t& y)
{
    ;
}

void
warthog::voxelmap_expansion_policy::get_xyz(warthog::sn_id_t nid, int32_t& x, int32_t& y, int32_t& z)
{
    map_->to_unpadded_xyz((uint32_t)nid, (uint32_t&)x, (uint32_t&)y, (uint32_t&)z);
}

warthog::search_node* 
warthog::voxelmap_expansion_policy::generate_start_node(
        warthog::problem_instance* pi)
{ 
    uint32_t max_id = map_->header_width() * map_->header_height() * map_->header_depth();
    if((uint32_t)pi->start_id_ >= max_id) { return 0; }
    uint32_t padded_id = map_->to_padded_id((uint32_t)pi->start_id_);
    if(map_->get_label(padded_id) == 0) { return 0; }
    return generate(padded_id);
}

warthog::search_node*
warthog::voxelmap_expansion_policy::generate_target_node(
        warthog::problem_instance* pi)
{
    uint32_t max_id = map_->header_width() * map_->header_height() * map_->header_depth();
    if((uint32_t)pi->target_id_ >= max_id) { return 0; }
    uint32_t padded_id = map_->to_padded_id((uint32_t)pi->target_id_);
    if(map_->get_label(padded_id) == 0) { return 0; }
    return generate(padded_id);
}

size_t
warthog::voxelmap_expansion_policy::mem()
{
    return 
        expansion_policy::mem() + 
        sizeof(*this) + 
        map_->mem();
}
