#ifndef WARTHOG_VOXELMAP_EXPANSION_POLICY_H
#define WARTHOG_VOXELMAP_EXPANSION_POLICY_H

// voxelmap_expansion_policy.h
//
// An ExpansionPolicy for cubic uniform-cost voxel grids
//
// @author: dharabor, tnobes
// @created: 10/04/2022
//

#include "expansion_policy.h"
#include "voxelmap.h"
#include "search_node.h"

#include <memory>

namespace warthog
{

class problem_instance;
class voxelmap_expansion_policy : public expansion_policy
{
	public:
		voxelmap_expansion_policy(warthog::voxelmap* map, bool rectilinear = false);
		virtual ~voxelmap_expansion_policy() { }

        virtual void
        expand(warthog::search_node*, warthog::problem_instance*);

        virtual void
        get_xy(sn_id_t node_id, int32_t& x, int32_t& y);
        
        virtual void
        get_xyz(sn_id_t node_id, int32_t& x, int32_t& y, int32_t& z);

        virtual warthog::search_node*
        generate_start_node(warthog::problem_instance* pi);

        virtual warthog::search_node*
        generate_target_node(warthog::problem_instance* pi);

        virtual size_t
        mem();

    private:
        warthog::voxelmap* map_;
        bool rectilinear_;
    
};

}

#endif
